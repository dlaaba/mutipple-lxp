<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mlms' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't~Hw,Lj6%B=r}jI%1?`.7_A5idA)p3*gw?xN/<DlC)DXy?K6HK}om*rc/B0B_=I%' );
define( 'SECURE_AUTH_KEY',  '-7Z^PtKaCK,!|VO]n Z=s.?mU=rp2gqTs;d?VS>O|^+k:DVMv*y=]k!*`R!cRN5;' );
define( 'LOGGED_IN_KEY',    'q4zqCW9fk)M6TR[Pv^2ivd(N0 yN BlE>$0R*Q]*7)%d(/DM^O?*/5 c0!jZXz_*' );
define( 'NONCE_KEY',        'yr9av,-ZmP3Wpt#3p10 B5*,y/0C+aky5IVlqteJm_Y8CktqPA)+|}8^nx)mNihH' );
define( 'AUTH_SALT',        'Ksjc}i6{&&9DT:mt8KLcuNBBYAP=-je4 C<!([L{?&Wh!Z79!u70M$k06F<1c%7>' );
define( 'SECURE_AUTH_SALT', '35dL=/NBV#)5JWH7Lh;9+-1X]2%lWSnLi5:~auWq+9OXz(XqCqj]bnKJNJbUILy&' );
define( 'LOGGED_IN_SALT',   'y;>GSFFVex{k%{NEfbC6!We@QMX#ui J,Jnn}+vlD-drn>ma#okn;h3?=8W9|Oqd' );
define( 'NONCE_SALT',       '4dy$uEHaacQOPn|.)XDDl+rFRmynbJV* CiVR5DLlH)]j56@p$/-c,tmrqNx/QKF' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'mx_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
