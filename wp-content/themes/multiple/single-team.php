<?php
/*
 * The template for displaying all single team.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();

// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else { $saaspot_content_padding = ''; }
// Padding - Theme Options
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
}

$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$large_image = $large_image[0];
if(class_exists('Aq_Resize')) {
	$team_img = aq_resize( $large_image, '270', '370', true );
} else {$team_img = $large_image;}
$team_featured_img = ( $team_img ) ? $team_img : SAASPOT_PLUGIN_ASTS . '/images/holders/270x370.png';

$abt_title = get_the_title();
$team_options = get_post_meta( get_the_ID(), 'team_options', true );
if($team_options) {
  $team_job_position = $team_options['team_job_position'];
	$team_socials = $team_options['social_icons'];
	$team_contact = $team_options['contact_details'];
} else {
  $team_job_position = '';
  $team_socials = '';
  $team_contact = '';
}
?>
<div class="saspot-mid-wrap team-single <?php echo esc_attr($saaspot_content_padding); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="saspot-team-wrap">
					<div class="row align-items-center">
						<div class="col-md-3">
							<?php if ($large_image) { ?>
			        <div class="saspot-image"><img src="<?php echo esc_url($team_featured_img); ?>" alt="<?php echo esc_attr($abt_title); ?>"></div>
			        <?php } ?>
						</div>
						<div class="col-md-9">
							<div class="single-mate-info">
								<div class="mate-info">
				          <h5 class="mate-name"><?php echo esc_html($abt_title); ?></h5>
				          <p><?php echo esc_html($team_job_position); ?></p>
				        </div>
				        <p><?php the_excerpt(); ?></p>
                <?php if ( ! empty( $team_contact ) ) { ?>
				        	<ul class="mate-contact">
                    <?php foreach ( $team_contact as $contact ) {
                    if($contact['contact_link']) { ?>
                      <li><span><?php echo esc_html($contact['contact_title']); ?></span><a href="<?php echo esc_url($contact['contact_link']); ?>" target="_blank"><?php echo esc_html($contact['contact_text']); ?></a></li>
	                  <?php } else { ?>
                      <li><span><?php echo esc_html($contact['contact_title']); ?></span><?php echo esc_html($contact['contact_text']); ?></li>
	                  <?php } } ?>
                	</ul>
                <?php }
                if ( ! empty( $team_socials ) ) { ?>
				        	<div class="saspot-social rounded">
                    <?php foreach ( $team_socials as $social ) {
                    if($social['icon_link']) { ?>
                    	<a href="<?php echo esc_url($social['icon_link']); ?>"><i class="<?php echo esc_attr($social['icon']); ?>"></i></a>
	                  <?php } } ?>
                	</div>
                <?php } ?>
			        </div>
						</div>
					</div>
				<?php
					if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile;
					endif;
				?>
				</div><!-- Blog Div -->
				<?php
		    	wp_reset_postdata();  // avoid errors further down the page
				?>
			</div><!-- Content Area -->
		</div>
	</div>
</div>
<?php
get_footer();
