jQuery(document).ready(function($) {
  "use strict";
  //SaaSpot On Has Div Remove Class In Another Div Script
  if($('.saspot-main-wrap').has('.saspot-menu').length) {
    $('.saspot-header').removeClass('saspot-sticky');
  };
  if (navigator.userAgent.search('Firefox') >= 0) {
    $('html').addClass('firefox');
    $('body').find('.wow').closest('.elementor-widget-container').addClass('wow-class');
  }

  // Menu Classes
  $('.saspot-navigation > ul > li.normal-style ul').addClass('normal-style');

  //SaaSpot Sticky Script
  $('.saspot-sticky').sticky ({
    topSpacing: 0,
    zIndex: 99
  });

  // Mean Menu
  var $navmenu = $('nav');
  var $menu_starts = ($navmenu.data('nav') !== undefined) ? $navmenu.data('nav') : 1199;
  $('.saspot-header .saspot-navigation').meanmenu({
    meanMenuContainer: '.saspot-header .header-right',
    meanMenuOpen: '<span></span>',
    meanMenuClose: '<span></span>',
    meanExpand: '<i class="fa fa-angle-down"></i>',
    meanContract: '<i class="fa fa-angle-up"></i>',
    meanScreenWidth: $menu_starts,
  });

  //SaaSpot Nice Select Script
  $('select').niceSelect();

  $('.saspot-sidebar .saspot-widget.widget_nav_menu ul li, .saspot-sidebar .saspot-widget.widget_nav_menu ul').removeAttr("class");
  $('.footer-widget.widget_nav_menu ul li, .footer-widget.widget_nav_menu ul').removeAttr("class");
  $('.sitemap-item .bullet-list li, .sitemap-item .bullet-list ul').removeAttr("class");
  $('<span class="dropdown-arrow"></span>').prependTo('.saspot-fullscreen-navigation .saspot-navigation > ul li.has-dropdown');

  $('#user_login').attr( 'placeholder', 'Email address' );
  $('#user_pass').attr( 'placeholder', 'Password' );

  //SaaSpot Onclick Slideup Slidedown Parent And Add Class Script
  $('.dropdown-arrow').on('click', function() {
    var element = $(this).parent('.has-dropdown');
    if (element.hasClass('open')) {
      element.removeClass('open');
      element.find('.has-dropdown').removeClass('open');
      element.find('.dropdown-nav').slideUp();
    }
    else {
      element.addClass('open');
      element.children('.dropdown-nav').slideDown();
      element.siblings('.has-dropdown').children('.dropdown-nav').slideUp();
      element.siblings('.has-dropdown').removeClass('open');
      element.siblings('.has-dropdown').find('.has-dropdown').removeClass('open');
      element.siblings('.has-dropdown').find('.dropdown-nav').slideUp();
    }
  });
  $('.saspot-color').on('click', function() {
    $('.saspot-brand-color').find('.copy-text').text('Click to copy.');
    $(this).parent('.saspot-brand-color').find('.copy-text').text('Copied!');
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(this).parent('.saspot-brand-color').find('span').text()).select();
    document.execCommand("copy");
    $temp.remove();    
  });

  $(window).load(function() {

    if ($('div').hasClass('woocommerce-tabs')) {
      $('.woocommerce-tabs').addClass('saspot-form');
    }
    if ($('section').hasClass('shipping-calculator-form')) {
      $('.shipping-calculator-form').addClass('saspot-form');
    }
    if ($('form').hasClass('woocommerce-checkout')) {
      $('.woocommerce-checkout').addClass('saspot-form');
    }
    if ($('form').hasClass('woocommerce-form')) {
      $('.woocommerce-form').addClass('saspot-form');
    }

    function getId(url){
      var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
      var v = url.match(/https:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);
      var d = url.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
      if ( !m && !v && !d) return null;
      if(m) {
        return m[1];
      }
      if(v){
        return v[2];
      }
      if(d){
        return d[2];
      }
    }
    $('.saspot-video-btn').each( function() {
      var $link = $(this);
      var myId;
      var myUrl = $link.attr("data-src");
      myId = getId(myUrl);

      if(myUrl.match(/[\\?\\&]v=([^\\?\\&]+)/)) {
        $($link).attr("data-src", "https://www.youtube.com/embed/"+ myId + "");
      } 
      if(myUrl.match(/https:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/)) {
        $($link).attr("data-src", "https://player.vimeo.com/video/"+ myId + "?title=0&byline=0&portrait=0");
      }
      if(myUrl.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/)) {
        $($link).attr("data-src", "https://iframespot.blogspot.com/ncr/?m=0&type=dv&url=https%3A%2F%2Fwww.dailymotion.com%2Fembed%2Fvideo%2F"+ myId + "%3Fapi%3D0%26autoplay%3D1%26info%3D0%26logo%3D0%26social%3D0%26related%3D0");
      } 
    });


    if($('div').hasClass('has-checker')) {
      var checker = document.getElementById('checkme');
      var sendbtn = document.getElementById('register');
      checker.checked = false;
      // when unchecked or checked, run the function
      checker.onchange = function(){
        if(this.checked){
          sendbtn.disabled = false;
        } else {
          sendbtn.disabled = true;
        }
      }
    }

    //SaaSpot Hover Script
    $('.header-contact-link, .looking-item, .client-item, .resource-item, .specialist-item, .job-item, .class-item, .environment-item, .video-wrap-inner, .promote-item, .common-feature-inner, .history-quality .saspot-image').hover (
      function() {
        $(this).addClass('saspot-hover');
      },
      function() {
        $(this).removeClass('saspot-hover');
      }
    );

    //SaaSpot Set Equal Height Script
    $('.saspot-item').matchHeight ({
      property: 'height'
    });

    //SaaSpot Masonry Script
    var $grid = $('.saspot-masonry').isotope ({
      itemSelector: '.masonry-item',
      layoutMode: 'packery',
      percentPosition: true,
      isFitWidth: true,
    });
    var filterFns = {
      ium: function() {
        var name = $(this).find('.name').text();
        return name.match(/ium$/);
      }
    };
    $('.masonry-filters').on('click', 'li a', function() {
      var filterValue = $(this).attr('data-filter');
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({
        filter: filterValue
      });
      $('.saspot-masonry').find('.first').removeClass('first');
      $($grid.data('isotope').filteredItems[0].element).addClass('first');
      $grid.isotope('layout');
    });
    $('.masonry-filters').each(function(i, buttonGroup) {
      var $buttonGroup = $(buttonGroup);
      $buttonGroup.on('click', 'li a', function() {
        $buttonGroup.find('.active').removeClass('active');
        $(this).addClass('active');
      });
    });

    //SaaSpot Preloader Script
    $('.saspot-preloader').fadeOut(500);

    if($('div').hasClass('swiper-slides')) {
      $('.swiper-slides').each(function (index) {
        //SaaSpot Swiper Slider Script
        var animEndEv = 'webkitAnimationEnd animationend';
        var swipermw = $('.swiper-container.swiper-mousewheel').length ? true : false;
        var swiperkb = $('.swiper-container.swiper-keyboard').length ? true : false;
        var swipercentered = $('.swiper-container.swiper-center').length ? true : false;
        var swiperautoplay = $('.swiper-container').data('autoplay');
        var swiperinterval = $('.swiper-container').data('interval');
        var swiperloop = $('.swiper-container').data('loop');
        var swipermousedrag = $('.swiper-container').data('mousedrag');
        var swipereffect = $('.swiper-container').data('effect');
        var swiperclikable = $('.swiper-container').data('clickpage');
        var swiperspeed = $('.swiper-container').data('speed');
        var swiperinteraction = $('.swiper-container').data('interaction');

        //SaaSpot Swiper Slides Script
        var autoplay = swiperinterval;
        var swiper = new Swiper($(this), {
          autoplayDisableOnInteraction: swiperinteraction,
          effect: swipereffect,
          speed: swiperspeed,
          loop: swiperloop,
          paginationClickable: swiperclikable,
          watchSlidesProgress: true,
          autoplay: swiperautoplay,
          simulateTouch: swipermousedrag,
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          mousewheelControl: swipermw,
          keyboardControl: swiperkb,
        });
        swiper.on('slideChange', function (s) {
          var currentSlide = $(swiper.slides[swiper.activeIndex]);
            var elems = currentSlide.find('.animated')
            elems.each(function() {
              var $this = $(this);
              var animationType = $this.data('animation');
              $this.addClass(animationType, 100).on(animEndEv, function() {
                $this.removeClass(animationType);
              });
            });
        });
      });
    }

  });

  //SaaSpot Parallax And Get Inline CSS Script
  $(window).resize(function() {
    if (screen.width >= 992) {
      $('.has-dropdown').hover (
        function() {
          $(this).addClass('open');
        },
        function() {
          $(this).removeClass('open');
        }
      );
    };
    if (screen.width >= 768) {
      $('.saspot-parallax').jarallax ({
        speed: 0.6,
      });
    };
    if (screen.width >= 1200) {
      $('.saspot-sticky-footer .main-wrap-inner').css('margin-bottom', $('.saspot-footer').outerHeight());
    };
  });
  $(window).trigger('resize');

  //SaaSpot Wow Animation Script
  $('body').waypoint(function() {
    new WOW().init();
  },
  {
    offset: '100%',
    triggerOnce: true,
  });

  //SaaSpot Onclick Remove Script
  $('.cookies-close').on('click', function() {
    $('.saspot-cookies').fadeOut('normal', function() {
      $(this).remove();
    });
  });

  //SaaSpot Owl Carousel Slider Script
  $('.owl-carousel').each( function() {
    var $carousel = $(this);
    var $items = ($carousel.data('items') !== undefined) ? $carousel.data('items') : 1;
    var $items_tablet = ($carousel.data('items-tablet') !== undefined) ? $carousel.data('items-tablet') : 1;
    var $items_mobile_landscape = ($carousel.data('items-mobile-landscape') !== undefined) ? $carousel.data('items-mobile-landscape') : 1;
    var $items_mobile_portrait = ($carousel.data('items-mobile-portrait') !== undefined) ? $carousel.data('items-mobile-portrait') : 1;
    $carousel.owlCarousel ({
      loop : ($carousel.data('loop') !== undefined) ? $carousel.data('loop') : true,
      items : $carousel.data('items'),
      margin : ($carousel.data('margin') !== undefined) ? $carousel.data('margin') : 0,
      dots : ($carousel.data('dots') !== undefined) ? $carousel.data('dots') : true,
      nav : ($carousel.data('nav') !== undefined) ? $carousel.data('nav') : false,
      navText : ["<div class='slider-no-current'><span class='current-no'></span><span class='total-no'></span></div><span class='current-monials'></span>", "<div class='slider-no-next'></div><span class='next-monials'></span>"],
      autoplay : ($carousel.data('autoplay') !== undefined) ? $carousel.data('autoplay') : false,
      autoplayTimeout : ($carousel.data('autoplay-timeout') !== undefined) ? $carousel.data('autoplay-timeout') : 5000,
      animateIn : ($carousel.data('animatein') !== undefined) ? $carousel.data('animatein') : false,
      animateOut : ($carousel.data('animateout') !== undefined) ? $carousel.data('animateout') : false,
      mouseDrag : ($carousel.data('mouse-drag') !== undefined) ? $carousel.data('mouse-drag') : true,
      autoWidth : ($carousel.data('auto-width') !== undefined) ? $carousel.data('auto-width') : false,
      autoHeight : ($carousel.data('auto-height') !== undefined) ? $carousel.data('auto-height') : false,
      center : ($carousel.data('center') !== undefined) ? $carousel.data('center') : false,
      responsiveClass: true,
      dotsEachNumber: true,
      smartSpeed: 600,
      autoplayHoverPause: true,
      responsive : {
        0 : {
          items : $items_mobile_portrait,
        },
        480 : {
          items : $items_mobile_landscape,
        },
        768 : {
          items : $items_tablet,
        },
        992 : {
          items : $items,
        }
      }
    });
    var totLength = $('.owl-dot', $carousel).length;
    $('.total-no', $carousel).html(totLength);
    $('.current-no', $carousel).html(totLength);
    $carousel.owlCarousel();
    $('.current-no', $carousel).html(1);
    $carousel.on('changed.owl.carousel', function(event) {
      var total_items = event.page.count;
      var currentNum = event.page.index + 1;
      $('.total-no', $carousel ).html(total_items);
      $('.current-no', $carousel).html(currentNum);
    });
  });

  $('.subscribe-form input[type=checkbox]').each( function() {
     $(this).wrap('<span class="checkbox-icon-wrap"></span>');
     $('<span class="checkbox-icon"></span>').appendTo($(this).parent('.checkbox-icon-wrap'));
  });
  $('.login-remember input[type=checkbox]').each( function() {
     $(this).wrap('<span class="checkbox-icon-wrap"></span>');
     $('<span class="checkbox-icon"></span>').appendTo($(this).parent('.checkbox-icon-wrap'));
  });
  $('.saspot-comment-form input[type=checkbox]').each( function() {
     $(this).wrap('<span class="checkbox-icon-wrap"></span>');
     $('<span class="checkbox-icon"></span>').appendTo($(this).parent('.checkbox-icon-wrap'));
  });

  //SaaSpot File Selector Script
  $('.file-selector').change(function() {
    $(this).parents('label').children('.file-title').text(this.files[0].name);
  });
  $('.file-selector').appendTo('.FileSelector');

  //SaaSpot Boostrap Video Modal Script
  var $videoSrc;
  $('[data-target="#SaaSpotVideoModal"]').on('click', function() {
    $videoSrc = $(this).data('src');
  });
  $('#SaaSpotVideoModal').on('shown.bs.modal', function (e) {
    $('#ModalVideoWrap').attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1");
  });
  $('#SaaSpotVideoModal').on('hide.bs.modal', function (e) {
    $('#ModalVideoWrap').attr('src',$videoSrc);
  });

  //SaaSpot Parallax Scroll Script
  $('.saspot-menu ul li a').on('click', function() {
    $('.saspot-menu ul li a').mPageScroll2id ({
      offset: '.sticky-wrapper',
      scrollSpeed: 700,
    });
  });
  $('.saspot-apps-section .masonry-filters ul li a').mPageScroll2id ({
    offset: 150,
    scrollSpeed: 1300,
  });

  //SaaSpot Counter Script
  $('.saspot-counter').counterUp ({
    delay: 1,
    time: 1000,
  });

  //SaaSpot Multiple Checkbox Checked Script
  $('.plan-feature-item > label input').on('click', function () {
    $('.plan-feature-item input[type="checkbox"]').not(this).prop('checked', this.checked);
    var Allfavorite = [];
      $.each($("input[name='feature']:checked"), function(){
          Allfavorite.push($(this).val());
      });
      var checkedValue = Allfavorite.join("&");
      var Clink = $('.plan-feature-item').find(".saspot-btn").attr("data-link");
      $('.plan-feature-item').find(".saspot-btn").attr("href", Clink + checkedValue);
  });

  $('.plan-feature-item .checkbox-wrap label input').on('click', function () {
      var favorite = [];
      $.each($("input[name='feature']:checked"), function(){
          favorite.push($(this).val());
      });
      var checkedValue = favorite.join("&");
      var Clink = $('.plan-feature-item').find(".saspot-btn").attr("data-link");
      $('.plan-feature-item').find(".saspot-btn").attr("href", Clink + checkedValue);
  });
  $(".input-checkbox").change(function(){
      if ($('.input-checkbox:checked').length == $('.input-checkbox').length) {
         $('.Minput-checkbox').prop('checked', true);
      } else {
         $('.Minput-checkbox').prop('checked', false);
      }
  });

  //SaaSpot Onclick Get Text Script
  $('.pricing-plan .nav-link').on('click', function() {
    $('.pricing-plan-title span').text($(this).text());
  });
  if($('.pricing-plan .nav-link').hasClass('active')) {
    $('.pricing-plan-title span').text($('.pricing-plan .nav-link.active').text());
  };

  // Accordion Active Only One At a Time.
  $('.collapse-others').each(function() {
    var $this = $(this);
    $('.collapse', $this).on('show.bs.collapse', function (e) {
      var all = $this.find('.collapse');
      var actives = $this.find('.collapsing, .collapse.show');
      all.each(function (index, element) {
        $(element).collapse('hide');
      })
      actives.each(function (index, element) {
        $(element).collapse('show');
      })
    });
  });

  //SaaSpot Tooltip script
  $('[data-toggle="tooltip"]').tooltip();
  var Tooltip = $.fn.tooltip.Constructor;
  $.extend( Tooltip.Default, {
    customClass: '',
  });
  var _show = Tooltip.prototype.show;
  Tooltip.prototype.show = function () {
    _show.apply(this,Array.prototype.slice.apply(arguments));
    if ( this.config.customClass ) {
      var tip = this.getTipElement();
      $(tip).addClass(this.config.customClass);
    }
  };

  //SaaSpot Add And Remove Class Script
  $('.saspot-full-page .saspot-toggle').on('click', function() {
    $('html').addClass('fullscreen-navigation-open');
  });
  $('.close-btn, .saspot-navigation-overlay').on('click', function() {
    $('html').removeClass('fullscreen-navigation-open');
  });
  $('html:has(.saspot-full-page)').addClass('has-full-page');

  //SaaSpot Navigation Hover Script
  $('.saspot-fullscreen-navigation .has-dropdown').hover (
    function() {
      $(this).find('ul.dropdown-nav').first().stop(false, false).slideDown(300);
    },
    function() {
      $(this).find('ul.dropdown-nav').first().stop(false, false).slideUp(300);
    }
  );

  //SaaSpot Popup Picture Script
  $('.saspot-popup').magnificPopup ({
    delegate: 'a',
    type: 'image',
    closeOnContentClick: false,
    closeBtnInside: false,
    mainClass: 'mfp-with-zoom mfp-img-mobile',
    closeMarkup:'<div class="mfp-close" title="%title%"></div>',
    image: {
      verticalFit: true,
      titleSrc: function(item) {
        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
      }
    },
    gallery: {
      enabled: true,
      arrowMarkup:'<div title="%title%" class="mfp-arrow mfp-arrow-%dir%"></div>',
    },
    zoom: {
      enabled: true,
      duration: 300,
      opener: function(element) {
        return element.find('*');
      }
    }
  });

  if ($('div').hasClass('saspot-popup')) {
    $('.saspot-popup').find('a').attr("data-elementor-open-lightbox","no");
  }
  if ($('div').hasClass('gdpr-privacy-bar')) {
    $('.gdpr-privacy-bar').addClass("saspot-cookies");
    $('button.gdpr-agreement').addClass("cookies-link");

  }

  //SaaSpot Back Top Script
  if ($('div').hasClass('saspot-back-top')) {
    var backtop = $('.saspot-back-top');
    var position = backtop.offset().top;
    $(window).scroll(function() {
      var windowposition = $(window).scrollTop();
      if(windowposition + $(window).height() == $(document).height()) {
        backtop.removeClass('active');
      }
      else if (windowposition > 150) {
        backtop.addClass('active');
      }
      else {
        backtop.removeClass('active');
      }
    });
    jQuery('.saspot-back-top a').on('click', function () {
      jQuery('body,html').animate ({
        scrollTop: 0
      }, 2000);
      return false;
    });
  }

});