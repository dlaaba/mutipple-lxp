<?php
/*
 * The template for displaying all pages.
 * Author & Copyright: VictorThemes
 * URL: https://victorthemes.com
 */

// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );

if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else { $saaspot_content_padding = ''; }
// Padding - Metabox
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
} else {
	$saaspot_custom_padding = '';
}

// Page Layout Options
$saaspot_woo_columns = cs_get_option('woo_product_columns');
$saaspot_woo_sidebar = cs_get_option('woo_sidebar_position');
$saaspot_woo_columns = $saaspot_woo_columns ? $saaspot_woo_columns : '3';

$saaspot_woo_widget = cs_get_option('woo_widget');
if ($saaspot_woo_widget) {
	$widget_select = $saaspot_woo_widget;
} else {
	if (is_active_sidebar( 'sidebar-shop' )) {
		$widget_select = 'sidebar-shop';
	} elseif (is_active_sidebar('sidebar-1')) {
		$widget_select = 'sidebar-1';
	} else {
		$widget_select = '';
	}
}

if ($widget_select && is_active_sidebar( $widget_select )) {
	if ($saaspot_woo_sidebar === 'left-sidebar') {
		$saaspot_column_class = 'saspot-primary';
		$saaspot_sidebar_class = 'saspot-left-sidebar';
	} elseif ($saaspot_woo_sidebar === 'sidebar-hide') {
		$saaspot_column_class = 'col-md-12';
		$saaspot_sidebar_class = 'saspot-hide-sidebar';
	} else {
		$saaspot_column_class = 'saspot-primary';
		$saaspot_sidebar_class = 'saspot-right-sidebar';
	}
} else {
	$saaspot_woo_sidebar = 'sidebar-hide';
	$saaspot_column_class = 'col-md-12';
	$saaspot_sidebar_class = 'saspot-hide-sidebar';
}

get_header(); ?>
<div class="saspot-mid-wrap mid-spacer-two <?php echo esc_attr($saaspot_content_padding); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
  <div class="container">
    <div class="woocommerce woocommerce-page">
      <div class="row">
			<?php do_action('saaspot_woocommerce_before_shop_loop'); ?>
				<div class="container saspot-content-area woo-col-<?php echo esc_attr($saaspot_woo_columns .' '. $saaspot_sidebar_class); ?>">
					<div class="row">
						<?php
						// Left Sidebar
						if($saaspot_woo_sidebar === 'left-sidebar') {
				   		get_sidebar('shop');
						}
						?>
						<div class="saspot-content-side <?php echo esc_attr($saaspot_column_class); ?>">
							<?php
								if ( have_posts() ) :
									woocommerce_content();
								endif; // End of the loop.
							?>
						</div><!-- Content Area -->
						<?php
						// Right Sidebar
						if($saaspot_woo_sidebar !== 'left-sidebar' && $saaspot_woo_sidebar !== 'sidebar-hide') {
							get_sidebar('shop');
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
