<?php
/*
 * The template for displaying the footer.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );

if ($saaspot_meta) {
	$saaspot_hide_footer  = $saaspot_meta['hide_footer'];
  $saaspot_hide_copyright = $saaspot_meta['hide_copyright'];
  $full_page  = $saaspot_meta['full_page'];
} else {
  $saaspot_hide_footer = '';
  $saaspot_hide_copyright = '';
  $full_page = '';
}
if (!$full_page) {
?>
</div><!-- Main Wrap Inner -->
<?php
if (is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4')) {
?>
<!-- Footer -->
<footer class="saspot-footer">
  <?php if (!$saaspot_hide_footer) { ?>
    <div class="container">
      <div class="footer-wrap">
        <?php get_template_part( 'layouts/footer/footer', 'widgets' ); ?>
      </div>
    </div>
  <?php do_action( 'saaspot_after_footer_action' ); // SaaSpot Action ?>
  <?php }
  if(!$saaspot_hide_copyright) { ?>
    <div class="saspot-copyright">
      <div class="container">
        <?php
        $need_copyright = cs_get_option('need_copyright');
        if($need_copyright) {
          get_template_part( 'layouts/footer/footer', 'copyright' );
        } ?>
      </div>
    </div>
  <?php do_action( 'saaspot_after_copyright_action' ); // SaaSpot Action ?>
  <?php } ?>
</footer>
<!-- Footer -->
<?php } else {
if(!$saaspot_hide_copyright) { ?>
<footer class="saspot-footer">
  <div class="saspot-copyright">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-12 textcenter">
          <p>&copy; <?php echo esc_html(date('Y')); ?>. <?php esc_html_e('Designed by', 'saaspot') ?> <a href="<?php echo esc_url(home_url( '/' )); ?>"><?php esc_html_e('VictorThemes', 'saaspot') ?></a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php	} } // Hide Footer Metabox ?>
</div><!-- Main Wrap -->
<?php
} // Full Page End
if ($full_page) { ?>
<!-- SaaSpot Fullscreen Navigation -->
<nav class="saspot-fullscreen-navigation">
  <div class="navigation-wrap">
    <div class="close-btn"><a href="javascript:void(0);"></a></div>
    <div class="saspot-navigation">
      <?php
        if ($saaspot_meta) {
          $full_menu = $saaspot_meta['full_page_menu'];
        } else { $full_menu = ''; }
        $full_menu = $full_menu ? $full_menu : '';
        wp_nav_menu(
          array(
            'menu'              => 'primary',
            'theme_location'    => 'primary',
            'container'         => '',
            'container_class'   => '',
            'container_id'      => '',
            'menu'              => $full_menu,
            'menu_class'        => '',
            'fallback_cb'       => 'Walker_Nav_Menu_Edit_Custom::fallback',
            'walker'            => new Walker_Nav_Menu_Edit_Custom()
          )
        );
      ?>
    </div>
  </div>
</nav>
<!-- SaaSpot Navigation Overlay -->
<div class="saspot-navigation-overlay"></div>
<?php }
$theme_preloader = cs_get_option('theme_preloader');
$theme_btotop = cs_get_option('theme_btotop');
if($theme_btotop) {
?>
<!-- Hanor Back Top -->
<div class="saspot-back-top">
  <a href="javascript:void(0);"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
</div>
<?php }
if ($theme_preloader) { ?>
<!-- Hanor Preloader -->
<div class="saspot-preloader">
  <div class="loader-wrap">
    <div class="loader">
      <div class="loader-inner ball-scale"></div>
    </div>
  </div>
</div>
<?php } ?>
<!-- SaaSpot Modal, Modal Video, Fade -->
<div class="modal modal-video fade" id="SaaSpotVideoModal">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item saspot-video-iframe" id="ModalVideoWrap"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
<?php
