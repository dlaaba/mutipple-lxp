<?php
/*
 * The template for displaying all single posts.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();

// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );

if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else { $saaspot_content_padding = ''; }
// Padding - Metabox
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
} else {
	$saaspot_custom_padding = '';
}

// Theme Options
$saaspot_sidebar_position = cs_get_option('single_sidebar_position');
$saaspot_single_blog_widget = cs_get_option('single_blog_widget');
if ($saaspot_single_blog_widget) {
	$widget_select = $saaspot_single_blog_widget;
} else {
	if (is_active_sidebar('sidebar-1')) {
		$widget_select = 'sidebar-1';
	} else {
		$widget_select = '';
	}
}
// Sidebar Position
if ($widget_select && is_active_sidebar( $widget_select )) {
	if ($saaspot_sidebar_position === 'sidebar-hide') {
		$layout_class = 'col-md-12';
		$saaspot_sidebar_class = 'hide-sidebar';
	} elseif ($saaspot_sidebar_position === 'sidebar-left') {
		$layout_class = 'saspot-primary';
		$saaspot_sidebar_class = 'left-sidebar';
	} else {
		$layout_class = 'saspot-primary';
		$saaspot_sidebar_class = 'right-sidebar';
	}
} else {
	$saaspot_sidebar_position = 'sidebar-hide';
	$layout_class = 'col-md-12';
	$saaspot_sidebar_class = 'hide-sidebar';
}
?>
<div class="saspot-blog <?php echo esc_attr($saaspot_content_padding .' '. $saaspot_sidebar_class); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
	<div class="container">
    <div class="row">
    <?php
	    if ($saaspot_sidebar_position === 'sidebar-left' && $saaspot_sidebar_position !== 'sidebar-hide') {
				get_sidebar(); // Sidebar
			}	?>
      <div class="<?php echo esc_attr($layout_class); ?>">
        <div class="saspot-unit-fix">
					<?php				
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							get_template_part( 'layouts/post/content', 'single' );
							if ( comments_open() || get_comments_number() ) :
			          comments_template();
			        endif;
						endwhile;
					else :
						get_template_part( 'layouts/post/content', 'none' );
					endif; ?>
				</div><!-- unit-fix -->
			</div><!-- layout -->
			<?php
				if ($saaspot_sidebar_position !== 'sidebar-hide') {
					get_sidebar(); // Sidebar
				}
			?>
		</div><!-- row -->
	</div><!-- container -->
</div><!-- mid -->
<?php
get_footer();
