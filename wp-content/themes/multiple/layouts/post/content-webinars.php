<?php
/*
 * Admin Styles for Sensation Theme
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
$webinars_meta  = get_post_meta( $saaspot_id, 'webinars_metabox', true );
if ($webinars_meta) {
  $webinars_video = $webinars_meta['webinars_video'];
} else {
  $webinars_video = '';
}

// Category
global $post;
$saaspot_terms = wp_get_post_terms($post->ID,'webinars_category');
foreach ($saaspot_terms as $term) {
  $saaspot_cat_class = 'webi-' . $term->slug;
}
$saaspot_count = count($saaspot_terms);
$i=0;
$saaspot_cat_class = '';
if ($saaspot_count > 0) {
  foreach ($saaspot_terms as $term) {
    $i++;
    $saaspot_cat_class .= 'webi-'. $term->slug .' ';
    if ($saaspot_count != $i) {
      $saaspot_cat_class .= '';
    } else {
      $saaspot_cat_class .= '';
    }
  }
}
$webinars_style = cs_get_option('webinars_style');
$video_text = cs_get_option('video_text');
$video_text = $video_text ? $video_text : esc_html__('WATCH NOW', 'saaspot');
$saaspot_webinars_aqr = cs_get_option('webinars_aqr');

// Featured Image
$saaspot_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$saaspot_large_image = $saaspot_large_image[0];
if ($saaspot_webinars_aqr) {
  $saaspot_featured_img = $saaspot_large_image;
} else {
  if ($webinars_style === 'two') {
    if(class_exists('Aq_Resize')) {
      $saaspot_webinars_img = aq_resize( $saaspot_large_image, '560', '360', true );
    } else {$saaspot_webinars_img = $saaspot_large_image;}
    $saaspot_featured_img = ( $saaspot_webinars_img ) ? $saaspot_webinars_img : SAASPOT_IMAGES . '/holders/560x360.png';
  } else {
    if(class_exists('Aq_Resize')) {
      $saaspot_webinars_img = aq_resize( $saaspot_large_image, '199', '184', true );
    } else {$saaspot_webinars_img = $saaspot_large_image;}
    $saaspot_featured_img = ( $saaspot_webinars_img ) ? $saaspot_webinars_img : SAASPOT_IMAGES . '/holders/200x185.png';
  }
}
if ($webinars_style === 'two') {
?>
<div class="col-md-6">
  <div class="market-example-item">
    <div class="video-wrap-inner">
      <div class="saspot-image">
        <?php if ($webinars_video) { ?>
        <a href="#0" id="myUrl" data-toggle="modal" data-src="<?php echo esc_url($webinars_video); ?>" data-target="#SaaSpotVideoModal" class="saspot-video-btn">
          <span class="video-btn"><i class="fa fa-play" aria-hidden="true"></i></span>
        </a>
        <?php } ?>
        <img src="<?php echo esc_url($saaspot_featured_img); ?>" alt="<?php echo esc_attr(get_the_title()); ?>">
      </div>
    </div>
    <div class="market-example-info">
      <div class="saspot-label">
        <?php
          $category_list = wp_get_post_terms($post->ID, 'webinars_category');
          foreach ($category_list as $term) {
            $webinars_term_link = get_term_link( $term );
            echo '<span><a href="'. esc_url($webinars_term_link) .'">'. esc_html($term->name) .'</a></span> ';
          }
        ?>
      </div>
      <h4 class="market-example-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(the_title()); ?></a></h4>
      <p><?php the_excerpt(); ?></p>
    </div>
  </div>
</div>
<?php } else { ?>
<div class="masonry-item <?php echo esc_attr($saaspot_cat_class); ?>" data-category="<?php echo esc_attr($saaspot_cat_class); ?>">
  <div class="webinar-item">
    <div class="saspot-image"><a href="<?php echo esc_url( get_permalink() ); ?>"><img src="<?php echo esc_url($saaspot_featured_img); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></a></div>
    <div class="webinar-info">
      <div class="saspot-label">
        <?php
          $category_list = wp_get_post_terms($post->ID, 'webinars_category');
          foreach ($category_list as $term) {
            $webinars_term_link = get_term_link( $term );
            echo '<span><a href="'. esc_url($webinars_term_link) .'">'. esc_html($term->name) .'</a></span> ';
          }
        ?>
      </div>
      <h3 class="webinar-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(the_title()); ?></a></h3>
      <p><?php the_excerpt(); ?></p>
      <?php if ($webinars_video) { ?>
      <div class="saspot-link-wrap link-wrap-style-two"><a href="#0" data-toggle="modal" data-src="<?php echo esc_url($webinars_video); ?>" data-target="#SaaSpotVideoModal" class="saspot-link"><i class="fa fa-play-circle-o" aria-hidden="true"></i> <?php echo esc_html($video_text); ?></a></div>
      <?php } ?>
    </div>
  </div>
</div>
<?php }
