<?php
/*
 * Admin Styles for Sensation Theme
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

// Category
global $post;
$saaspot_terms = wp_get_post_terms($post->ID,'apps_category');
foreach ($saaspot_terms as $term) {
  $saaspot_cat_class = 'cat-' . $term->slug;
}
$saaspot_count = count($saaspot_terms);
$i=0;
$saaspot_cat_class = '';
if ($saaspot_count > 0) {
  foreach ($saaspot_terms as $term) {
    $i++;
    $saaspot_cat_class .= 'cat-'. $term->slug .' ';
    if ($saaspot_count != $i) {
      $saaspot_cat_class .= '';
    } else {
      $saaspot_cat_class .= '';
    }
  }
}
$saaspot_apps_aqr = cs_get_option('apps_aqr');
// Featured Image
$saaspot_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$saaspot_large_image = $saaspot_large_image[0];

if ($saaspot_apps_aqr) {
  $saaspot_featured_img = $saaspot_large_image;
} else {
  if(class_exists('Aq_Resize')) {
    $saaspot_apps_img = aq_resize( $saaspot_large_image, '300', '230', true );
  } else {$saaspot_apps_img = $saaspot_large_image;}
  $saaspot_featured_img = ( $saaspot_apps_img ) ? $saaspot_apps_img : SAASPOT_IMAGES . '/holders/300x230.png';
}
?>

<div class="masonry-item <?php echo esc_attr($saaspot_cat_class); ?>" data-category="<?php echo esc_attr($saaspot_cat_class); ?>">
  <div class="app-item">
    <div class="saspot-image">
      <a href="<?php echo esc_url( get_permalink() ); ?>"><img src="<?php echo esc_url($saaspot_featured_img); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></a>
      <div class="saspot-label">
        <?php
          $category_list = wp_get_post_terms($post->ID, 'apps_category');
          foreach ($category_list as $term) {
          $apps_term_link = get_term_link( $term );
            echo '<span><a href="'. esc_url($apps_term_link) .'">'. esc_html($term->name) .'</a></span> ';
          }
        ?>
      </div>
    </div>
    <div class="app-info">
      <h4 class="app-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(the_title()); ?></a></h4>
      <p><?php the_excerpt(); ?></p>
    </div>
  </div>
</div>
<?php
