<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php
$saaspot_all_element_color  = cs_get_customize_option( 'all_element_colors' );
?>
<meta name="msapplication-TileColor" content="<?php echo esc_attr($saaspot_all_element_color); ?>">
<meta name="theme-color" content="<?php echo esc_attr($saaspot_all_element_color); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
wp_head();
if (is_elementor()) {
  $layout_class = 'container-fluid';
} else {
  $layout_class = 'container';
}
?>
</head>
	<body <?php body_class(); ?>>
    <div class="saspot-mid-wrap vt-maintenance-mode">
      <div class="<?php echo esc_attr($layout_class); ?>">
        <div class="row">
         	<div class="col-md-12">
            <?php
              $saaspot_page = get_post( cs_get_option('maintenance_mode_page') );
              if (is_elementor()) {
                echo ( is_object( $saaspot_page ) ) ? do_shortcode( '[saaspot_elementor_template id="'.$saaspot_page->ID.'"]' ) : '';
              } else {
                echo ( is_object( $saaspot_page ) ) ? do_shortcode( $saaspot_page->post_content ) : '';
              }
            ?>
          </div>
        </div>
      </div>
    </div>
    <?php wp_footer(); ?>
  </body>
</html>
<?php
