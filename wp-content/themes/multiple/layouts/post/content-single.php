<?php
/**
 * Single Post.
 */
// Metabox
global $post;

$saaspot_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$saaspot_large_image = $saaspot_large_image[0];

$date_format = cs_get_option('blog_date_format');
$date_format_actual = $date_format ? $date_format : '';
// Single Theme Option
$saaspot_single_tag_list = cs_get_option('single_tag_list');
$saaspot_single_author_info = cs_get_option('single_author_info');

$cat_list = get_the_category();
$tag_list = get_the_tags();
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('saspot-blog-post'); ?>>
	<div class="blog-detail-wrap">    
    <?php do_action( 'saaspot_before_single_content_action' ); // SaaSpot Action ?>
		<?php if ($saaspot_large_image) { ?>
	  <div class="blog-image">
	    <img src="<?php echo esc_url($saaspot_large_image); ?>" alt="<?php echo esc_attr(get_the_title()); ?>">
	  </div>
		<?php } ?>          
    <!-- Content -->
		<?php
		the_content();
		echo saaspot_wp_link_pages();
		?>
		<!-- Content -->
		<!-- SaaSpotWP -->
		<?php if ( $saaspot_single_tag_list && $tag_list ) { ?>
    <div class="blog-tags">
    	<?php
	    $tags = get_the_tags();
      foreach ( $tags as $tag ) : ?>
          <a href="<?php echo esc_url( get_tag_link( $tag->term_id ) ); ?>"><span class="saspot-label"><?php echo esc_html( $tag->name ); ?></span></a>
      <?php endforeach; ?>
    </div>
		<?php } ?>
    <?php do_action( 'saaspot_after_single_content_action' ); // SaaSpot Action ?>
  </div>
	<!-- Author Info -->
	<?php	if(!$saaspot_single_author_info) {} else { echo saaspot_author_info(); } ?>
	<!-- Related Articles -->
	<?php
	$single_related_post = cs_get_option('single_related_post');
	$single_related_title = cs_get_option('single_related_title');
	$single_related_limit = cs_get_option('single_related_limit');
	$single_related_title = $single_related_title ? $single_related_title : esc_html__( 'Related Posts', 'saaspot' );
	$single_related_limit = $single_related_limit ? $single_related_limit : '2';

	if(!$single_related_post) {} else {
	$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => $single_related_limit, 'post__not_in' => array($post->ID) ) );
	if ($related) { ?>
	<div class="saspot-related-articles blog-style-two">
	  <h3 class="related-articles-title"><?php echo esc_html($single_related_title); ?></h3>
    <div class="row">
    <?php do_action( 'saaspot_before_related_post_action' ); // SaaSpot Action ?>
    <?php
		if( $related ) foreach( $related as $post ) {
		setup_postdata($post);
    $related_large_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
    $related_large_image = $related_large_image[0];
		?>
      <div class="col-sm-6">
        <div class="blog-item">
				  <?php if ($related_large_image) { ?>
					  <div class="saspot-image">
					    <img src="<?php echo esc_url($related_large_image); ?>" alt="<?php echo esc_attr(get_the_title()); ?>">
					  </div>
					<?php } ?>
				  <div class="blog-info">
			  		<?php if ( $cat_list ) { ?>
			        <div class="blog-cats">
				        <?php
						    $categories = get_the_category();
				        foreach ( $categories as $category ) : ?>
				            <a href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>"><span class="saspot-label"><?php echo esc_html( $category->name ); ?></span></a>
				        <?php endforeach; ?>       
			        </div>
			      <?php } ?>
				    <h4 class="blog-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a></h4>
				    <?php
							$blog_excerpt = cs_get_option('theme_blog_excerpt');
							if ($blog_excerpt) {
								$blog_excerpt = $blog_excerpt;
							} else {
								$blog_excerpt = '55';
							}
							echo '<p>';
							saaspot_excerpt($blog_excerpt);
							echo '</p>';
							echo saaspot_wp_link_pages();
						?>
				    <div class="saspot-meta">
				      <div class="row align-items-center">
				        <div class="col-md-6">
				          <div class="author">
				          	<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
					            <?php echo get_avatar( get_the_author_meta( 'ID' ), 35 ); ?>
					            <span><?php echo esc_html(get_the_author()); ?></span>
					          </a>
				        </div>
				        </div>
				        <div class="col-md-6">
				          <div class="blog-date">
				            <ul>
				              <li><?php echo get_the_date($date_format_actual); ?></li>
				              <?php if (shortcode_exists( 'rt_reading_time' )) { ?><li><?php echo do_shortcode('[rt_reading_time postfix="mins read" postfix_singular="min read"]'); ?></li><?php } ?> <!-- SaaSpotWP -->
				            </ul>
				          </div>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
      </div>
		<?php	} wp_reset_postdata(); ?>
    <?php do_action( 'saaspot_after_related_post_action' ); // SaaSpot Action ?>
    </div>
	</div>
	<?php	} } ?>
</div><!-- #post-## -->
<?php
