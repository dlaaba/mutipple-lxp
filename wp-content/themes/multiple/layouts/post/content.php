<?php
/**
 * Template part for displaying posts.
 */
// Metabox
global $post;

$saaspot_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$saaspot_large_image = $saaspot_large_image[0];

$saaspot_blog_style = cs_get_option('blog_listing_style');
$saaspot_metas_hide = (array) cs_get_option( 'theme_metas_hide' );
$saaspot_blog_columns = cs_get_option('blog_listing_columns');
$date_format = cs_get_option('blog_date_format');
$saaspot_blog_aqr = cs_get_option('blog_aqr');
$saaspot_read_more_text = cs_get_option('read_more_text');
$saaspot_read_text = $saaspot_read_more_text ? $saaspot_read_more_text : esc_html__( 'READ MORE', 'saaspot' );
$date_format_actual = $date_format ? $date_format : '';
// Columns
if ($saaspot_blog_columns === 'col-2') {
	$saaspot_blog_col_class = 'col-md-6 col-sm-6';
} else {
	$saaspot_blog_col_class = 'col-lg-4 col-md-6 col-sm-12';
}

if ($saaspot_blog_style === 'style-two') {
	$layout_class = $saaspot_blog_col_class;
} else {
	$layout_class = 'blog-item-wrap';
}
if ($saaspot_blog_aqr) {
	$featured_img = $saaspot_large_image;
} else {
	if ($saaspot_blog_style === 'style-two') {
		if ($saaspot_blog_columns === 'col-2') {
	    if(class_exists('Aq_Resize')) {
	      $blog_img = aq_resize( $saaspot_large_image, '570', '370', true );
	    } else {$blog_img = $saaspot_large_image;}
	    $featured_img = ( $blog_img ) ? $blog_img : SAASPOT_IMAGES . '/holders/570x370.png';
		} else {
	  	if(class_exists('Aq_Resize')) {
				$blog_img = aq_resize( $saaspot_large_image, '370', '330', true );
	    } else {$blog_img = $saaspot_large_image;}
			$featured_img = ( $blog_img ) ? $blog_img : SAASPOT_IMAGES . '/holders/370x330.png';
		}
	} else {
		if(class_exists('Aq_Resize')) {
			$blog_img = aq_resize( $saaspot_large_image, '800', '380', true );
	   } else {$blog_img = $saaspot_large_image;}
		$featured_img = ( $blog_img ) ? $blog_img : $saaspot_large_image;
	}
}

if(is_sticky()) {
  $sticky_class = ' sticky';
} else {
  $sticky_class = '';
}
$cat_list = get_the_category();
?>
<div class="<?php echo esc_attr($layout_class); ?>">
	<div class="blog-item saspot-item<?php echo esc_attr($sticky_class); ?>">
		<div id="post-<?php the_ID(); ?>" <?php post_class('saspot-blog-post'); ?>>
		  <?php if ($saaspot_large_image) { ?>
			  <div class="saspot-image">
			    <a href="<?php echo esc_url( get_permalink() ); ?>"><img src="<?php echo esc_url($featured_img); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></a>
			  </div>
			<?php } ?>
		  <div class="blog-info">
		  	<div class="row">
		  		<?php if ( !in_array( 'category', $saaspot_metas_hide ) && $cat_list ) { ?>
		      <div class="col-md-12">
		        <div class="blog-cats">
			        <?php
					    $categories = get_the_category();
			        foreach ( $categories as $category ) : ?>
			            <a href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>"><span class="saspot-label"><?php echo esc_html( $category->name ); ?></span></a>
			        <?php endforeach; ?>       
		        </div>
		      </div>
		      <?php } ?>
		    </div>
		    <h4 class="blog-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a></h4>
		    <?php
					$blog_excerpt = cs_get_option('theme_blog_excerpt');
					if ($blog_excerpt) {
						$blog_excerpt = $blog_excerpt;
					} else {
						$blog_excerpt = '55';
					}
					echo '<p>';
					saaspot_excerpt($blog_excerpt);
					echo '</p>';
					echo saaspot_wp_link_pages();
				?>
				<div class="saspot-link-wrap">
					<a href="<?php echo esc_url( get_permalink() ); ?>" class="saspot-link"><?php echo esc_html($saaspot_read_text); ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
				</div>
		    <div class="saspot-meta">
		      <div class="row align-items-center">
		        <div class="col-md-6">
		        	<?php if ( !in_array( 'author', $saaspot_metas_hide ) ) { ?>
		          <div class="author"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
		            <?php echo get_avatar( get_the_author_meta( 'ID' ), 35 ); ?>
		            <span><?php echo esc_html(get_the_author()); ?></span>
		          </a></div>
		          <?php } ?>
		        </div>
		        <div class="col-md-6">
		          <div class="blog-date">
		            <ul>
		              <?php if ( !in_array( 'date', $saaspot_metas_hide ) ) { ?><li><?php echo get_the_date($date_format_actual); ?></li><?php } ?>
		              <?php if (shortcode_exists( 'rt_reading_time' )) { ?><li><?php echo do_shortcode('[rt_reading_time postfix="mins read" postfix_singular="min read"]'); ?></li><?php } ?> <!-- SaaSpotWP -->
		            </ul>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div><!-- #post-## -->
<?php
