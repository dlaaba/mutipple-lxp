<?php
// Logo Image
$saaspot_brand_logo_default = cs_get_option('brand_logo_default');

// Metabox - Header Transparent
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
// Retina Size
$saaspot_retina_width = cs_get_option('retina_width');
$saaspot_retina_height = cs_get_option('retina_height');
// Logo Spacings
$saaspot_brand_logo_top = cs_get_option('brand_logo_top');
$saaspot_brand_logo_bottom = cs_get_option('brand_logo_bottom');
if ($saaspot_brand_logo_top !== '') {
	$saaspot_brand_logo_top = $saaspot_brand_logo_top ? 'padding-top:'. saaspot_check_px($saaspot_brand_logo_top) .';' : '';
} else { $saaspot_brand_logo_top = ''; }
if ($saaspot_brand_logo_bottom !== '') {
	$saaspot_brand_logo_bottom = $saaspot_brand_logo_bottom ? 'padding-bottom:'. saaspot_check_px($saaspot_brand_logo_bottom) .';' : '';
} else { $saaspot_brand_logo_bottom = ''; }

$saaspot_brand_logo_top = $saaspot_brand_logo_top ? $saaspot_brand_logo_top : '';
$saaspot_brand_logo_bottom = $saaspot_brand_logo_bottom ? $saaspot_brand_logo_bottom : '';
if ($saaspot_meta) {
  $full_page  = $saaspot_meta['full_page'];
} else { 
  $full_page = ''; 
} 
?>
<div class="saspot-brand" style="<?php echo esc_attr($saaspot_brand_logo_top); echo esc_attr($saaspot_brand_logo_bottom); ?>">
  <?php do_action( 'saaspot_before_logo_action' ); // SaaSpot Action ?>
	<a href="<?php echo esc_url(home_url( '/' )); ?>">
	<?php
	
		if (isset($saaspot_brand_logo_default)) {
			echo '<img src="'. esc_url(wp_get_attachment_url($saaspot_brand_logo_default)) .'" alt="'. esc_attr(get_bloginfo( 'name' )) . '" class="default-logo normal-logo" width="'. esc_attr($saaspot_retina_width) .'" height="'. esc_attr($saaspot_retina_height) .'">';
		} else {
			echo '<div class="text-logo">'. esc_html(get_bloginfo( 'name' )) . '</div>';
		}
	echo '</a>';
	?>
  <?php do_action( 'saaspot_after_logo_action' ); // SaaSpot Action ?>
</div>
<?php 
$header_wpml_info = cs_get_option('header_wpml_info');
$header_contact = cs_get_option('header_contact');
if (!$full_page) {
if ($header_wpml_info || $header_contact) {
?>
<div class="header-contact nice-select-two">
  <ul>
    <li><?php echo do_shortcode($header_wpml_info); ?></li> <!-- SaaSpotWP -->
    <li><?php echo do_shortcode($header_contact); ?></li> <!-- SaaSpotWP -->
  </ul>
  <?php do_action( 'saaspot_after_logo_meta_action' ); // SaaSpot Action ?>
</div>
<?php } }
