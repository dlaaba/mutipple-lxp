<?php
// Topbar options
$saaspot_top_left = cs_get_option('top_left');
$saaspot_top_right = cs_get_option('top_right');

$saaspot_hide_topbar = cs_get_option('top_bar');
if ($saaspot_hide_topbar === true ) {
  $saaspot_hide_topbar = 'hide';
} else {
  $saaspot_hide_topbar = 'show';
}

if($saaspot_hide_topbar === 'show') {
if($saaspot_top_left || $saaspot_top_right ) {
?>
<div class="saspot-topbar">
  <div class="row align-items-center">
    <div class="col-md-6">
      <?php do_action( 'saaspot_before_top_left_action' ); // SaaSpot Action ?>
    	<?php echo do_shortcode($saaspot_top_left); ?>
      <?php do_action( 'saaspot_after_top_left_action' ); // SaaSpot Action ?>
  	</div> <!-- SaaSpotWP -->
    <div class="col-md-6 textright">
      <?php do_action( 'saaspot_before_top_right_action' ); // SaaSpot Action ?>
    	<?php echo do_shortcode($saaspot_top_right); ?>
      <?php do_action( 'saaspot_after_top_right_action' ); // SaaSpot Action ?>
  	</div> <!-- SaaSpotWP -->
  </div>
</div>
<?php } } // Hide Topbar - From Metabox
