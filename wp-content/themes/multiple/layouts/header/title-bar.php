<?php
// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_id    = ( !is_tag() && !is_archive() && !is_search()) ? $saaspot_id : false;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
if ($saaspot_meta) {
  $title_style = $saaspot_meta['title_style'];
	$saaspot_title_bar_padding = $saaspot_meta['title_area_spacings'];
  $title_content = $saaspot_meta['title_content'];
  $saaspot_bg_overlay_color = $saaspot_meta['titlebar_bg_overlay_color'];
  $hide_parallax = $saaspot_meta['hide_parallax'];
  $title_image = $saaspot_meta['title_image'];
  $btn_image = $saaspot_meta['btn_image'];
  $title_area_bg_size = $saaspot_meta['title_area_bg_size'];
  $content_size = $saaspot_meta['content_size'];
  $content_width = $saaspot_meta['content_width'];
  $button_text = $saaspot_meta['button_text'];
  $button_link = $saaspot_meta['button_link'];
  $conferenceby_text = $saaspot_meta['conferenceby_text'];
  $conference_image = $saaspot_meta['conference_image'];
  $conference_text = $saaspot_meta['conference_text'];
  $need_share = $saaspot_meta['need_share'];
  $overlay_image = $saaspot_meta['overlay_image'];
  $title_icon = $saaspot_meta['title_icon'];
} else {
  $title_style = '';
	$saaspot_title_bar_padding = '';
  $title_content = '';
  $saaspot_bg_overlay_color = cs_get_option('titlebar_bg_overlay_color');
  $hide_parallax = '';
  $title_image = '';
  $title_area_bg_size = '';
  $content_size = '';
  $content_width = '';
  $btn_image = '';
  $button_text = '';
  $button_link = '';
  $conferenceby_text = '';
  $conference_image = '';
  $conference_text = '';
  $need_share = '';
  $overlay_image = '';
  $title_icon = '';
}
$job_options = get_post_meta( get_the_ID(), 'job_options', true );
if ($job_options) {
  $job_location = $job_options['job_location'];
  $job_type = $job_options['job_type'];
} else {
  $job_location = '';
  $job_type = '';
}

// Padding - Theme Options
if ($saaspot_title_bar_padding && $saaspot_title_bar_padding !== 'padding-default') {
	$saaspot_title_top_spacings = $saaspot_meta['title_top_spacings'];
	$saaspot_title_bottom_spacings = $saaspot_meta['title_bottom_spacings'];
	if ($saaspot_title_bar_padding === 'padding-custom') {
		$saaspot_title_top_spacings = $saaspot_title_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_title_top_spacings) .';' : '';
		$saaspot_title_bottom_spacings = $saaspot_title_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_title_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_title_top_spacings . $saaspot_title_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
} else {
	$saaspot_title_bar_padding = cs_get_option('title_bar_padding');
	$saaspot_titlebar_top_padding = cs_get_option('titlebar_top_padding');
	$saaspot_titlebar_bottom_padding = cs_get_option('titlebar_bottom_padding');
	if ($saaspot_title_bar_padding === 'padding-custom') {
		$saaspot_titlebar_top_padding = $saaspot_titlebar_top_padding ? 'padding-top:'. saaspot_check_px($saaspot_titlebar_top_padding) .';' : '';
		$saaspot_titlebar_bottom_padding = $saaspot_titlebar_bottom_padding ? 'padding-bottom:'. saaspot_check_px($saaspot_titlebar_bottom_padding) .';' : '';
		$saaspot_custom_padding = $saaspot_titlebar_top_padding . $saaspot_titlebar_bottom_padding;
	} else {
		$saaspot_custom_padding = '';
	}
}

// Banner Type - Meta Box
if ($saaspot_meta) {
	$saaspot_banner_type = $saaspot_meta['banner_type'];
} else { $saaspot_banner_type = ''; }

// Overlay Color
if ($saaspot_meta && $saaspot_bg_overlay_color) {
  $saaspot_bg_overlay_color = $saaspot_meta['titlebar_bg_overlay_color'];
} else {
  $saaspot_bg_overlay_color = cs_get_option('titlebar_bg_overlay_color');
}
$overlay_image = wp_get_attachment_url($overlay_image);
if ($overlay_image) {
  $lay_image = 'background-image:url('.esc_url($overlay_image).')';
} else {
  $lay_image = '';
}
if ($saaspot_bg_overlay_color || $overlay_image) {
	$saaspot_overlay_color = 'style=background-color:'.$saaspot_bg_overlay_color.';'.$lay_image;
} else {
	$saaspot_overlay_color = '';
}
if ($content_size) {
  $saaspot_content_size = 'style=font-size:'.$content_size;
} else {
  $saaspot_content_size = '';
}

if ($content_width) {
  $saaspot_content_width = 'style=max-width:'.$content_width;
} else {
  $saaspot_content_width = '';
}

if ($title_area_bg_size) {
  $saaspot_bg_size = 'background-size:'.$title_area_bg_size;
} else {
  $saaspot_bg_size = '';
}

// Button Shortcode
if ($saaspot_meta) {

}

// Background - Type
if( $saaspot_meta && isset( $saaspot_meta['title_area_bg'] ) ) {
  extract( $saaspot_meta['title_area_bg'] );
  if ($image) {
    $saaspot_background_image       = ( ! empty( $image ) ) ? 'background-image: url(' . $image . ');' : '';
    $saaspot_background_repeat      = ( ! empty( $image ) && ! empty( $repeat ) ) ? ' background-repeat: ' . $repeat . ';' : '';
    $saaspot_background_position    = ( ! empty( $image ) && ! empty( $position ) ) ? ' background-position: ' . $position . ';' : '';
    $saaspot_background_size        = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-size: ' . $size . ';' : '';
    $saaspot_background_attachment  = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-attachment: ' . $attachment . ';' : '';
    $saaspot_background_color       = ( ! empty( $color ) ) ? ' background-color: ' . $color . ';' : '';
    $saaspot_background_style       = ( ! empty( $image ) ) ? $saaspot_background_image . $saaspot_background_repeat . $saaspot_background_position . $saaspot_background_size . $saaspot_background_attachment : '';
    $saaspot_title_bg = ( ! empty( $saaspot_background_style ) || ! empty( $saaspot_background_color ) ) ? $saaspot_background_style . $saaspot_background_color : '';
  } else {
  	$saaspot_title_bgg = cs_get_option('titlebar_bg');
    if ($saaspot_title_bgg) {
    extract( $saaspot_title_bgg );
     $saaspot_background_image       = ( ! empty( $image ) ) ? 'background-image: url(' . $image . ');' : '';
     $saaspot_background_repeat      = ( ! empty( $image ) && ! empty( $repeat ) ) ? ' background-repeat: ' . $repeat . ';' : '';
     $saaspot_background_position    = ( ! empty( $image ) && ! empty( $position ) ) ? ' background-position: ' . $position . ';' : '';
     $saaspot_background_size        = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-size: ' . $size . ';' : '';
     $saaspot_background_attachment  = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-attachment: ' . $attachment . ';' : '';
     $saaspot_background_color       = ( ! empty( $color ) ) ? ' background-color: ' . $color . ';' : '';
     $saaspot_background_style       = ( ! empty( $image ) ) ? $saaspot_background_image . $saaspot_background_repeat . $saaspot_background_position . $saaspot_background_size . $saaspot_background_attachment : '';
     $saaspot_title_bg = ( ! empty( $saaspot_background_style ) || ! empty( $saaspot_background_color ) ) ? $saaspot_background_style . $saaspot_background_color : '';
     } else {
      $saaspot_title_bg = '';
     }
  }

} else {
$saaspot_title_bgg = cs_get_option('titlebar_bg');
  if ($saaspot_title_bgg) {
  extract( $saaspot_title_bgg );
   $saaspot_background_image       = ( ! empty( $image ) ) ? 'background-image: url(' . $image . ');' : '';
   $saaspot_background_repeat      = ( ! empty( $image ) && ! empty( $repeat ) ) ? ' background-repeat: ' . $repeat . ';' : '';
   $saaspot_background_position    = ( ! empty( $image ) && ! empty( $position ) ) ? ' background-position: ' . $position . ';' : '';
   $saaspot_background_size        = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-size: ' . $size . ';' : '';
   $saaspot_background_attachment  = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-attachment: ' . $attachment . ';' : '';
   $saaspot_background_color       = ( ! empty( $color ) ) ? ' background-color: ' . $color . ';' : '';
   $saaspot_background_style       = ( ! empty( $image ) ) ? $saaspot_background_image . $saaspot_background_repeat . $saaspot_background_position . $saaspot_background_size . $saaspot_background_attachment : '';

  $saaspot_title_bg = ( ! empty( $saaspot_background_style ) || ! empty( $saaspot_background_color ) ) ? $saaspot_background_style . $saaspot_background_color : '';
  } else {
  $saaspot_title_bg = '';
  }
 }

if ($hide_parallax || is_404()) {
  $parallax_cls = '';
} else {
  $parallax_cls = ' saspot-parallax';
}

if ($title_style === 'two') {
  $style_cls = ' page-title-style-two';
} elseif ($title_style === 'three') {
  $style_cls = ' page-title-style-two page-title-style-three';
} elseif (is_singular( 'job' )) {
  $style_cls = ' page-title-style-five';
} else {
  $style_cls = '';
}

$error_page_title = cs_get_option('error_page_title');
$error_title_content = cs_get_option('error_title_content');
$title = $error_page_title ? $error_page_title : '404';

if( is_404() ) {
  $title_content = $error_title_content ? $error_title_content : '';
} else {
  $title_content = $title_content;
}

if($saaspot_banner_type === 'hide-title-area') { // Hide Title Area
} elseif($saaspot_meta && $saaspot_banner_type === 'revolution-slider') {
   echo do_shortcode($saaspot_meta['page_revslider']);
} elseif($saaspot_meta && $saaspot_banner_type === 'elementor-templates') {
   echo do_shortcode('[saaspot_elementor_template id="'.$saaspot_meta['ele_templates'].'"]');
} else { ?> <!-- SaaSpotWP -->
<!-- Hanor Page Title, Hanor Parallax -->
<div class="saspot-page-title<?php echo esc_attr($parallax_cls.$style_cls); ?> <?php echo esc_attr($saaspot_title_bar_padding); ?>" style="<?php echo esc_attr($saaspot_custom_padding . $saaspot_title_bg . $saaspot_bg_size); ?>">
  <div class="parallax-overlay" <?php echo esc_attr($saaspot_overlay_color); ?>></div>

  <div class="container">
  <?php do_action( 'saaspot_before_title_action' ); // SaaSpot Action ?>
    <?php if($title_style === 'three') { ?>

    <div class="row">
      <div class="col-lg-6 col-md-7">
        <div class="page-title-wrap" <?php echo esc_attr($saaspot_content_width); ?>>
          <h2 class="page-title"><?php echo esc_html(saaspot_title_area()); ?></h2>
          <?php if($title_content && $title_style !== 'four') { ?>
            <p <?php echo esc_attr($saaspot_content_size); ?>><?php echo esc_html($title_content); ?></p>
          <?php }
          $btn_image = wp_get_attachment_url($btn_image);
          if($btn_image && $button_link) { ?>
          <div class="app-link"><a href="<?php echo esc_url($button_link); ?>"><img src="<?php echo esc_url($btn_image); ?>" alt="Google Play"></a></div>
          <?php }
          if($button_text && $button_link) { ?>
            <div class="saspot-btn-wrap"><a href="<?php echo esc_url($button_link); ?>" class="saspot-btn saspot-light-blue-btn"><?php echo esc_html($button_text); ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
          <?php } ?>
        </div>
      </div>
      <div class="col-lg-6 col-md-5 textright">
        <?php if($title_image) {
        $image = wp_get_attachment_url($title_image)  ?>
        <div class="saspot-image wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
          <img src="<?php echo esc_url($image); ?>" alt="Agencies">
        </div>
        <?php } ?>
      </div>
    </div>

    <?php } else { ?>

    <div class="page-title-wrap" <?php echo esc_attr($saaspot_content_width); ?>>
      <?php
      if(is_singular( 'job' )) {
      $category_list = wp_get_post_terms($post->ID, 'job_role');
      if($category_list) { ?>
        <p>
          <?php
            foreach ($category_list as $term) {
              echo '<span>'.esc_html($term->name).'</span> ';
            }
          ?>
        </p>
      <?php } }
      if (is_singular( 'post' )) {
      $date_format = cs_get_option('blog_date_format');
      $date_format_actual = $date_format ? $date_format : '';
      ?>
        <div class="blog-date">
          <ul>
            <?php do_action( 'saaspot_before_title_meta_action' ); // SaaSpot Action ?>
            <li>
              <?php
              $categories = get_the_category();
              foreach ( $categories as $category ) : ?>
                  <a href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>" class="cat"><?php echo esc_html( $category->name ); ?></a>
              <?php endforeach; ?>
            </li>
            <li><?php echo get_the_date($date_format_actual); ?></li>
            <?php if (shortcode_exists( 'rt_reading_time' )) { ?><li><?php echo do_shortcode('[rt_reading_time postfix="mins read" postfix_singular="min read"]'); ?></li><?php } ?> <!-- SaaSpotWP -->
            <?php do_action( 'saaspot_after_title_meta_action' ); // SaaSpot Action ?>
          </ul>
        </div>
      <?php }
      $tl_icon = wp_get_attachment_url($title_icon);
      if ($tl_icon) { ?>
      <div class="saspot-icon">
        <img src="<?php echo esc_url($tl_icon); ?>" alt="Icon">
      </div>
      <?php } if( is_404() ) { ?>
        <h2 class="page-title"><?php echo esc_html($title); ?></h2>
      <?php } else { ?>
        <h2 class="page-title"><?php echo esc_html(saaspot_title_area()); ?></h2>
      <?php }
      if (is_singular( 'job' )) {
        if($job_location || $job_type) { ?>
          <h3 class="page-subtitle"><?php echo esc_html($job_location); if ($job_type) { ?> - <?php echo esc_html($job_type); } ?></h3>
        <?php }
      }
      if($title_content && !is_singular( 'job' )) { ?>
        <p <?php echo esc_attr($saaspot_content_size); ?>><?php echo do_shortcode($title_content); ?></p>
      <?php }
      if ($need_share) { saaspot_title_share_option(); }
      if($button_text || $button_link) { ?>
        <div class="saspot-btn-wrap"><a href="<?php echo esc_url($button_link); ?>" class="saspot-btn saspot-light-blue-btn"><?php echo esc_html($button_text); ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
      <?php }
      if($conferenceby_text || $conference_text) {
      $conference_image = wp_get_attachment_url($conference_image)  ?>
      <div class="event-conference"><?php echo esc_html($conferenceby_text); if($conference_image) { ?><img src="<?php echo esc_url($conference_image); ?>" width="19" alt="SaaSpot"><?php } echo esc_html($conference_text); ?></div>
    <?php } ?>
    </div>
    <?php if($title_image) {
    $image = wp_get_attachment_url($title_image);  ?>
    <div class="saspot-image wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
      <img src="<?php echo esc_url($image); ?>" alt="Agencies">
    </div>
    <?php } } ?>
  <?php do_action( 'saaspot_after_title_action' ); // SaaSpot Action ?>
  </div>

</div>

<?php }
