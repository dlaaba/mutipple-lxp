<?php
// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $saaspot_id : false;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
// Header Style - ThemeOptions & Metabox

$header_btns = cs_get_option('header_btns');
$saaspot_mobile_breakpoint = cs_get_option('mobile_breakpoint');
$saaspot_breakpoint = $saaspot_mobile_breakpoint ? $saaspot_mobile_breakpoint : '1199';
?>
<!-- Navigation & Search -->
<?php do_action( 'saaspot_before_menu_action' ); // SaaSpot Action ?>
<nav class="saspot-navigation" data-nav="<?php echo esc_attr($saaspot_breakpoint); ?>">
<?php
  if ($saaspot_meta) {
    $saaspot_choose_menu = $saaspot_meta['choose_menu'];
  } else { $saaspot_choose_menu = ''; }
  $saaspot_choose_menu = $saaspot_choose_menu ? $saaspot_choose_menu : '';
  wp_nav_menu(
    array(
      'menu'              => 'primary',
      'theme_location'    => 'primary',
      'container'         => '',
      'container_class'   => '',
      'container_id'      => '',
      'menu'              => $saaspot_choose_menu,
      'menu_class'        => '',
      'fallback_cb'       => 'Walker_Nav_Menu_Edit_Custom::fallback',
      'walker'            => new Walker_Nav_Menu_Edit_Custom()
    )
  );
?>
</nav> <!-- Container -->
<?php do_action( 'saaspot_after_menu_action' ); // SaaSpot Action ?>
<?php echo do_shortcode($header_btns); ?> <!-- SaaSpotWP -->

