<!-- Footer Widgets -->
<?php 
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
?>
<div class="row">
	<?php echo saaspot_vt_footer_widgets(); ?>
</div>
<!-- Footer Widgets -->
<?php
