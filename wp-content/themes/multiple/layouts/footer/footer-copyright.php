<?php
	// Main Text
	$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
	$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
	$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
	$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );

$left_copyright_text = cs_get_option('copyright_text');
$middle_copyright_text = cs_get_option('secondary_text');
$right_copyright_text = cs_get_option('copyright_text_right');

$copyright_layout = cs_get_option('copyright_layout');
if ($copyright_layout === 'copyright-1') {
	$layout_class = 'col-md-12 textcenter';
} elseif ($copyright_layout === 'copyright-2') {
	$layout_class = 'col-md-6';
} else {
	$layout_class = 'col-md-4';
}
?>
<!-- Copyright Bar -->
<div class="row align-items-center">
  <div class="<?php echo esc_attr($layout_class); ?>">
  	<?php do_action( 'saaspot_before_copyright_left_action' ); // SaaSpot Action ?>
    <?php if ($left_copyright_text){ echo do_shortcode($left_copyright_text); } ?> <!-- SaaSpotWP -->
  	<?php do_action( 'saaspot_after_copyright_left_action' ); // SaaSpot Action ?>
  </div>
  <?php if ($copyright_layout === 'copyright-3') { ?>
	  <div class="<?php echo esc_attr($layout_class); ?> textcenter">
  		<?php do_action( 'saaspot_before_copyright_center_action' ); // SaaSpot Action ?>
	    <p><?php if ($middle_copyright_text){ echo do_shortcode($middle_copyright_text); } ?></p> <!-- SaaSpotWP -->
  		<?php do_action( 'saaspot_after_copyright_center_action' ); // SaaSpot Action ?>
	  </div>
	<?php } 
  if ($copyright_layout === 'copyright-2' || $copyright_layout === 'copyright-3') { ?>
	  <div class="<?php echo esc_attr($layout_class); ?> textright">
  		<?php do_action( 'saaspot_before_copyright_right_action' ); // SaaSpot Action ?>
	    <?php if ($right_copyright_text){ echo do_shortcode($right_copyright_text); } ?> <!-- SaaSpotWP -->
  		<?php do_action( 'saaspot_after_copyright_right_action' ); // SaaSpot Action ?>
	  </div>
	<?php } ?>
</div>
<!-- Copyright Bar -->
<?php
