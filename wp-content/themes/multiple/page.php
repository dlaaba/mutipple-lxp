<?php
/*
 * The template for displaying all pages.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );

if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else {
	$saaspot_content_padding = '';
}
// Padding - Metabox
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
} else {
	$saaspot_custom_padding = '';
}

// Page Layout Options
if ($saaspot_meta) {
	$saaspot_page_layout = $saaspot_meta['page_layout'];
	if($saaspot_page_layout === 'left-sidebar' || $saaspot_page_layout === 'right-sidebar') {
		$saaspot_column_class = 'saspot-primary';
		$saaspot_layout_class = 'container';
	} elseif($saaspot_page_layout === 'full-width') {
    $saaspot_column_class = 'col-md-12';
    $saaspot_layout_class = 'container-fluid';
  } else {
		$saaspot_column_class = 'col-md-12';
		$saaspot_layout_class = 'container';
	}

	// Page Layout Class
	if ($saaspot_page_layout === 'left-sidebar') {
		$saaspot_sidebar_class = 'left-sidebar';
	} elseif ($saaspot_page_layout === 'right-sidebar') {
		$saaspot_sidebar_class = 'right-sidebar';
	} else {
		$saaspot_sidebar_class = 'full-width';
	}
} else {
	$saaspot_page_layout = '';
	$saaspot_column_class = 'col-md-12';
	$saaspot_layout_class = 'container';
	$saaspot_sidebar_class = 'saspot-full-width';
}

if ($saaspot_meta) {
  $full_page  = $saaspot_meta['full_page'];
  $form_title  = $saaspot_meta['form_title'];
  $form_content  = $saaspot_meta['form_content'];
  $form_code  = $saaspot_meta['form_code'];
  $footer_code  = $saaspot_meta['footer_code'];
  $form_image  = $saaspot_meta['form_image'];
  $form_video  = $saaspot_meta['form_video'];
} else {
  $full_page = '';
  $form_title = '';
  $form_content = '';
  $form_code = '';
  $footer_code = '';
  $form_image = '';
  $form_video = '';
}
// Full Page
get_header();
if ($full_page) {
$form_image = ( $form_image ) ? wp_get_attachment_url($form_image) : SAASPOT_IMAGES . '/full-page.png';
?>
<div class="saspot-full-page">
  <div class="row">
    <div class="col-lg-6">
      <div class="full-page-wrap saspot-overlay">
        <div class="saspot-table-wrap">
          <div class="saspot-table-row">
            <header class="saspot-header">
              <div class="row align-items-center">
                <div class="col-md-5 col-8">
                  <?php  get_template_part( 'layouts/header/logo' ); ?>
                </div>
                <div class="col-md-7 col-4">
                  <div class="textright">
                    <a href="javascript:void(0);" class="saspot-toggle">
                      <span class="toggle-separator"></span>
                    </a>
                  </div>
                </div>
              </div>
            </header>
          </div>
          <div class="saspot-table-row">
            <div class="saspot-table-wrap">
              <div class="saspot-align-wrap">
                <div class="request-form saspot-form">
                  <div class="section-title-wrap">
                    <h2 class="section-title"><?php echo esc_html($form_title); ?></h2>
                    <p><?php echo esc_html($form_content); ?></p>
                  </div>
                  <?php if ($form_code) { echo do_shortcode('[contact-form-7 id="'. $form_code .'"]'); } ?> <!-- SaaSpotWP -->
                </div>
              </div>
            </div>
          </div>
          <div class="saspot-table-row">
            <div class="saspot-table-wrap">
              <div class="saspot-align-wrap">
                <div class="request-bottom-wrap">
                  <?php echo do_shortcode($footer_code); ?> <!-- SaaSpotWP -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="saspot-background" style="background-image: url(<?php echo esc_url($form_image); ?>);">
        <div class="saspot-table-wrap">
          <div class="saspot-align-wrap">
            <a href="#0" id="myUrl" data-toggle="modal" data-src="<?php echo esc_url($form_video); ?>" data-target="#SaaSpotVideoModal" class="saspot-video-btn">
              <span class="video-btn"><i class="fa fa-play" aria-hidden="true"></i></span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } else { ?>
<div class="saspot-mid-wrap <?php echo esc_attr($saaspot_content_padding .' '. $saaspot_sidebar_class); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
	<div class="<?php echo esc_attr($saaspot_layout_class); ?>">
		<div class="row">
			<?php
			// Left Sidebar
			if($saaspot_page_layout === 'left-sidebar') {
	   		get_sidebar();
			}
			?>
			<div class="<?php echo esc_attr($saaspot_column_class); ?>">
      <?php do_action( 'saaspot_before_content_action' ); // SaaSpot Action ?>
				<?php
				while ( have_posts() ) : the_post();
					the_content();
					echo saaspot_wp_link_pages();
					// If comments are open or we have at least one comment, load up the comment template.
					if (comments_open() || get_comments_number()) :
						comments_template();
					endif;
				endwhile;
				?>
      <?php do_action( 'saaspot_after_content_action' ); // SaaSpot Action ?>
			</div><!-- Content Area -->
			<?php
			// Right Sidebar
			if($saaspot_page_layout === 'right-sidebar') {
				get_sidebar();
			}
			?>
		</div>
	</div>
</div>
<?php
} // Full Page End
get_footer();