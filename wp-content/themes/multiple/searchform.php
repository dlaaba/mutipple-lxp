<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" class="searchform saspot-form" >
	<p>
		<input type="text" name="s" id="s" placeholder="<?php esc_attr_e('Search...','saaspot'); ?>" />
		<input type="submit" id="searchsubmit" class="button-primary" value="Search" />
		<input type="hidden" name="post_type" value="">
	</p>
</form>
<?php
