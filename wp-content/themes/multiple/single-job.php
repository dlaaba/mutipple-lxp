<?php
/*
 * The template for displaying all single job.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();

// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else { $saaspot_content_padding = ''; }
// Padding - Theme Options
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
}
?>
<div class="saspot-mid-wrap job-single-page <?php echo esc_attr($saaspot_content_padding); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
	<div class="container ">
		<div class="job-details">
			<?php
				if (have_posts()) : while (have_posts()) : the_post();
					the_content();
				endwhile;
				endif;			
	    	wp_reset_postdata();  // avoid errors further down the page
			?>
		</div>
	</div>
</div>
<?php
get_footer();
