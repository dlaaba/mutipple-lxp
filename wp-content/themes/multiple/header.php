<?php
/*
 * The header for our theme.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
?><!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php
// if the `wp_site_icon` function does not exist (ie we're on < WP 4.3)
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
    <link rel="shortcut icon" href="<?php echo esc_url(SAASPOT_IMAGES); ?>/favicon.ico" />
<?php }
$saaspot_all_element_color  = cs_get_customize_option( 'all_element_colors' );
?>
<meta name="msapplication-TileColor" content="<?php echo esc_attr($saaspot_all_element_color); ?>">
<meta name="theme-color" content="<?php echo esc_attr($saaspot_all_element_color); ?>">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
// Metabox
global $post;
$saaspot_id    = ( isset( $post ) ) ? $post->ID : false;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $saaspot_id : false;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );

// Header Style
$saaspot_sticky_header      = cs_get_option('sticky_header');
$saaspot_sticky_footer      = cs_get_option('sticky_footer');

if ($saaspot_sticky_footer) {
  $footer_class = ' saspot-sticky-footer';
} else {
  $footer_class = '';
}
if ($saaspot_sticky_header) {
  $header_class = ' saspot-sticky';
} else {
  $header_class = '';
}
// fullwidth Topbar
if ($saaspot_meta) {
  $hide_header  = $saaspot_meta['hide_header'];
  $full_page  = $saaspot_meta['full_page'];
} else {
  $hide_header = '';
  $full_page = '';
}
wp_head();
?>
</head>
<body <?php body_class(); ?>>
<!-- Full Page -->
<?php if (!$full_page) { ?>
<!-- Hanor Main Wrap -->
<div class="saspot-main-wrap <?php echo esc_attr($footer_class); ?>">
  <!-- Hanor Main Wrap Inner -->
  <div class="main-wrap-inner">
  <?php if(!$hide_header) { ?>
  <!-- SaaSpot Topbar -->
  <?php get_template_part( 'layouts/header/top', 'bar' ); ?>
  <!-- Header -->
  <?php do_action( 'saaspot_before_header_action' ); // SaaSpot Action ?>
  <header class="saspot-header <?php echo esc_attr($header_class); ?>">
    <div class="row align-items-center">
      <div class="col-xl-5 col-lg-4 col-md-6 col-10">
        <?php  get_template_part( 'layouts/header/logo' ); ?>
      </div>
      <div class="col-xl-7 col-lg-8 col-md-6 col-2">
        <div class="header-right">
          <?php get_template_part( 'layouts/header/menu', 'bar' ); ?>
        </div>
      </div>
    </div> <!-- Container -->
  </header>
  <?php do_action( 'saaspot_after_header_action' ); // SaaSpot Action ?>
  <?php }
  // Title Area
  $saaspot_need_title_bar = cs_get_option('need_title_bar');
  // SaaSpotWP
  if($saaspot_need_title_bar) {
    get_template_part( 'layouts/header/title', 'bar' );
    // echo '<pre>'. json_encode( get_option('_cs_options') ). '<pre>'; // SaaSpotWP - JSON File, json, Json.
  }
} // Full Page End

