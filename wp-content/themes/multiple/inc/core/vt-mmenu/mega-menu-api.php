<?php
/*
 * Mega Menu API
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

require_once( SAASPOT_FRAMEWORK . '/core/vt-mmenu/custom-menu-walker.php' );

class saspot_framework_custom_menu {

  public $saspot_custom_fields = array( 'mega_menu', 'dropdown_style', 'bold_title', 'submenu_style', 'see_all' );
  public $walker = null;

  public function __construct() {
		add_action( 'saspot_custom_menu_fields', array( $this, 'saspot_custom_menu_fields_add_function' ), 10, 2 );
		add_action( 'wp_update_nav_menu_item', array( $this, 'saspot_framework_update_fields'), 10, 3 );

		add_filter( 'wp_setup_nav_menu_item', array( $this, 'saspot_framework_add_fields' ) );
		add_filter( 'wp_edit_nav_menu_walker', array( $this, 'saspot_framework_edit_walker'), 10, 2 );
		add_filter( 'wp_nav_menu_args', array( $this, 'wp_nav_menu_args' ), 99 );
  }

  /**
  * Add custom fields
  */
  public function saspot_custom_menu_fields_add_function( $item_id, $item ) {
?>
		<div class="saspot-menu-mega">
			<span class="saspot-cf-sep"></span>
			<p class="saspot-cf-mega_menu saspot-cf-field description description-thin cs-field-switcher">
        <span class="saspot-cf-title">Enable Mega Menu?</span><br/>
        <label for="edit-menu-item-mega_menu-<?php echo esc_attr($item_id); ?>">
          <input type="checkbox" id="edit-menu-item-mega_menu-<?php echo esc_attr($item_id); ?>"<?php checked( $item->mega_menu, 1 ); ?> name="menu-item-mega_menu[<?php echo esc_attr($item_id); ?>]" value="1" />
          <em data-on="on" data-off="off"></em><span></span>
        </label>
			</p>
      <p class="saspot-cf-dropdown_style saspot-cf-field description description-thin">
        <span class="saspot-cf-title">Dropdown Style</span>
        <label for="edit-menu-item-dropdown_style-<?php echo esc_attr($item_id); ?>">
        <select id="edit-menu-item-dropdown_style-<?php echo esc_attr($item_id); ?>" name="menu-item-dropdown_style[<?php echo esc_attr($item_id); ?>]">
          <option value="">Select Style</option>
          <?php
            $dropdown_style = array(
            'normal-style'    => 'Style One (Normal)',
            'style-two'    => 'Style Two (Mega Menu)',
            );
            foreach ($dropdown_style as $key => $value) {
                echo '<option value="'. esc_attr($key) .'"'. selected($key, $item->dropdown_style) .'>'. esc_attr($value) .'</option>';
            } ?>
        </select>
        </label>
      </p>
			<span class="saspot-cf-sep"></span>
		</div>

	  <!-- Columns -->
	  <div class="saspot-menu-columns">
      <span class="saspot-cf-sep"></span>
      <p class="saspot-cf-submenu_style saspot-cf-field description description-thin">
        <span class="saspot-cf-title">Sub Menu Style</span>
        <label for="edit-menu-item-submenu_style-<?php echo esc_attr($item_id); ?>">
        <select id="edit-menu-item-submenu_style-<?php echo esc_attr($item_id); ?>" name="menu-item-submenu_style[<?php echo esc_attr($item_id); ?>]">
          <option value="">Select Style</option>
          <?php
            $submenu_style = array(
            'one'    => 'Style One (Full Width)',
            'two'    => 'Style Two (Full Width Inline)',
            'three'    => 'Style Three (Half Width)',
            'four'    => 'Style Four (Half Width Border)',
            );
            foreach ($submenu_style as $key => $value) {
                echo '<option value="'. esc_attr($key) .'"'. selected($key, $item->submenu_style) .'>'. esc_html($value) .'</option>';
            } ?>
        </select>
        </label>
      </p>
      <p class="els-cf-bold_title els-cf-field description description-thin cs-field-switcher">
        <span class="els-cf-title">Bold Title?</span><br/>
        <label for="edit-menu-item-bold_title-<?php echo esc_attr($item_id); ?>">
            <input type="checkbox" id="edit-menu-item-bold_title-<?php echo esc_attr($item_id); ?>"<?php checked( $item->bold_title, 1 ); ?> name="menu-item-bold_title[<?php echo esc_attr($item_id); ?>]" value="1" />
            <em data-on="on" data-off="off"></em><span></span>
        </label>
      </p>
      <p class="els-cf-see_all els-cf-field description description-thin cs-field-switcher">
        <span class="els-cf-title">Need Button Style?</span><br/>
        <label for="edit-menu-item-see_all-<?php echo esc_attr($item_id); ?>">
            <input type="checkbox" id="edit-menu-item-see_all-<?php echo esc_attr($item_id); ?>"<?php checked( $item->see_all, 1 ); ?> name="menu-item-see_all[<?php echo esc_attr($item_id); ?>]" value="1" />
            <em data-on="on" data-off="off"></em><span></span>
        </label>
      </p>
      <span class="saspot-cf-sep"></span>
	  </div>
  <?php
  }

	/**
	 * Add custom fields to $menu_item nav object
	*/
	public function saspot_framework_add_fields( $menu_item ) {
    foreach ( $this->saspot_custom_fields as $key ) {
      $menu_item->$key = get_post_meta( $menu_item->ID, '_menu_item_'. $key, true );
    }
    return $menu_item;
	}

	/**
	 * Save menu custom fields
	*/
	public function saspot_framework_update_fields( $menu_id, $menu_item_db_id, $args ) {
    foreach ( $this->saspot_custom_fields as $key ) {
      $value = ( isset( $_REQUEST['menu-item-'.$key][$menu_item_db_id] ) ) ? $_REQUEST['menu-item-'.$key][$menu_item_db_id] : '';
      update_post_meta( $menu_item_db_id, '_menu_item_'. $key, $value );
    }
	}

	/**
	 * Setting these cutomization into core function of WordPress : wp_nav_menu()
	*/
	public function wp_nav_menu_args( $args ) {
    $walker = new Walker_Nav_Menu_Custom();
    $args['container'] = false;
    $theme_megamenu = cs_get_option('theme_megamenu');
    if ((!$theme_megamenu) && ($theme_megamenu === NULL)) {
      $args['walker'] = $walker;
    } else {
      $args['walker'] = '';
    }

    return $args;
	}

	/**
	 * Define new Walker edit
	*/
	public function saspot_framework_edit_walker($walker,$menu_id) {
    return 'Walker_Nav_Menu_Edit_Custom';
	}

}

// instantiate plugin's class
$GLOBALS['saspot_custom_menu'] = new saspot_framework_custom_menu();