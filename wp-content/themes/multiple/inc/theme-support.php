<?php
/*
 * All theme related setups here.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/* Set content width */
if ( ! isset( $content_width ) ) $content_width = 1170;

/* Register menu */
register_nav_menus( array(
	'primary' => esc_html__( 'Main Navigation', 'saaspot' )
) );

/* Thumbnails */
add_theme_support( 'post-thumbnails' );

/* Feeds */
add_theme_support( 'automatic-feed-links' );

/* Add support for Title Tag. */
add_theme_support( 'title-tag' );

/* WooCommerce */
add_theme_support( 'woocommerce' );

/* HTML5 */
add_theme_support( 'html5', array( 'gallery', 'caption' ) );

/* Extend wp_title */
if( ! function_exists( 'saaspot_theme_wp_title' ) ) {
	function saaspot_theme_wp_title( $title, $sep ) {
	 global $paged, $page;

	 if ( is_feed() )
	  return $title;

	 // Add the site name.
	 $site_name = get_bloginfo( 'name' );

	 // Add the site description for the home/front page.
	 $site_description = get_bloginfo( 'description', 'display' );
	 if ( $site_description && ( is_front_page() ) )
	  $title = "$site_name $sep $site_description";

	 // Add a page number if necessary.
	 if ( $paged >= 2 || $page >= 2 )
	  $title = "$site_name $sep" . sprintf( esc_html__( ' Page %s', 'saaspot' ), max( $paged, $page ) );

	 return $title;
	}
	add_filter( 'wp_title', 'saaspot_theme_wp_title', 10, 2 );
}

/* Languages */
if( ! function_exists( 'saaspot_theme_language_setup' ) ) {
	function saaspot_theme_language_setup(){
	  load_theme_textdomain( 'saaspot', get_template_directory() . '/languages' );
	}
	add_action('after_setup_theme', 'saaspot_theme_language_setup');
}

/* Slider Revolution Theme Mode */
if(function_exists( 'set_revslider_as_theme' )){
	add_action( 'init', 'saaspot_theme_revslider' );
	function saaspot_theme_revslider() {
		set_revslider_as_theme();
	}
}
