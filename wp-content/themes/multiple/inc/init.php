<?php
/*
 * All SaaSpot Theme Related Functions Files are Linked here
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/* Theme All Basic Setup */
require_once( SAASPOT_FRAMEWORK . '/theme-support.php' );
require_once( SAASPOT_FRAMEWORK . '/backend-functions.php' );
require_once( SAASPOT_FRAMEWORK . '/frontend-functions.php' );
require_once( SAASPOT_FRAMEWORK . '/enqueue-files.php' );
require_once( SAASPOT_CS_FRAMEWORK . '/config.php' );

/* Custom Menu Walker */
require_once( SAASPOT_FRAMEWORK . '/core/vt-mmenu/mega-menu-api.php' );

/* Bootstrap Menu Walker */
require_once( SAASPOT_FRAMEWORK . '/core/vt-mmenu/wp_bootstrap_navwalker.php' );

/* Install Plugins */
require_once( SAASPOT_FRAMEWORK . '/plugins/notify/activation.php' );

/* Sidebars */
require_once( SAASPOT_FRAMEWORK . '/core/sidebars.php' );
