<?php
/*
 * All customizer related options for SaaSpot theme.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

if( ! function_exists( 'saaspot_vt_customizer' ) ) {
  function saaspot_vt_customizer( $options ) {

	$options        = array(); // remove old options 

	// Primary Color
	$options[]      = array(
	  'name'        => 'elemets_color_section',
	  'title'       => esc_html__('Primary Color', 'saaspot'),
	  'settings'    => array(

	    // Fields Start
			array(
				'name'      => 'all_element_colors',
				'default'   => '#13b5ea',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Elements Color', 'saaspot'),
					'description'    => esc_html__('This is theme primary color, means it\'ll affect all elements that have default color of our theme primary color.', 'saaspot'),
				),
			),
	    // Fields End

	  )
	);
	// Primary Color

	// Secondary Color
	$options[]      = array(
	  'name'        => 'elemets_sec_color_section',
	  'title'       => esc_html__('Secondary Color', 'saaspot'),
	  'settings'    => array(

	    // Fields Start
			array(
				'name'      => 'all_element_secondary_colors',
				'default'   => '#4776e6',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Elements secondary Color', 'saaspot'),
					'description'    => esc_html__('This is theme secondary color, means it\'ll affect all elements that have default color of our theme secondary color.', 'saaspot'),
				),
			),
	    // Fields End
	  )
	);
	// Secondary Color

	// Topbar Color
	$options[]      = array(
	  'name'        => 'topbar_color_section',
	  'title'       => esc_html__('01. Topbar Colors', 'saaspot'),
	  'settings'    => array(

	    // Fields Start
	    array(
				'name'          => 'topbar_bg_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Bar Color', 'saaspot'),
					),
				),
			),
			array(
				'name'      => 'topbar_bg_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Background Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'topbar_border_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Border Color', 'saaspot'),
				),
			),
			array(
				'name'          => 'topbar_text_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Common Color', 'saaspot'),
					),
				),
			),
			array(
				'name'      => 'topbar_text_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Text Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'topbar_link_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'topbar_link_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Hover Color', 'saaspot'),
				),
			),
			array(
				'name'          => 'topbar_label_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Label Color', 'saaspot'),
					),
				),
			),
			array(
				'name'      => 'topbar_label_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Label Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'topbar_label_bg_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Label Background Color', 'saaspot'),
				),
			),
	    // Fields End

	  )
	);
	// Topbar Color

	// Header Color
	$options[]      = array(
	  'name'        => 'header_color_section',
	  'title'       => esc_html__('02. Header Colors', 'saaspot'),
	  'sections'    => array(

	  	array(
	      'name'          => 'normal_header_section',
	      'title'         => esc_html__('Normal Header', 'saaspot'),
	      'settings'      => array(

			    // Fields Start
					array(
						'name'          => 'header_main_menu_heading',
						'control'       => array(
							'type'        => 'cs_field',
							'options'     => array(
								'type'      => 'notice',
								'class'     => 'info',
								'content'   => esc_html__('Main Menu Colors', 'saaspot'),
							),
						),
					),
					array(
						'name'      => 'header_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'header_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'header_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Hover Color', 'saaspot'),
						),
					),

					// Sub Menu Color
					array(
						'name'          => 'header_submenu_heading',
						'control'       => array(
							'type'        => 'cs_field',
							'options'     => array(
								'type'      => 'notice',
								'class'     => 'info',
								'content'   => esc_html__('Sub-Menu Colors', 'saaspot'),
							),
						),
					),
					array(
						'name'      => 'submenu_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'submenu_bg_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'submenu_border_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Border Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'submenu_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'submenu_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Hover Color', 'saaspot'),
						),
					),

					// Header Button
					array(
						'name'          => 'header_button_heading',
						'control'       => array(
							'type'        => 'cs_field',
							'options'     => array(
								'type'      => 'notice',
								'class'     => 'info',
								'content'   => esc_html__('Header Button Colors', 'saaspot'),
							),
						),
					),
					array(
						'name'      => 'button_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'button_text_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Text Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'button_border_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Border Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'button_bg_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background HoverColor', 'saaspot'),
						),
					),
					array(
						'name'      => 'button_text_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Text Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'button_border_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Border Hover Color', 'saaspot'),
						),
					),
				)
    	),
			array(
	      'name'          => 'sticky_header_section',
	      'title'         => esc_html__('Sticky Header', 'saaspot'),
	      'settings'      => array(
	      	array(
						'name'          => 'header_sticky_heading',
						'control'       => array(
							'type'        => 'cs_field',
							'options'     => array(
								'type'      => 'notice',
								'class'     => 'info',
								'content'   => esc_html__('Sticky Header Colors', 'saaspot'),
							),
						),
					),
					array(
						'name'      => 'sticky_header_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'sticky_header_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'sticky_header_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Hover Color', 'saaspot'),
						),
					),
				)
      ),
      array(
	      'name'          => 'mobile_menu_section',
	      'title'         => esc_html__('Mobile Menu', 'saaspot'),
	      'settings'      => array(
	      	array(
						'name'          => 'mobile_menu_heading',
						'control'       => array(
							'type'        => 'cs_field',
							'options'     => array(
								'type'      => 'notice',
								'class'     => 'info',
								'content'   => esc_html__('Mobile Menu Colors', 'saaspot'),
							),
						),
					),
					array(
						'name'      => 'mobile_menu_toggle_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Menu Toggle Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_bg_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_border_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Border Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_expand_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Menu Expand Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_expand_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Menu Expand Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_expand_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Menu Expand Background Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'mobile_menu_expand_bg_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Menu Expand Background Hover Color', 'saaspot'),
						),
					),
				)
      ),
	    // Fields End

	  )
	);
	// Header Color

	// Sidebar Menu Color
	$options[]      = array(
	  'name'        => 'sidemenu_section',
	  'title'       => esc_html__('03. Sidebar Menu Colors', 'saaspot'),
    'settings'      => array(

    	// Fields Start
    	array(
				'name'          => 'sidemenu_colors_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Sidebar Menu Colors', 'saaspot'),
					),
				),
			),
    	array(
				'name'      => 'sidebar_bg_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Background Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'sidebar_link_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'sidebar_link_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Hover Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'sidebar_menu_border_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Menu Border Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'sidebar_submenu_bg_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Sub-Menu Background Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'sidebar_submenu_link_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Sub-Menu Link Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'sidebar_submenu_link_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Sub-Menu Link Hover Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'sidebar_submenu_border_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Sub-Menu Border Color', 'saaspot'),
				),
			),
	    // Fields End

	  )
	);
	// Sidebar Menu Color

	// Title Bar Color
	$options[]      = array(
	  'name'        => 'titlebar_section',
	  'title'       => esc_html__('03. Title Bar Colors', 'saaspot'),
    'settings'      => array(

    	// Fields Start
    	array(
				'name'          => 'titlebar_colors_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('<h2 style="margin: 0;text-align: center;">Title Colors</h2> <br /> This is common settings, if this settings not affect in your page. Please check your page metabox. You may set default settings there.', 'saaspot'),
					),
				),
			),
    	array(
				'name'      => 'titlebar_title_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Title Color', 'saaspot'),
				),
			),
			array(
				'name'      => 'titlebar_sub_title_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Sub Title Color', 'saaspot'),
				),
			),
	    // Fields End

	  )
	);
	// Title Bar Color

	// Content Color
	$options[]      = array(
	  'name'        => 'content_section',
	  'title'       => esc_html__('04. Content Colors', 'saaspot'),
	  'description' => esc_html__('This is all about content area text and heading colors.', 'saaspot'),
	  'sections'    => array(

	  	array(
	      'name'          => 'content_text_section',
	      'title'         => esc_html__('Content Text', 'saaspot'),
	      'settings'      => array(

			    // Fields Start
			    array(
						'name'      => 'body_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Body & Content Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'body_links_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Body Links Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'body_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Body Links Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'sidebar_content_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Sidebar Content Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'sidebar_links_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Sidebar Links Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'sidebar_links_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Sidebar Links Hover Color', 'saaspot'),
						),
					),
			    // Fields End
			  )
			),

			// Text Colors Section
			array(
	      'name'          => 'content_heading_section',
	      'title'         => esc_html__('Headings', 'saaspot'),
	      'settings'      => array(

	      	// Fields Start
					array(
						'name'      => 'content_heading_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Content Heading Color', 'saaspot'),
						),
					),
	      	array(
						'name'      => 'sidebar_heading_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Sidebar Heading Color', 'saaspot'),
						),
					),
			    // Fields End
      	)
      ),
	  )
	);
	// Content Color

	// Footer Color
	$options[]      = array(
	  'name'        => 'footer_section',
	  'title'       => esc_html__('05. Footer Colors', 'saaspot'),
	  'description' => esc_html__('This is all about footer settings. Make sure you\'ve enabled your needed section at : SaaSpot > Theme Options > Footer ', 'saaspot'),
	  'sections'    => array(

			// Footer Widgets Block
	  	array(
	      'name'          => 'footer_widget_section',
	      'title'         => esc_html__('Widget Block', 'saaspot'),
	      'settings'      => array(

			    // Fields Start
					array(
			      'name'          => 'footer_widget_color_notice',
			      'control'       => array(
			        'type'        => 'cs_field',
			        'options'     => array(
			          'type'      => 'notice',
			          'class'     => 'info',
			          'content'   => esc_html__('Content Colors', 'saaspot'),
			        ),
			      ),
			    ),
					array(
						'name'      => 'footer_heading_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Heading Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'footer_text_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Text Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'footer_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Link Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'footer_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Link Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'footer_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'saaspot'),
						),
					),
					
			    // Fields End
			  )
			),
			// Footer Widgets Block
	),
);
	// Copyright Color
	$options[]      = array(
	  'name'        => 'copyright_section',
	  'title'       => esc_html__('06. Copyright Colors', 'saaspot'),
	  'description' => esc_html__('This is all about footer settings. Make sure you\'ve enabled your needed section at : SaaSpot > Theme Options > Footer > Copyright Layout ', 'saaspot'),
	  'sections'    => array(

			// Copyright Block
	  	array(
	      'name'          => 'footer_copyright_section',
	      'title'         => esc_html__('Copyright Block', 'saaspot'),
	      'settings'      => array(

			    // Fields Start
					array(
			      'name'          => 'copyright_color_notice',
			      'control'       => array(
			        'type'        => 'cs_field',
			        'options'     => array(
			          'type'      => 'notice',
			          'class'     => 'info',
			          'content'   => esc_html__('Copyright Colors', 'saaspot'),
			        ),
			      ),
			    ),
					array(
						'name'      => 'copyright_text_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Copyright Text Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'copyright_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Copyright Link Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'copyright_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Copyright Link Hover Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'copyright_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'saaspot'),
						),
					),
					array(
						'name'      => 'copyright_border_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Border Color', 'saaspot'),
						),
					),
			    // Fields End
			  )
			),
			// Footer Widgets Block
	  )
	);
	// Footer Color

	return $options;

  }
  add_filter( 'cs_customize_options', 'saaspot_vt_customizer' );
}
