<?php
/*
 * All Theme Options for SaaSpot theme.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

function saaspot_vt_settings( $settings ) {
  $settings           = array(
    'menu_title'      => SAASPOT_NAME . esc_html__(' Options', 'saaspot'),
    'menu_slug'       => sanitize_title(SAASPOT_NAME) . '_options',
    'menu_type'       => 'menu',
    'menu_icon'       => 'dashicons-awards',
    'menu_position'   => '4',
    'ajax_save'       => false,
    'show_reset_all'  => true,
    'framework_title' => SAASPOT_NAME .' <small>V-'. SAASPOT_VERSION .' by <a href="'. SAASPOT_BRAND_URL .'" target="_blank">'. SAASPOT_BRAND_NAME .'</a></small>',
  );
  return $settings;
}
add_filter( 'cs_framework_settings', 'saaspot_vt_settings' );

// Theme Framework Options
function saaspot_vt_options( $options ) {

  $options      = array(); // remove old options
  // ------------------------------
  // Theme Brand
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_brand',
    'title'    => esc_html__('Logo', 'saaspot'),
    'icon'     => 'fa fa-bookmark',
    'fields' => array(

      // Site Logo
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Site Logo', 'saaspot')
      ),
      array(
        'id'    => 'brand_logo_default',
        'type'  => 'image',
        'title' => esc_html__('Logo', 'saaspot'),
        'info'  => esc_html__('Upload your 2x size of logo here. It\'ll comfortable for retina screens.', 'saaspot'),
        'add_title' => esc_html__('Add Logo', 'saaspot'),
      ),
      array(
        'id'          => 'retina_width',
        'type'        => 'text',
        'title'       => esc_html__('Logo Width', 'saaspot'),
        'unit'        => 'px',
      ),
      array(
        'id'          => 'retina_height',
        'type'        => 'text',
        'title'       => esc_html__('Logo Height', 'saaspot'),
        'unit'        => 'px',
      ),
      array(
        'id'          => 'brand_logo_top',
        'type'        => 'number',
        'title'       => esc_html__('Logo Top Space', 'saaspot'),
        'attributes'  => array( 'placeholder' => 5 ),
        'unit'        => 'px',
      ),
      array(
        'id'          => 'brand_logo_bottom',
        'type'        => 'number',
        'title'       => esc_html__('Logo Bottom Space', 'saaspot'),
        'attributes'  => array( 'placeholder' => 5 ),
        'unit'        => 'px',
      ),

      // WordPress Admin Logo
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('WordPress Admin Logo', 'saaspot')
      ),
      array(
        'id'    => 'brand_logo_wp',
        'type'  => 'image',
        'title' => esc_html__('Login logo', 'saaspot'),
        'info'  => esc_html__('Upload your WordPress login page logo here.', 'saaspot'),
        'add_title' => esc_html__('Add Login Logo', 'saaspot'),
      ),

    ),
  );

  // ------------------------------
  // Layout
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_layout',
    'title'  => esc_html__('Layout', 'saaspot'),
    'icon'   => 'fa fa-file-text'
  );
  $options[]      = array(
    'name'        => 'theme_general',
    'title'       => esc_html__('General', 'saaspot'),
    'icon'        => 'fa fa-wrench',

    // begin: fields
    'fields'      => array(

      // -----------------------------
      // Begin: Responsive
      // -----------------------------
      array(
        'id'    => 'theme_preloader',
        'type'  => 'switcher',
        'title' => esc_html__('Preloader', 'saaspot'),
        'info' => esc_html__('Turn off if you don\'t want preloader.', 'saaspot'),
      ),
      array(
        'id'    => 'theme_btotop',
        'type'  => 'switcher',
        'title' => esc_html__('Back To Top', 'saaspot'),
        'info' => esc_html__('Turn off if you don\'t want back to top button.', 'saaspot'),
        'default' => true,
      ),
      array(
        'id'    => 'theme_megamenu',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Mega Menu', 'saaspot'),
        'info' => esc_html__('Turn on if you don\'t want mega menu (Strictly not recomended for Elementor PRO version header menu).', 'saaspot'),
        'default' => false,
      ),
      array(
        'id'          => 'redirection_link',
        'type'        => 'text',
        'title'       => esc_html__('Redirection Link', 'saaspot'),
        'info' => esc_html__('This link will be redirected after successfull registration.', 'saaspot'),
      ),

    ), // end: fields

  );

  // ------------------------------
  // Header Sections
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_header_tab',
    'title'    => esc_html__('Header', 'saaspot'),
    'icon'     => 'fa fa-bars',
    'sections' => array(

      // header design tab
      array(
        'name'     => 'header_design_tab',
        'title'    => esc_html__('Design', 'saaspot'),
        'icon'     => 'fa fa-magic',
        'fields'   => array(
          // Header Select
          array(
            'id'    => 'need_content',
            'type'  => 'switcher',
            'title' => esc_html__('Need Header Content', 'saaspot'),
            'default' => true,
          ),
          array(
            'id'              => 'header_wpml_info',
            'title'           => esc_html__('Header Wpml', 'saaspot'),
            'desc'            => esc_html__('Add your header WPML here. Example : Wpml', 'saaspot'),
            'type'            => 'textarea',
            'shortcode'       => true,
            'dependency'   => array('need_content', '==', 'true'),
          ),
          array(
            'id'              => 'header_contact',
            'title'           => esc_html__('Header Contact', 'saaspot'),
            'desc'            => esc_html__('Add your header contact here. Example : Contact', 'saaspot'),
            'type'            => 'textarea',
            'shortcode'       => true,
            'dependency'   => array('need_content', '==', 'true'),
          ),
          array(
            'id'              => 'header_btns',
            'title'           => esc_html__('Header Buttons', 'saaspot'),
            'desc'            => esc_html__('Add your header buttons here. Example : Buttons', 'saaspot'),
            'type'            => 'textarea',
            'shortcode'       => true,
            'dependency'   => array('need_content', '==', 'true'),
          ),
          // Header Select
          // Extra's
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Extra\'s', 'saaspot'),
          ),
          array(
            'id'    => 'sticky_header',
            'type'  => 'switcher',
            'title' => esc_html__('Sticky Header', 'saaspot'),
            'info' => esc_html__('Turn On if you want your naviagtion bar on sticky.', 'saaspot'),
            'default' => true,
          ),
          array(
            'id'    => 'sticky_footer',
            'type'  => 'switcher',
            'title' => esc_html__('Enable Sticky Footer', 'saaspot'),
            'info' => esc_html__('If you turn this ON the footer will be sticky.', 'saaspot'),
            'default' => false,
          ),
          array(
            'id'          => 'mobile_breakpoint',
            'type'        => 'text',
            'title'       => esc_html__('Mobile Menu Starts from?', 'saaspot'),
            'attributes'  => array( 'placeholder' => '1199' ),
            'info' => esc_html__('Just put numeric value only. Like : 1199. Don\'t use px or any other units.', 'saaspot'),
          ),

        )
      ),

      // header top bar
      array(
        'name'     => 'header_top_bar_tab',
        'title'    => esc_html__('Top Bar', 'saaspot'),
        'icon'     => 'fa fa-minus',
        'fields'   => array(

          array(
            'id'          => 'top_bar',
            'type'        => 'switcher',
            'title'       => esc_html__('Hide Top Bar', 'saaspot'),
            'on_text'     => esc_html__('Yes', 'saaspot'),
            'off_text'    => esc_html__('No', 'saaspot'),
            'default'     => false,
          ),
          array(
            'id'          => 'top_left',
            'title'       => esc_html__('Top Left Block', 'saaspot'),
            'desc'        => esc_html__('Top bar left block.', 'saaspot'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),
          array(
            'id'          => 'top_right',
            'title'       => esc_html__('Top Right Block', 'saaspot'),
            'desc'        => esc_html__('Top bar right block.', 'saaspot'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),

        )
      ),

      // header banner
      array(
        'name'     => 'header_banner_tab',
        'title'    => esc_html__('Title Bar (or) Banner', 'saaspot'),
        'icon'     => 'fa fa-bullhorn',
        'fields'   => array(

          // Title Area
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Title Area', 'saaspot')
          ),
          array(
            'id'      => 'need_title_bar',
            'type'    => 'switcher',
            'title'   => esc_html__('Title Bar', 'saaspot'),
            'label'   => esc_html__('If you want title bar in your sub-pages, please turn this ON.', 'saaspot'),
            'default'    => true,
          ),
          array(
            'id'             => 'title_bar_padding',
            'type'           => 'select',
            'title'          => esc_html__('Padding Spaces Top & Bottom', 'saaspot'),
            'options'        => array(
              'padding-default' => esc_html__('Default Spacing', 'saaspot'),
              'padding-xs' => esc_html__('Extra Small Padding', 'saaspot'),
              'padding-sm' => esc_html__('Small Padding', 'saaspot'),
              'padding-md' => esc_html__('Medium Padding', 'saaspot'),
              'padding-lg' => esc_html__('Large Padding', 'saaspot'),
              'padding-xl' => esc_html__('Extra Large Padding', 'saaspot'),
              'padding-no' => esc_html__('No Padding', 'saaspot'),
              'padding-custom' => esc_html__('Custom Padding', 'saaspot'),
            ),
            'dependency'   => array( 'need_title_bar', '==', 'true' ),
          ),
          array(
            'id'             => 'titlebar_top_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Top', 'saaspot'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding|need_title_bar', '==|==', 'padding-custom|true' ),
          ),
          array(
            'id'             => 'titlebar_bottom_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Bottom', 'saaspot'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding|need_title_bar', '==|==', 'padding-custom|true' ),
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Background Options', 'saaspot'),
            'dependency' => array( 'need_title_bar', '==', 'true' ),
          ),
          array(
            'id'      => 'titlebar_bg',
            'type'    => 'background',
            'title'   => esc_html__('Background', 'saaspot'),
            'dependency' => array( 'need_title_bar', '==', 'true' ),
          ),
          array(
            'id'      => 'titlebar_bg_overlay_color',
            'type'    => 'color_picker',
            'title'   => esc_html__('Overlay Color', 'saaspot'),
            'dependency' => array( 'need_title_bar', '==', 'true' ),
          ),

        )
      ),

    ),
  );

  // ------------------------------
  // Footer Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'footer_section',
    'title'    => esc_html__('Footer', 'saaspot'),
    'icon'     => 'fa fa-ellipsis-h',
    'fields'   => array(

      // Footer Widget Block
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Footer Widget Block', 'saaspot')
      ),
      array(
        'id'    => 'footer_widget_block',
        'type'  => 'switcher',
        'title' => esc_html__('Enable Widget Block', 'saaspot'),
        'info' => esc_html__('If you turn this ON, then Goto : Appearance > Widgets. There you can see Footer Widget 1,2,3 or 4 Widget Area, add your widgets there.', 'saaspot'),
        'default' => true,
      ),
      array(
        'id'    => 'footer_widget_layout',
        'type'  => 'image_select',
        'title' => esc_html__('Widget Layouts', 'saaspot'),
        'info' => esc_html__('Choose your footer widget layouts.', 'saaspot'),
        'default' => 10,
        'options' => array(
          1   => SAASPOT_CS_IMAGES . '/footer/footer-1.png',
          2   => SAASPOT_CS_IMAGES . '/footer/footer-2.png',
          3   => SAASPOT_CS_IMAGES . '/footer/footer-3.png',
          4   => SAASPOT_CS_IMAGES . '/footer/footer-4.png',
          5   => SAASPOT_CS_IMAGES . '/footer/footer-5.png',
          6   => SAASPOT_CS_IMAGES . '/footer/footer-6.png',
          7   => SAASPOT_CS_IMAGES . '/footer/footer-7.png',
          8   => SAASPOT_CS_IMAGES . '/footer/footer-8.png',
          9   => SAASPOT_CS_IMAGES . '/footer/footer-9.png',
          10   => SAASPOT_CS_IMAGES . '/footer/footer-10.png',
        ),
        'radio'       => true,
        'dependency'  => array('footer_widget_block', '==', true),
      ),

      // Copyright
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Copyright Layout', 'saaspot'),
      ),
      array(
        'id'    => 'need_copyright',
        'type'  => 'switcher',
        'title' => esc_html__('Enable Copyright Section', 'saaspot'),
        'default' => true,
      ),
      array(
        'id'    => 'copyright_layout',
        'type'  => 'image_select',
        'title' => esc_html__('Select Copyright Layout', 'saaspot'),
        'default'      => 'copyright-3',
        'options'      => array(
          'copyright-1'    => SAASPOT_CS_IMAGES .'/footer/copyright-3.png',
          'copyright-2'    => SAASPOT_CS_IMAGES .'/footer/copyright-1.png',
          'copyright-3'    => SAASPOT_CS_IMAGES .'/footer/copyright-4.png',
          ),
        'radio'        => true,
        'dependency'     => array('need_copyright', '==', true),
      ),
     array(
        'id'    => 'copyright_text',
        'type'  => 'textarea',
        'title' => esc_html__('Copyright Left Text', 'saaspot'),
        'shortcode' => true,
        'dependency' => array('need_copyright', '==', 'true'),
        'after'       => 'Helpful shortcodes: [saaspot_current_year] [saaspot_home_url] or any shortcode.',
      ),
      array(
        'id'    => 'secondary_text',
        'type'  => 'textarea',
        'title' => esc_html__('Copyright Middle Text', 'saaspot'),
        'shortcode' => true,
        'dependency' => array('need_copyright|copyright_layout_copyright-3', '==|==', 'true|true'),
        'after'       => 'Helpful shortcodes: Enter any shortcode.',
      ),
      array(
        'id'    => 'copyright_text_right',
        'type'  => 'textarea',
        'title' => esc_html__('Copyright Right Text', 'saaspot'),
        'shortcode' => true,
        'dependency' => array('need_copyright|copyright_layout_copyright-1', '==|==', 'true|false'),
        'after'       => 'Helpful shortcodes: Enter any shortcode.',
      ),

    ),
  );

  // ------------------------------
  // Design
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_design',
    'title'  => esc_html__('Design', 'saaspot'),
    'icon'   => 'fa fa-magic'
  );

  // ------------------------------
  // color section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_color_section',
    'title'    => esc_html__('Colors', 'saaspot'),
    'icon'     => 'fa fa-eyedropper',
    'fields' => array(

      array(
        'type'    => 'heading',
        'content' => esc_html__('Color Options', 'saaspot'),
      ),
      array(
        'type'    => 'subheading',
        'wrap_class' => 'color-tab-content',
        'content' => esc_html__('All color options are available in our theme customizer. The reason of we used customizer options for color section is because, you can choose each part of color from there and see the changes instantly using customizer. Highly customizable colors are in Appearance > Customize', 'saaspot'),
      ),

    ),
  );

  // ------------------------------
  // Typography section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_typo_section',
    'title'    => esc_html__('Typography', 'saaspot'),
    'icon'     => 'fa fa-header',
    'fields' => array(

      // Start fields
      array(
        'id'                  => 'typography',
        'type'                => 'group',
        'fields'              => array(
          array(
            'id'              => 'title',
            'type'            => 'text',
            'title'           => esc_html__('Title', 'saaspot'),
          ),
          array(
            'id'              => 'selector',
            'type'            => 'textarea',
            'title'           => esc_html__('Selector', 'saaspot'),
            'info'           => esc_html__('Enter css selectors like : body, .custom-class', 'saaspot'),
          ),
          array(
            'id'              => 'font',
            'type'            => 'typography',
            'title'           => esc_html__('Font Family', 'saaspot'),
          ),
          array(
            'id'              => 'css',
            'type'            => 'textarea',
            'title'           => esc_html__('Custom CSS', 'saaspot'),
          ),
        ),
        'button_title'        => esc_html__('Add New Typography', 'saaspot'),
        'accordion_title'     => esc_html__('New Typography', 'saaspot'),
        'default'             => array(
          array(
            'title'           => esc_html__('Body Typography', 'saaspot'),
            'selector'        => 'body',
            'font'            => array(
              'family'        => 'Muli',
              'variant'       => 'regular',
            ),
          ),
          array(
            'title'           => esc_html__('Menu Typography', 'saaspot'),
            'selector'        => '.saspot-navigation, .saspot-navigation > ul > li, .mean-container .mean-nav ul li a',
            'font'            => array(
              'family'        => 'Muli',
              'variant'       => 'regular',
            ),
          ),
          array(
            'title'           => esc_html__('Sub Menu Typography', 'saaspot'),
            'selector'        => '.dropdown-nav > li, .dropdown-nav, .mean-container .mean-nav ul.sub-menu li a',
            'font'            => array(
              'family'        => 'Muli',
              'variant'       => 'regular',
            ),
          ),
          array(
            'title'           => esc_html__('Headings Typography', 'saaspot'),
            'selector'        => 'h1, h2, h3, h4, h5, h6, .saspot-location-name, .text-logo, .saspot-team-intro .member-details h3, h2.quote-title p',
            'font'            => array(
              'family'        => 'Muli',
              'variant'       => 'regular',
            ),
          ),
          array(
            'title'           => esc_html__('Shortcode Elements Primary Font', 'saaspot'),
            'selector'        => 'body, input[type="submit"], .nice-select-two .nice-select, .footer-widget .saspot-label, .testimonial-info p, .saspot-file-selector .saspot-icon, .testimonials-style-two .testimonials-wrap p, .guide-details ul li p, .table td p, .testimonials-style-three .testimonials-wrap p, ul.check-list.style-two, .market-wrap .bullet-list, .tooltip',
            'font'            => array(
              'family'        => 'Muli',
              'variant'       => 'regular',
            ),
          ),
          array(
            'title'           => esc_html__('Shortcode Elements Secondary Font', 'saaspot'),
            'selector'        => '.section-title-wrap p, .looking-item p, .tool-item p, .enterprises-info p, .step-info p, .feature-item p, .market-wrap p, .card-body p, .agency-item p, .testimonial-info p, .resource-item p, .analytics-inner p, .marketing-item p, .promote-item p, .market-example-info p, .extension-wrap p, .app-info p, .process-info p, .class-item p, .webinar-info p, .mate-info p, .table td p, .blog-info p, .saspot-sidebar p, .place-info p, .address-info p, .office-item p, .chat-support-info p, .corporate-info p, .identify-info p, .single-post p, .single-apps p, .single-webinars p, .single-job p, .single-testimonial p, .single-team p, .free-trial .saspot-label, .nice-select, .menu-subtitle, .banner-subtitle, .features-link, .create-account, .footer-widget ul, .copyright-language.nice-select-two .nice-select, .check-list, .bullet-list, .commission-info ul, form label, .page-subtitle, .file-types, .features-link .saspot-link, .guide-details ul, .guide-inner-title, .pricing-style-two ul, ul.check-list.style-two li span, .request-bottom-wrap',
            'font'            => array(
              'family'        => 'Noto Sans',
              'variant'       => 'regular',
            ),
          ),
          array(
            'title'           => esc_html__('Example Usage', 'saaspot'),
            'selector'        => '.your-custom-class',
            'font'            => array(
              'family'        => 'Noto Sans',
              'variant'       => 'regular',
            ),
          ),
        ),
      ),

      // Subset
      array(
        'id'                  => 'subsets',
        'type'                => 'select',
        'title'               => esc_html__('Subsets', 'saaspot'),
        'class'               => 'chosen',
        'options'             => array(
          'latin'             => 'latin',
          'latin-ext'         => 'latin-ext',
          'cyrillic'          => 'cyrillic',
          'cyrillic-ext'      => 'cyrillic-ext',
          'greek'             => 'greek',
          'greek-ext'         => 'greek-ext',
          'vietnamese'        => 'vietnamese',
          'devanagari'        => 'devanagari',
          'khmer'             => 'khmer',
        ),
        'attributes'         => array(
          'data-placeholder' => 'Subsets',
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( 'latin' ),
      ),

      array(
        'id'                  => 'font_weight',
        'type'                => 'select',
        'title'               => esc_html__('Font Weights', 'saaspot'),
        'class'               => 'chosen',
        'options'             => array(
          '300'   => 'Light 300',
          '400'   => 'Regular 400',
          '500'   => 'Medium 500',
          '600'   => 'Semi Bold 600',
          '700'   => 'Bold 700',
          '800'   => 'Extra Bold 800',
        ),
        'attributes'         => array(
          'data-placeholder' => 'Font Weight',
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( '300', '400', '500', '600', '700', '800', '900' ),
      ),

      // Custom Fonts Upload
      array(
        'id'                 => 'font_family',
        'type'               => 'group',
        'title'              => 'Upload Custom Fonts',
        'button_title'       => 'Add New Custom Font',
        'accordion_title'    => 'Adding New Font',
        'accordion'          => true,
        'desc'               => 'It is simple. Only add your custom fonts and click to save. After you can check "Font Family" selector. Do not forget to Save!',
        'fields'             => array(

          array(
            'id'             => 'name',
            'type'           => 'text',
            'title'          => 'Font-Family Name',
            'attributes'     => array(
              'placeholder'  => 'for eg. Arial'
            ),
          ),

          array(
            'id'             => 'ttf',
            'type'           => 'upload',
            'title'          => 'Upload .ttf <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.ttf</i>',
            ),
          ),

          array(
            'id'             => 'eot',
            'type'           => 'upload',
            'title'          => 'Upload .eot <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.eot</i>',
            ),
          ),

          array(
            'id'             => 'otf',
            'type'           => 'upload',
            'title'          => 'Upload .otf <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.otf</i>',
            ),
          ),

          array(
            'id'             => 'woff',
            'type'           => 'upload',
            'title'          => 'Upload .woff <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.woff</i>',
            ),
          ),

          array(
            'id'             => 'css',
            'type'           => 'textarea',
            'title'          => 'Extra CSS Style <small><i>(optional)</i></small>',
            'attributes'     => array(
              'placeholder'  => 'for eg. font-weight: normal;'
            ),
          ),

        ),
      ),
      // End All field

    ),
  );

  // ------------------------------
  // Pages
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_pages',
    'title'  => esc_html__('Pages', 'saaspot'),
    'icon'   => 'fa fa-files-o'
  );

  // ------------------------------
  // Apps Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'apps_section',
    'title'    => esc_html__('Apps', 'saaspot'),
    'icon'     => 'fa fa-briefcase',
    'fields' => array(

      // apps name change
      array(
        'id'    => 'noneed_apps_post',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Apps Post?', 'saaspot'),
        'info' => esc_html__('If need to disable this post type, please turn this ON.', 'saaspot'),
        'default' => false,
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Name Change', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_apps_name',
        'type'    => 'text',
        'title'   => esc_html__('Apps Name', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'Apps'
        ),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_apps_slug',
        'type'    => 'text',
        'title'   => esc_html__('Apps Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'apps-item'
        ),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_apps_cat_slug',
        'type'    => 'text',
        'title'   => esc_html__('Apps Category Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'apps-category'
        ),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'type'    => 'notice',
        'class'   => 'danger',
        'content' => esc_html__('Important: Please do not set apps slug and page slug as same. It\'ll not work.', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      // Apps Name

      // apps listing
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Apps Style', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'          => 'categories_text',
        'type'        => 'text',
        'title'       => esc_html__('Categories Title', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'apps_limit',
        'type'    => 'text',
        'title'   => esc_html__('Apps Limit', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => '10'
        ),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'apps_show_category',
        'type'    => 'text',
        'title'   => esc_html__('Apps Categories', 'saaspot'),
        'info'   => esc_html__('Enter category SLUGS (comma separated) you want to display.', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'             => 'apps_orderby',
        'type'           => 'select',
        'title'          => esc_html__('Order By', 'saaspot'),
        'options'        => array(
          'none' => esc_html__('None', 'saaspot'),
          'ID' => esc_html__('ID', 'saaspot'),
          'author' => esc_html__('Author', 'saaspot'),
          'title' => esc_html__('Title', 'saaspot'),
          'date' => esc_html__('Date', 'saaspot'),
          'name' => esc_html__('Name', 'saaspot'),
          'modified' => esc_html__('Modified', 'saaspot'),
          'rand' => esc_html__('Random', 'saaspot'),
          'menu_order' => esc_html__('Menu Order', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Apps Order By', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'             => 'apps_order',
        'type'           => 'select',
        'title'          => esc_html__('Order', 'saaspot'),
        'options'        => array(
          'ASC' => esc_html__('Asending', 'saaspot'),
          'DESC' => esc_html__('Desending', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Apps Order', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),

      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Enable/Disable Options', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'    => 'apps_aqr',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Image Resize?', 'saaspot'),
        'info' => esc_html__('If need to disable image resize, please turn this ON.', 'saaspot'),
        'default' => false,
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'apps_filter',
        'type'    => 'switcher',
        'title'   => esc_html__('Filter', 'saaspot'),
        'label'   => esc_html__('If you need filter in apps pages, please turn this ON.', 'saaspot'),
        'default'   => true,
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'apps_pagination',
        'type'    => 'switcher',
        'title'   => esc_html__('Pagination', 'saaspot'),
        'label'   => esc_html__('If you need pagination in apps pages, please turn this ON.', 'saaspot'),
        'default'   => true,
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),

      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Related Links', 'saaspot'),
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'apps_need_related',
        'type'    => 'switcher',
        'title'   => esc_html__('Need Related Links', 'saaspot'),
        'label'   => esc_html__('If you need related projects in apps single pages, please turn this ON.', 'saaspot'),
        'default'   => true,
        'dependency'   => array('noneed_apps_post', '==', 'false'),
      ),
      array(
        'id'      => 'apps_related_title',
        'type'    => 'text',
        'title'   => esc_html__('Apps Related Links Title', 'saaspot'),
        'info'   => esc_html__('Enter related projects title.', 'saaspot'),
        'dependency'     => array('apps_need_related|noneed_apps_post', '==|==', 'true|false'),
      ),
      // Apps Listing

    ),
  );

  // ------------------------------
  // Webinars Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'webinars_section',
    'title'    => esc_html__('Webinars', 'saaspot'),
    'icon'     => 'fa fa-play-circle',
    'fields' => array(

      // webinars name change
      array(
        'id'    => 'noneed_webinars_post',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Webinars Post?', 'saaspot'),
        'info' => esc_html__('If need to disable this post type, please turn this ON.', 'saaspot'),
        'default' => false,
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Name Change', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_webinars_name',
        'type'    => 'text',
        'title'   => esc_html__('Webinars Name', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'Webinars'
        ),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_webinars_slug',
        'type'    => 'text',
        'title'   => esc_html__('Webinars Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'webinars-item'
        ),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_webinars_cat_slug',
        'type'    => 'text',
        'title'   => esc_html__('Webinars Category Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'webinars-category'
        ),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'type'    => 'notice',
        'class'   => 'danger',
        'content' => esc_html__('Important: Please do not set webinars slug and page slug as same. It\'ll not work.', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      // Webinars Name

      // webinars listing
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Webinars Style', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'             => 'webinars_style',
        'type'           => 'select',
        'title'          => esc_html__('Webinars Style', 'saaspot'),
        'options'        => array(
          'one' => esc_html__('Style One', 'saaspot'),
          'two' => esc_html__('Style Two (Grid)', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Webinars Style', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'          => 'video_text',
        'type'        => 'text',
        'title'       => esc_html__('Video Text', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'webinars_limit',
        'type'    => 'text',
        'title'   => esc_html__('Webinars Limit', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => '10'
        ),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'webinars_show_category',
        'type'    => 'text',
        'title'   => esc_html__('Webinars Categories', 'saaspot'),
        'info'   => esc_html__('Enter category SLUGS (comma separated) you want to display.', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'             => 'webinars_orderby',
        'type'           => 'select',
        'title'          => esc_html__('Order By', 'saaspot'),
        'options'        => array(
          'none' => esc_html__('None', 'saaspot'),
          'ID' => esc_html__('ID', 'saaspot'),
          'author' => esc_html__('Author', 'saaspot'),
          'title' => esc_html__('Title', 'saaspot'),
          'date' => esc_html__('Date', 'saaspot'),
          'name' => esc_html__('Name', 'saaspot'),
          'modified' => esc_html__('Modified', 'saaspot'),
          'rand' => esc_html__('Random', 'saaspot'),
          'menu_order' => esc_html__('Menu Order', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Webinars Order By', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'             => 'webinars_order',
        'type'           => 'select',
        'title'          => esc_html__('Order', 'saaspot'),
        'options'        => array(
          'ASC' => esc_html__('Asending', 'saaspot'),
          'DESC' => esc_html__('Desending', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Webinars Order', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),

      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Enable/Disable Options', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'    => 'webinars_aqr',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Image Resize?', 'saaspot'),
        'info' => esc_html__('If need to disable image resize, please turn this ON.', 'saaspot'),
        'default' => false,
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'webinars_filter',
        'type'    => 'switcher',
        'title'   => esc_html__('Filter', 'saaspot'),
        'label'   => esc_html__('If you need filter in webinars pages, please turn this ON.', 'saaspot'),
        'default'   => true,
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'webinars_pagination',
        'type'    => 'switcher',
        'title'   => esc_html__('Pagination', 'saaspot'),
        'label'   => esc_html__('If you need pagination in webinars pages, please turn this ON.', 'saaspot'),
        'default'   => true,
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),

      // Webinars Listing

      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Related Webinars', 'saaspot'),
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'webinars_need_related',
        'type'    => 'switcher',
        'title'   => esc_html__('Need Related Webinars', 'saaspot'),
        'label'   => esc_html__('If you need related projects in webinars single pages, please turn this ON.', 'saaspot'),
        'default'   => true,
        'dependency'   => array('noneed_webinars_post', '==', 'false'),
      ),
      array(
        'id'      => 'webinars_related_title',
        'type'    => 'text',
        'title'   => esc_html__('Apps Related Webinars Title', 'saaspot'),
        'info'   => esc_html__('Enter related projects title.', 'saaspot'),
        'dependency'     => array('webinars_need_related|noneed_webinars_post', '==|==', 'true|false'),
      ),
      array(
        'id'      => 'webinars_related_content',
        'type'    => 'textarea',
        'title'   => esc_html__('Apps Related Webinars Title', 'saaspot'),
        'info'   => esc_html__('Enter related projects title.', 'saaspot'),
        'dependency'     => array('webinars_need_related|noneed_webinars_post', '==|==', 'true|false'),
      ),

    ),
  );

  // ------------------------------
  // Job Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'job_section',
    'title'    => esc_html__('Job', 'saaspot'),
    'icon'     => 'fa fa-black-tie',
    'fields' => array(

      // job name change
      array(
        'id'    => 'noneed_job_post',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Job Post?', 'saaspot'),
        'info' => esc_html__('If need to disable this post type, please turn this ON.', 'saaspot'),
        'default' => false,
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Name Change', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_job_name',
        'type'    => 'text',
        'title'   => esc_html__('Job Name', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'Job'
        ),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_job_slug',
        'type'    => 'text',
        'title'   => esc_html__('Job Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'job-item'
        ),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_job_cat_slug',
        'type'    => 'text',
        'title'   => esc_html__('Job Category Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'job-category'
        ),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'type'    => 'notice',
        'class'   => 'danger',
        'content' => esc_html__('Important: Please do not set job slug and page slug as same. It\'ll not work.', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      // Job Name

      // job listing
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Job Style', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'      => 'job_limit',
        'type'    => 'text',
        'title'   => esc_html__('Job Limit', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => '10'
        ),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'      => 'applay_job',
        'type'    => 'text',
        'title'   => esc_html__('Applay Button Text', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'      => 'job_show_category',
        'type'    => 'text',
        'title'   => esc_html__('Job Roles', 'saaspot'),
        'info'   => esc_html__('Enter category SLUGS (comma separated) you want to display.', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'             => 'job_orderby',
        'type'           => 'select',
        'title'          => esc_html__('Order By', 'saaspot'),
        'options'        => array(
          'none' => esc_html__('None', 'saaspot'),
          'ID' => esc_html__('ID', 'saaspot'),
          'author' => esc_html__('Author', 'saaspot'),
          'title' => esc_html__('Title', 'saaspot'),
          'date' => esc_html__('Date', 'saaspot'),
          'name' => esc_html__('Name', 'saaspot'),
          'modified' => esc_html__('Modified', 'saaspot'),
          'rand' => esc_html__('Random', 'saaspot'),
          'menu_order' => esc_html__('Menu Order', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Job Order By', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'             => 'job_order',
        'type'           => 'select',
        'title'          => esc_html__('Order', 'saaspot'),
        'options'        => array(
          'ASC' => esc_html__('Asending', 'saaspot'),
          'DESC' => esc_html__('Desending', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Job Order', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Enable/Disable Options', 'saaspot'),
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      array(
        'id'      => 'job_pagination',
        'type'    => 'switcher',
        'title'   => esc_html__('Pagination', 'saaspot'),
        'label'   => esc_html__('If you need pagination in job pages, please turn this ON.', 'saaspot'),
        'default'   => true,
        'dependency'   => array('noneed_job_post', '==', 'false'),
      ),
      // Job Listing

    ),
  );

  // ------------------------------
  // Testimonial Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'testimonial_section',
    'title'    => esc_html__('Testimonial', 'saaspot'),
    'icon'     => 'fa fa-users',
    'fields' => array(

      // Testimonial name change
      array(
        'id'    => 'noneed_testimonial_post',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Testimonial Post?', 'saaspot'),
        'info' => esc_html__('If need to disable this post type, please turn this ON.', 'saaspot'),
        'default' => false,
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Name Change', 'saaspot'),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_testimonial_name',
        'type'    => 'text',
        'title'   => esc_html__('Testimonial Name', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'Testimonial'
        ),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_testimonial_slug',
        'type'    => 'text',
        'title'   => esc_html__('Testimonial Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'testimonial-item'
        ),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_testimonial_cat_slug',
        'type'    => 'text',
        'title'   => esc_html__('Testimonial Category Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'testimonial-category'
        ),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'type'    => 'notice',
        'class'   => 'danger',
        'content' => esc_html__('Important: Please do not set testimonial slug and page slug as same. It\'ll not work.', 'saaspot'),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      // Testimonial Name

      // Testimonial Start
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Testimonial Options', 'saaspot'),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'id'             => 'testimonial_style',
        'type'           => 'select',
        'title'          => esc_html__('Testimonial Style', 'saaspot'),
        'options'        => array(
          'testimonial_one'          => esc_html__('Style One', 'saaspot'),
          'testimonial_two'          => esc_html__('Style Two', 'saaspot'),
        ),
        'default_option' => esc_html__('Select Testimonial Style', 'saaspot'),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'id'      => 'testimonial_limit',
        'type'    => 'text',
        'title'   => esc_html__('Testimonial Limit', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => '3'
        ),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'id'             => 'testimonial_orderby',
        'type'           => 'select',
        'title'          => esc_html__('Order By', 'saaspot'),
        'options'        => array(
          'none' => esc_html__('None', 'saaspot'),
          'ID' => esc_html__('ID', 'saaspot'),
          'author' => esc_html__('Author', 'saaspot'),
          'title' => esc_html__('Title', 'saaspot'),
          'date' => esc_html__('Date', 'saaspot'),
          'name' => esc_html__('Name', 'saaspot'),
          'modified' => esc_html__('Modified', 'saaspot'),
          'rand' => esc_html__('Random', 'saaspot'),
          'menu_order' => esc_html__('Menu Order', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Testimonial Order By', 'saaspot'),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      array(
        'id'             => 'testimonial_order',
        'type'           => 'select',
        'title'          => esc_html__('Order', 'saaspot'),
        'options'        => array(
          'ASC' => esc_html__('Asending', 'saaspot'),
          'DESC' => esc_html__('Desending', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Testimonial Order', 'saaspot'),
        'dependency'   => array('noneed_testimonial_post', '==', 'false'),
      ),
      // Testimonial End

    ),
  );

  // ------------------------------
  // Team Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'team_section',
    'title'    => esc_html__('Team', 'saaspot'),
    'icon'     => 'fa fa-users',
    'fields' => array(

      // Team name change
      array(
        'id'    => 'noneed_team_post',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Team Post?', 'saaspot'),
        'info' => esc_html__('If need to disable this post type, please turn this ON.', 'saaspot'),
        'default' => false,
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Name Change', 'saaspot'),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_team_name',
        'type'    => 'text',
        'title'   => esc_html__('Team Name', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'Team'
        ),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_team_slug',
        'type'    => 'text',
        'title'   => esc_html__('Team Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'team-item'
        ),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'id'      => 'theme_team_cat_slug',
        'type'    => 'text',
        'title'   => esc_html__('Team Category Slug', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => 'team-category'
        ),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'type'    => 'notice',
        'class'   => 'danger',
        'content' => esc_html__('Important: Please do not set team slug and page slug as same. It\'ll not work.', 'saaspot'),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      // Team Name

      // Team Start
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Team Options', 'saaspot'),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'id'    => 'team_aqr',
        'type'  => 'switcher',
        'title' => esc_html__('Disable Image Resize?', 'saaspot'),
        'info' => esc_html__('If need to disable image resize, please turn this ON.', 'saaspot'),
        'default' => false,
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'id'      => 'team_limit',
        'type'    => 'text',
        'title'   => esc_html__('Team Limit', 'saaspot'),
        'attributes'     => array(
          'placeholder'  => '8'
        ),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'id'             => 'team_orderby',
        'type'           => 'select',
        'title'          => esc_html__('Order By', 'saaspot'),
        'options'        => array(
          'none' => esc_html__('None', 'saaspot'),
          'ID' => esc_html__('ID', 'saaspot'),
          'author' => esc_html__('Author', 'saaspot'),
          'title' => esc_html__('Title', 'saaspot'),
          'date' => esc_html__('Date', 'saaspot'),
          'name' => esc_html__('Name', 'saaspot'),
          'modified' => esc_html__('Modified', 'saaspot'),
          'rand' => esc_html__('Random', 'saaspot'),
          'menu_order' => esc_html__('Menu Order', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Team Order By', 'saaspot'),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),
      array(
        'id'             => 'team_order',
        'type'           => 'select',
        'title'          => esc_html__('Order', 'saaspot'),
        'options'        => array(
          'ASC' => esc_html__('Asending', 'saaspot'),
          'DESC' => esc_html__('Desending', 'saaspot'),
        ),
        'default_option'     => esc_html__('Select Team Order', 'saaspot'),
        'dependency'   => array('noneed_team_post', '==', 'false'),
      ),

      // Team End

    ),
  );

  // ------------------------------
  // Blog Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'blog_section',
    'title'    => esc_html__('Blog', 'saaspot'),
    'icon'     => 'fa fa-edit',
    'sections' => array(

      // blog general section
      array(
        'name'     => 'blog_general_tab',
        'title'    => esc_html__('General', 'saaspot'),
        'icon'     => 'fa fa-cog',
        'fields'   => array(

          // Layout
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Layout', 'saaspot')
          ),
          array(
            'id'             => 'blog_listing_style',
            'type'           => 'select',
            'title'          => esc_html__('Blog Listing Style', 'saaspot'),
            'options'        => array(
              'style-one'   => esc_html__('Style One (List)', 'saaspot'),
              'style-two'   => esc_html__('Style Two (Grid)', 'saaspot'),
            ),
            // 'default_option' => 'Select blog style',
            'help'          => esc_html__('This style will apply, default blog pages - Like : Archive, Category, Tags, Search & Author. If this settings will not apply your blog page, please set that page as a post page in Settings > Readings.', 'saaspot'),
          ),
          array(
            'id'             => 'blog_listing_columns',
            'type'           => 'select',
            'title'          => esc_html__('Blog Listing Columns', 'saaspot'),
            'options'        => array(
              'col-3' => esc_html__('Column Three', 'saaspot'),
              'col-2' => esc_html__('Column Two', 'saaspot'),
            ),
            'default_option' => 'Select blog column',
            'dependency'     => array('blog_listing_style', '==', 'style-two'),
          ),
          array(
            'id'             => 'blog_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'saaspot'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'saaspot'),
              'sidebar-left' => esc_html__('Left', 'saaspot'),
              'sidebar-hide' => esc_html__('Hide', 'saaspot'),
            ),
            'default_option' => 'Select sidebar position',
            'help'          => esc_html__('This style will apply, default blog pages - Like : Archive, Category, Tags, Search & Author.', 'saaspot'),
            'info'          => esc_html__('Default option : Right', 'saaspot'),
          ),
          array(
            'id'             => 'blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'saaspot'),
            'options'        => saaspot_vt_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'saaspot'),
            'dependency'     => array('blog_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'saaspot'),
          ),
          // Layout
          // Global Options
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Global Options', 'saaspot')
          ),
          array(
            'id'    => 'blog_aqr',
            'type'  => 'switcher',
            'title' => esc_html__('Disable Image Resize?', 'saaspot'),
            'info' => esc_html__('If need to disable image resize, please turn this ON.', 'saaspot'),
            'default' => false,
          ),
          array(
            'id'         => 'theme_exclude_categories',
            'type'       => 'checkbox',
            'title'      => esc_html__('Exclude Categories', 'saaspot'),
            'info'      => esc_html__('Select categories you want to exclude from blog page.', 'saaspot'),
            'options'    => 'categories',
          ),
          array(
            'id'      => 'theme_blog_excerpt',
            'type'    => 'text',
            'title'   => esc_html__('Excerpt Length', 'saaspot'),
            'info'   => esc_html__('Blog short content length, in blog listing pages.', 'saaspot'),
            'default' => '35',
          ),
          array(
            'id'      => 'theme_metas_hide',
            'type'    => 'checkbox',
            'title'   => esc_html__('Meta\'s to hide', 'saaspot'),
            'info'    => esc_html__('Check items you want to hide from blog/post meta field.', 'saaspot'),
            'class'      => 'horizontal',
            'options'    => array(
              'category'   => esc_html__('Category', 'saaspot'),
              'tag'   => esc_html__('Tag', 'saaspot'),
              'date'    => esc_html__('Date', 'saaspot'),
              'author'     => esc_html__('Author', 'saaspot'),
            ),
          ),
          array(
            "id"  => "blog_date_format",
            "type"        => 'text',
            "title"     => esc_html__('Date Format', 'saaspot'),
            "info" => esc_html__( "Enter date format (for more info <a href='https://codex.wordpress.org/Formatting_Date_and_Time' target='_blank'>click here</a>).", 'saaspot')
          ),
          // End fields

        )
      ),

      // blog single section
      array(
        'name'     => 'blog_single_tab',
        'title'    => esc_html__('Single', 'saaspot'),
        'icon'     => 'fa fa-sticky-note',
        'fields'   => array(

          // Start fields
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Enable / Disable', 'saaspot')
          ),
          array(
            'id'    => 'single_tag_list',
            'type'  => 'switcher',
            'title' => esc_html__('Tags', 'saaspot'),
            'info' => esc_html__('If need to hide tags from single blog post page, please turn this OFF.', 'saaspot'),
            'default' => true,
          ),
          array(
            'id'    => 'single_author_info',
            'type'  => 'switcher',
            'title' => esc_html__('Author Info', 'saaspot'),
            'info' => esc_html__('If need to hide author info on single blog page, please turn this OFF.', 'saaspot'),
            'default' => true,
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Related Posts', 'saaspot')
          ),
          array(
            'id'    => 'single_related_post',
            'type'  => 'switcher',
            'title' => esc_html__('Related Posts', 'saaspot'),
            'info' => esc_html__('If need to hide related posts on single blog page, please turn this OFF.', 'saaspot'),
            'default' => true,
          ),
          array(
            'id'      => 'single_related_title',
            'type'    => 'text',
            'title'   => esc_html__('Section Title', 'saaspot'),
            'info'   => esc_html__('Related post section title.', 'saaspot'),
            'dependency'     => array('single_related_post', '==', 'true'),
          ),
          array(
            'id'      => 'single_related_limit',
            'type'    => 'text',
            'title'   => esc_html__('Post Limit', 'saaspot'),
            'info'   => esc_html__('Related post limit, in single blog page.', 'saaspot'),
            'default' => '2',
            'dependency'     => array('single_related_post', '==', 'true'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Sidebar', 'saaspot')
          ),
          array(
            'id'             => 'single_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'saaspot'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'saaspot'),
              'sidebar-left' => esc_html__('Left', 'saaspot'),
              'sidebar-hide' => esc_html__('Hide', 'saaspot'),
            ),
            'default_option' => 'Select sidebar position',
            'info'          => esc_html__('Default option : Right', 'saaspot'),
          ),
          array(
            'id'             => 'single_blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'saaspot'),
            'options'        => saaspot_vt_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'saaspot'),
            'dependency'     => array('single_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'saaspot'),
          ),
          // End fields

        )
      ),

    ),
  );

  // ------------------------------
  // Extra Pages
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_extra_pages',
    'title'    => esc_html__('Extra Pages', 'saaspot'),
    'icon'     => 'fa fa-clone',
    'sections' => array(

      // error 404 page
      array(
        'name'     => 'error_page_section',
        'title'    => esc_html__('404 Page', 'saaspot'),
        'icon'     => 'fa fa-exclamation-triangle',
        'fields'   => array(

          // Start 404 Page
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('404 Error Page Options', 'saaspot')
          ),
          array(
            'id'    => 'error_page_title',
            'type'  => 'text',
            'title' => esc_html__('404 Page Title', 'saaspot'),
            'info'  => esc_html__('Enter title text here.', 'saaspot'),
          ),
          array(
            'id'    => 'error_title_content',
            'type'  => 'text',
            'title' => esc_html__('404 Title Content', 'saaspot'),
            'info'  => esc_html__('Enter content text here.', 'saaspot'),
          ),
          array(
            'id'    => 'error_heading',
            'type'  => 'text',
            'title' => esc_html__('404 Page Heading', 'saaspot'),
            'info'  => esc_html__('Enter 404 page heading.', 'saaspot'),
          ),
          array(
            'id'    => 'error_page_content',
            'type'  => 'textarea',
            'title' => esc_html__('404 Page Content', 'saaspot'),
            'info'  => esc_html__('Enter 404 page content.', 'saaspot'),
            'shortcode' => true,
          ),
          array(
            'id'    => 'error_page_bg',
            'type'  => 'image',
            'title' => esc_html__('404 Page Background', 'saaspot'),
            'info'  => esc_html__('Choose 404 page background image.', 'saaspot'),
            'add_title' => esc_html__('Add 404 Image', 'saaspot'),
          ),
          array(
            'id'    => 'error_btn_text',
            'type'  => 'text',
            'title' => esc_html__('Button Text', 'saaspot'),
            'info'  => esc_html__('Enter BACK TO HOME button text. If you want to change it.', 'saaspot'),
          ),
          // End 404 Page

        ) // end: fields
      ), // end: fields section

      // maintenance mode page
      array(
        'name'     => 'maintenance_mode_section',
        'title'    => esc_html__('Maintenance Mode', 'saaspot'),
        'icon'     => 'fa fa-hourglass-half',
        'fields'   => array(

          // Start Maintenance Mode
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('If you turn this ON : Only Logged in users will see your pages. All other visiters will see, selected page of : Maintenance Mode Page', 'saaspot')
          ),
          array(
            'id'             => 'enable_maintenance_mode',
            'type'           => 'switcher',
            'title'          => esc_html__('Maintenance Mode', 'saaspot'),
            'default'        => false,
          ),
          array(
            'id'             => 'maintenance_mode_page',
            'type'           => 'select',
            'title'          => esc_html__('Maintenance Mode Page', 'saaspot'),
            'options'        => 'pages',
            'default_option' => esc_html__('Select a page', 'saaspot'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          // End Maintenance Mode

        ) // end: fields
      ), // end: fields section

    )
  );

  // ------------------------------
  // Advanced
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_advanced',
    'title'  => esc_html__('Advanced', 'saaspot'),
    'icon'   => 'fa fa-cog'
  );

  // ------------------------------
  // Misc Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'misc_section',
    'title'    => esc_html__('Misc', 'saaspot'),
    'icon'     => 'fa fa-recycle',
    'sections' => array(

      // custom sidebar section
      array(
        'name'     => 'custom_sidebar_section',
        'title'    => esc_html__('Custom Sidebar', 'saaspot'),
        'icon'     => 'fa fa-reorder',
        'fields'   => array(

          // start fields
          array(
            'id'              => 'custom_sidebar',
            'title'           => esc_html__('Sidebars', 'saaspot'),
            'desc'            => esc_html__('Go to Appearance -> Widgets after create sidebars', 'saaspot'),
            'type'            => 'group',
            'fields'          => array(
              array(
                'id'          => 'sidebar_name',
                'type'        => 'text',
                'title'       => esc_html__('Sidebar Name', 'saaspot'),
              ),
              array(
                'id'          => 'sidebar_desc',
                'type'        => 'text',
                'title'       => esc_html__('Custom Description', 'saaspot'),
              )
            ),
            'accordion'       => true,
            'button_title'    => esc_html__('Add New Sidebar', 'saaspot'),
            'accordion_title' => esc_html__('New Sidebar', 'saaspot'),
          ),
          // end fields
        )
      ),
      // custom sidebar section

      // Translation
      array(
        'name'        => 'theme_translation_section',
        'title'       => esc_html__('Translation', 'saaspot'),
        'icon'        => 'fa fa-language',

        // begin: fields
        'fields'      => array(
          // Start Translation
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Common Texts', 'saaspot')
          ),
          array(
            'id'          => 'read_more_text',
            'type'        => 'text',
            'title'       => esc_html__('Read More Text', 'saaspot'),
          ),
          array(
            'id'          => 'share_text',
            'type'        => 'text',
            'title'       => esc_html__('Share Text', 'saaspot'),
          ),
          array(
            'id'          => 'share_on_text',
            'type'        => 'text',
            'title'       => esc_html__('Share On Tooltip Text', 'saaspot'),
          ),
          array(
            'id'          => 'author_text',
            'type'        => 'text',
            'title'       => esc_html__('Author Text', 'saaspot'),
          ),
          array(
            'id'          => 'post_comment_text',
            'type'        => 'text',
            'title'       => esc_html__('Post Comment Text [Submit Button]', 'saaspot'),
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Filter All Texts', 'saaspot')
          ),
          array(
            'id'          => 'all_text',
            'type'        => 'text',
            'title'       => esc_html__('Filter All Text (Apps)', 'saaspot'),
          ),
          array(
            'id'          => 'webi_all_text',
            'type'        => 'text',
            'title'       => esc_html__('Filter All Text (Webinars)', 'saaspot'),
          ),
          // End Translation
        ) // end: fields
      ),

    ),
  );

  // ------------------------------
  // backup                       -
  // ------------------------------
  $options[]   = array(
    'name'     => 'backup_section',
    'title'    => 'Backup',
    'icon'     => 'fa fa-shield',
    'fields'   => array(

      array(
        'type'    => 'notice',
        'class'   => 'warning',
        'content' => 'You can save your current options. Download a Backup and Import.',
      ),
      array(
        'type'    => 'backup',
      ),

    )
  );
  return $options;
}
add_filter( 'cs_framework_options', 'saaspot_vt_options' );
