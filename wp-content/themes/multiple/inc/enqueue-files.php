<?php
/*
 * All CSS and JS files are enqueued from this file
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/**
 * Enqueue Files for FrontEnd
 */
if ( ! function_exists( 'saaspot_vt_scripts_styles' ) ) {
  function saaspot_vt_scripts_styles() {

    // Styles
    wp_enqueue_style( 'font-awesome', SAASPOT_CSS . '/font-awesome.min.css', array(), '4.7.0', 'all' );
    wp_enqueue_style( 'animate', SAASPOT_CSS .'/animate.min.css', array(), '3.6.0', 'all' );
    wp_enqueue_style( 'themify-icons', SAASPOT_CSS .'/themify-icons.min.css', array(), SAASPOT_VERSION, 'all' );
    wp_enqueue_style( 'linea', SAASPOT_CSS .'/linea.min.css', array(), SAASPOT_VERSION, 'all' );
    wp_enqueue_style( 'loaders', SAASPOT_CSS .'/loaders.min.css', array(), SAASPOT_VERSION, 'all' );
    wp_enqueue_style( 'magnific-popup', SAASPOT_CSS .'/magnific-popup.min.css', array(), SAASPOT_VERSION, 'all' );
    wp_enqueue_style( 'nice-select', SAASPOT_CSS .'/nice-select.min.css', array(), SAASPOT_VERSION, 'all' );
    wp_enqueue_style( 'owl-carousel', SAASPOT_CSS .'/owl.carousel.min.css', array(), '2.3.0', 'all' );
    wp_enqueue_style( 'meanmenu', SAASPOT_CSS .'/meanmenu.css', array(), '2.0.7', 'all' );
    wp_enqueue_style( 'bootstrap', SAASPOT_CSS .'/bootstrap.min.css', array(), '4.1.3', 'all' );
    wp_enqueue_style( 'saaspot-style', SAASPOT_CSS .'/styles.css', array(), SAASPOT_VERSION, 'all' );

    // Scripts
    wp_enqueue_script( 'popper', SAASPOT_SCRIPTS . '/popper.min.js', array( 'jquery' ), SAASPOT_VERSION, true );
    wp_enqueue_script( 'bootstrap', SAASPOT_SCRIPTS . '/bootstrap.min.js', array( 'jquery' ), '4.1.3', true );
    wp_enqueue_script( 'html5shiv', SAASPOT_SCRIPTS . '/html5shiv.min.js', array( 'jquery' ), '3.7.0', true );
    wp_enqueue_script( 'respond', SAASPOT_SCRIPTS . '/respond.min.js', array( 'jquery' ), '1.4.2', true );
    wp_enqueue_script( 'placeholders', SAASPOT_SCRIPTS . '/placeholders.min.js', array( 'jquery' ), '4.0.1', true );
    wp_enqueue_script( 'sticky', SAASPOT_SCRIPTS . '/jquery.sticky.min.js', array( 'jquery' ), '1.0.4', true );
    wp_enqueue_script( 'nice-select', SAASPOT_SCRIPTS . '/jquery.nice-select.min.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'jarallax', SAASPOT_SCRIPTS . '/jarallax.min.js', array( 'jquery' ), '1.10.5', true );
    wp_enqueue_script( 'matchHeight', SAASPOT_SCRIPTS . '/jquery.matchHeight-min.js', array( 'jquery' ), '0.7.2', true );
    wp_enqueue_script( 'waypoints', SAASPOT_SCRIPTS . '/waypoints.min.js', array( 'jquery' ), '2.0.3', true );
    wp_enqueue_script( 'wow', SAASPOT_SCRIPTS . '/wow.min.js', array( 'jquery' ), '1.1.3', true );
    wp_enqueue_script( 'owl-carousel', SAASPOT_SCRIPTS . '/owl.carousel.min.js', array( 'jquery' ), '2.3.0', true );
    wp_enqueue_script( 'isotope', SAASPOT_SCRIPTS . '/isotope.min.js', array( 'jquery' ), '3.0.1', true );
    wp_enqueue_script( 'packery-mode', SAASPOT_SCRIPTS . '/packery-mode.pkgd.min.js', array( 'jquery' ), '2.0.0', true );
    wp_enqueue_script( 'page-scroll', SAASPOT_SCRIPTS . '/page-scroll.min.js', array( 'jquery' ), '1.5.8', true );
    wp_enqueue_script( 'counterup', SAASPOT_SCRIPTS . '/jquery.counterup.min.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'magnific-popup', SAASPOT_SCRIPTS . '/jquery.magnific-popup.min.js', array( 'jquery' ), '1.1.0', true );
    wp_enqueue_script( 'loaders', SAASPOT_SCRIPTS . '/loaders.min.js', array( 'jquery' ), SAASPOT_VERSION, true );
    wp_enqueue_script( 'meanmenu', SAASPOT_SCRIPTS . '/jquery.meanmenu.js', array( 'jquery' ), '2.0.8', true );
    wp_enqueue_script( 'saaspot-scripts', SAASPOT_SCRIPTS . '/scripts.js', array( 'jquery' ), SAASPOT_VERSION, true );

    // Comments
    wp_enqueue_script( 'validate', SAASPOT_SCRIPTS . '/jquery.validate.min.js', array( 'jquery' ), '1.9.0', true );
    wp_add_inline_script( 'validate', 'jQuery(document).ready(function($) {$("#commentform").validate({rules: {author: {required: true,minlength: 2},email: {required: true,email: true},comment: {required: true,minlength: 10}}});});' );

    // Responsive
    wp_enqueue_style( 'saaspot-responsive', SAASPOT_CSS .'/responsive.css', array(), SAASPOT_VERSION, 'all' );

    // Dynamic Styles
    wp_enqueue_style( 'dynamic-style', SAASPOT_THEMEROOT_URI . '/inc/dynamic-style.php', array(), SAASPOT_VERSION, 'all' );

    // Adds support for pages with threaded comments
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }

  }
  add_action( 'wp_enqueue_scripts', 'saaspot_vt_scripts_styles' );
}

/**
 * Enqueue Files for BackEnd
 */
if ( ! function_exists( 'saaspot_vt_admin_scripts_styles' ) ) {
  function saaspot_vt_admin_scripts_styles() {

    wp_enqueue_style( 'saaspot-admin-main', SAASPOT_CSS . '/admin-styles.css', true );
    wp_enqueue_script( 'saaspot-admin-scripts', SAASPOT_SCRIPTS . '/admin-scripts.js', true );
    wp_enqueue_style( 'themify-icons', SAASPOT_CSS .'/themify-icons.min.css', array(), SAASPOT_VERSION, 'all' );
    wp_enqueue_style( 'linea', SAASPOT_CSS .'/linea.min.css', array(), SAASPOT_VERSION, 'all' );
  }
  add_action( 'admin_enqueue_scripts', 'saaspot_vt_admin_scripts_styles' );
}

/* Enqueue All Styles */
if ( ! function_exists( 'saaspot_vt_wp_enqueue_styles' ) ) {
  function saaspot_vt_wp_enqueue_styles() {
    saaspot_vt_google_fonts();
  }
  add_action( 'wp_enqueue_scripts', 'saaspot_vt_wp_enqueue_styles' );
}

/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function saaspot_add_editor_styles() {
  add_editor_style( get_stylesheet_uri() );
}
add_action( 'init', 'saaspot_add_editor_styles' );
