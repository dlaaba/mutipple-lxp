<?php
/*
* ---------------------------------------------------------------------
* VictorThemes Dynamic Style
* ---------------------------------------------------------------------
*/

header("Content-type: text/css;");
$absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once($wp_load);

/* Custom Font */
$all_element_color  = cs_get_customize_option( 'all_element_colors' );
$all_element_secondary_colors = cs_get_customize_option('all_element_secondary_colors');
if($all_element_color) {
?>
  .no-class {}
  .saspot-btn:hover, .saspot-btn:focus, .saspot-btn.saspot-light-blue-btn, .saspot-social.rounded a:hover, .saspot-social.rounded a:focus, .agency-item .saspot-label, .testimonial-author-wrap, input[type="submit"], .saspot-download, .banner-subtitle .saspot-label, .looking-item.saspot-hover .saspot-btn, .saspot-link:after, .job-item.saspot-hover .saspot-btn, .class-item.saspot-hover .saspot-light-blue-bdr-btn, .saspot-menu, .saspot-callout.callout-style-five, .process-counter, .promote-counter, .saspot-callout.callout-style-eight, .market-types-style-two .market-wrap .saspot-link:after, .saspot-campaign.campaign-style-two, .cookies-link:hover, .cookies-link:focus, .saspot-conference .saspot-btn:hover {
    background-color: <?php echo esc_attr($all_element_color); ?>;
  }

  .saspot-btn.saspot-light-blue-bdr-btn, ul:not(.elementor-nav-menu) .dropdown-nav li.all-resources a, .header-contact-link a, .banner-subtitle a, .saspot-link, .saspot-link:hover, .check-list li:before, .saspot-page-title.page-title-style-five p, .create-account .saspot-link:hover, .guide-details ul li .fa-check, .table td .fa-check, .place-subtitle, .extension-price span, .wpcf7-list-item-label a:hover, .wpcf7-list-item-label a:focus, .callout-style-eight .saspot-btn.saspot-white-btn, .callout-style-ten .saspot-btn.saspot-white-btn, .market-types-style-two .market-wrap .saspot-link, .price-top-subtitle, .request-bottom-wrap a:hover, .dropdown-nav li .inline-link a, .saspot-cookies p a, input[type="submit"]:hover, button[type="submit"]:hover, .page-title-wrap .blog-date ul li a:hover, .saspot-losi-wrap .form-wrap form .forgot-link a:hover {
    color: <?php echo esc_attr($all_element_color); ?>;
  }

  .saspot-btn.saspot-light-blue-bdr-btn, input[type="submit"]:hover, button[type="submit"]:hover {border-color: <?php echo esc_attr($all_element_color); ?>;}

<?php } if($all_element_secondary_colors) { ?>
  .no-class {}
  .saspot-preloader, .saspot-label, .saspot-navigation > ul > li.active .menu-text:before, .saspot-navigation > ul > .current-page-ancestor .menu-text:before, .saspot-btn, .enterprises-wrap, .saspot-back-top a, .saspot-sidebar .saspot-widget ul li:before, .bullet-list li:before, .saspot-trial, .feature-item .saspot-icon, .saspot-file-selector .saspot-icon, .masonry-filters ul li a:before, .saspot-btn.saspot-light-blue-btn:hover, .saspot-btn.saspot-light-blue-btn:focus, .commission-wrap, .saspot-callout.callout-style-four, .video-btn:hover, .saspot-testimonials.testimonials-style-two, .service-item .saspot-icon, .value-item .saspot-icon, .tabs-style-two .nav-link:after, .corporate-item .saspot-icon, .benefit-item .saspot-icon, .testimonials-style-three .author-name:before, .video-style-three .video-btn, .saspot-market-types, .plan-feature-item .checkbox-icon-wrap input[type="checkbox"]:checked + .checkbox-icon:before, .tabs-style-three .nav-link:after, .request-bottom-wrap .saspot-social.rounded a:hover, .cookies-link, .cookies-close:hover:before, .cookies-close:hover:after, .wp-link-pages a, .wp-pagenavi > a:after, .wp-pagenavi > span:after, .saspot-pagination ul li a:after, .saspot-pagination ul li span:after, .saspot-conference .saspot-btn, .blog-info .saspot-link:after {
    background-color: <?php echo esc_attr($all_element_secondary_colors); ?>;
  }

  ::selection {background: <?php echo esc_attr($all_element_secondary_colors); ?>;}
  ::-webkit-selection {background: <?php echo esc_attr($all_element_secondary_colors); ?>;}
  ::-moz-selection {background: <?php echo esc_attr($all_element_secondary_colors); ?>;}
  ::-o-selection {background: <?php echo esc_attr($all_element_secondary_colors); ?>;}
  ::-ms-selection {background: <?php echo esc_attr($all_element_secondary_colors); ?>;}

  a:hover, a:focus, .saspot-topbar ul li a:hover, .saspot-topbar ul li a:focus, .saspot-navigation > ul > li:hover > a, .saspot-navigation > ul > li.active > a, .saspot-navigation > ul > .current-page-ancestor > a, .dropdown-nav li a:hover .menu-subtitle, ul:not(.elementor-nav-menu) .dropdown-nav li.all-resources a:hover, .banner-subtitle a:hover, .client-subtitle, .footer-widget ul li a:hover, .footer-widget ul li a:focus, .saspot-copyright p a:hover, .step-subtitle, .section-title span, .checkbox-icon-wrap input[type="checkbox"]:checked + .checkbox-icon:before, .job-label, .saspot-btn.saspot-white-btn, .video-btn, .class-date, .indepth-subtitle, .guide-label, .guide-main-label, .arrow-wrap:before, .wpcf7-list-item-label a, .market-role-item i, .common-feature-inner .saspot-icon, .analytics-inner .icon-linea, .accordion-title a:before, .pricing-plan-title span, .nav-tabs.tabs-style-three .nav-item.show .nav-link, .nav-tabs.tabs-style-three .nav-link.active, .nav-tabs.tabs-style-three .nav-link:focus, .nav-tabs.tabs-style-three .nav-link:hover, .chat-support-item .icon-linea, .no-cost-wrap .saspot-icon .icon-linea, .saspot-sidebar .saspot-widget ul li a:hover, .bullet-list li a:hover, .masonry-filters.filters-style-two ul li a.active, .use-step-arrow:before, .navigation-wrap .dropdown-nav li a:hover, .saspot-cookies p a:hover, .dropdown-nav li .inline-link a:hover, .saspot-pagination, .features-style-three .feature-item .saspot-icon, .chat-support-item .saspot-icon i, .blog-info a.saspot-link, .saspot-meta .author a:hover, .blog-date li a:hover, .saspot-sidebar .saspot-widget .blog-date li a:hover, .blog-detail-wrap .elementor-text-editor ol, .saspot-comments-area .comment-reply-link {
    color: <?php echo esc_attr($all_element_secondary_colors); ?>;
  }

  .navigation-wrap .saspot-navigation > ul > li.active, .saspot-file-selector label:hover, blockquote, .wp-link-pages a, .blog-item.sticky {
    border-color: <?php echo esc_attr($all_element_secondary_colors); ?>;
  }
<?php } ?>

<!-- Topbar Colors -->
<?php
$topbar_bg_color = cs_get_customize_option( 'topbar_bg_color' );
$topbar_border_color  = cs_get_customize_option( 'topbar_border_color' );
$topbar_text_color = cs_get_customize_option( 'topbar_text_color' );
$topbar_link_color = cs_get_customize_option( 'topbar_link_color' );
$topbar_link_hover_color  = cs_get_customize_option( 'topbar_link_hover_color' );
$topbar_label_color = cs_get_customize_option( 'topbar_label_color' );
$topbar_label_bg_color  = cs_get_customize_option( 'topbar_label_bg_color' );
if($topbar_bg_color) { ?>
.no-class {}
.saspot-topbar {
  background-color: <?php echo esc_attr($topbar_bg_color); ?>;
}
<?php } if($topbar_border_color) { ?>
.no-class {}
.saspot-topbar {
  border-color: <?php echo esc_attr($topbar_border_color); ?>;
}
<?php } if($topbar_text_color) { ?>
.no-class {}
.saspot-topbar ul, 
.saspot-topbar ul li, 
.saspot-topbar ul li span, 
.saspot-topbar {
  color: <?php echo esc_attr($topbar_text_color); ?>;
}
.topbar-search-cart {
  border-color: <?php echo esc_attr($topbar_text_color); ?>;
}
<?php } if($topbar_link_color) { ?>
.no-class {}
.saspot-topbar a, 
.saspot-topbar ul li a, 
.saspot-topbar p a {
  color: <?php echo esc_attr($topbar_link_color); ?>;
}
<?php } if($topbar_link_hover_color) { ?>
.no-class {}
.saspot-topbar a:hover, 
.saspot-topbar ul li a:hover, 
.saspot-topbar p a:hover,
.saspot-topbar a:focus, 
.saspot-topbar ul li a:focus, 
.saspot-topbar p a:focus {
  color: <?php echo esc_attr($topbar_link_hover_color); ?>;
}
<?php } if($topbar_label_color) { ?>
.no-class {}
.free-trial .saspot-label {
  color: <?php echo esc_attr($topbar_label_color); ?>;
}
<?php } if($topbar_label_bg_color) { ?>
.no-class {}
.free-trial .saspot-label {
  background-color: <?php echo esc_attr($topbar_label_bg_color); ?>;
}
<?php }

// Header colors - Customizer
$header_bg_color  = cs_get_customize_option( 'header_bg_color' );
$header_link_color  = cs_get_customize_option( 'header_link_color' );
$header_link_hover_color  = cs_get_customize_option( 'header_link_hover_color' );
if($header_bg_color) { ?>
.no-class {}
.saspot-header {
  background-color: <?php echo esc_attr($header_bg_color); ?>;
}
<?php } if($header_link_color) { ?>
.no-class {}
.saspot-navigation > ul > li > a {
  color: <?php echo esc_attr($header_link_color); ?>;
}
<?php } if($header_link_hover_color) { ?>
.no-class{}
.saspot-navigation > ul > li:hover > a,
.mean-container .mean-nav > ul > li.current-menu-parent > a,
.saspot-navigation > ul li.current-menu-parent > a,
.saspot-navigation > ul li.active > a,
.saspot-navigation > ul li.current-menu-ancestor > a,
.mean-container .mean-nav > ul li.current-menu-ancestor > a,
.saspot-navigation > ul > li > a:hover,
.saspot-navigation > ul > li > a:focus {
  color: <?php echo esc_attr($header_link_hover_color); ?>;
}
.saspot-navigation > ul > li.current-menu-parent > a .menu-text:before, 
.saspot-navigation > ul > .current-menu-ancestor > a .menu-text:before, 
.saspot-navigation > ul > li.active > a .menu-text:before,
.saspot-navigation > ul > li.active .menu-text:before {
  background: <?php echo esc_attr($header_link_hover_color); ?>;
}
<?php } 
/* Transparent Sticky Header - Customizer */
$sticky_header_bg_color  = cs_get_customize_option( 'sticky_header_bg_color' );
$sticky_header_link_color  = cs_get_customize_option( 'sticky_header_link_color' );
$sticky_header_link_hover_color  = cs_get_customize_option( 'sticky_header_link_hover_color' );

if ($sticky_header_bg_color) { ?>
.no-class {}
.saspot-header.saspot-sticky {
  background-color: <?php echo esc_attr($sticky_header_bg_color); ?>;
}
<?php } if($sticky_header_link_color) { ?>
.no-class {}
.saspot-header.saspot-sticky .saspot-navigation > ul > li > a {
  color: <?php echo esc_attr($sticky_header_link_color); ?>;
}
<?php } if($sticky_header_link_hover_color) { ?>
.no-class {}
.saspot-header.saspot-sticky .saspot-navigation > ul > li > a:hover,
.saspot-header.saspot-sticky .saspot-navigation > ul > li > a:focus {
  color: <?php echo esc_attr($sticky_header_link_hover_color); ?>;
}
.saspot-header.saspot-sticky .saspot-navigation > ul > li.active .menu-text:before {
  background: <?php echo esc_attr($sticky_header_link_hover_color); ?>;
}
<?php }
// Sub-menu Colors
$submenu_bg_color  = cs_get_customize_option( 'submenu_bg_color' );
$submenu_bg_hover_color  = cs_get_customize_option( 'submenu_bg_hover_color' );
$submenu_border_color  = cs_get_customize_option( 'submenu_border_color' );
$submenu_link_color  = cs_get_customize_option( 'submenu_link_color' );
$submenu_link_hover_color  = cs_get_customize_option( 'submenu_link_hover_color' );
if($submenu_bg_color) { ?>
.no-class {}
.dropdown-nav {
  background-color: <?php echo esc_attr($submenu_bg_color); ?>;
}
<?php } if($submenu_link_color) { ?>
.no-class {}
.saspot-header .dropdown-nav > li > a {
  color: <?php echo esc_attr($submenu_link_color); ?>;
}
<?php } if($submenu_border_color) { ?>
.no-class {}
.dropdown-nav li, .dropdown-nav {
  border-color: <?php echo esc_attr($submenu_border_color); ?>;
}
<?php } if($submenu_bg_hover_color) { ?>
.no-class {}
.saspot-header .dropdown-nav > li > a {
  background-color: <?php echo esc_attr($submenu_bg_hover_color); ?>;
}
<?php } if($submenu_link_hover_color) { ?>
.no-class {}
ul:not(.elementor-nav-menu) .dropdown-nav li.all-resources a:hover,
.saspot-navigation > ul .dropdown-nav li.active > a,
.dropdown-nav li.active a,
.dropdown-nav li.active a .menu-subtitle,
.saspot-header .dropdown-nav > li > a:hover,
.saspot-header .dropdown-nav > li > a:focus,
.saspot-header .dropdown-nav li a:hover .menu-subtitle {
  color: <?php echo esc_attr($submenu_link_hover_color); ?>;
}
<?php }

/* Header Button Colors */
$button_bg_color = cs_get_customize_option('button_bg_color');
$button_text_color = cs_get_customize_option('button_text_color');
$button_border_color = cs_get_customize_option('button_border_color');
$button_bg_hover_color = cs_get_customize_option('button_bg_hover_color');
$button_text_hover_color = cs_get_customize_option('button_text_hover_color');
$button_border_hover_color = cs_get_customize_option('button_border_hover_color');
if($button_bg_color) { ?>
.no-class {}
.header-btn .saspot-btn {}
.header-btn .saspot-btn {
  background-color: <?php echo esc_attr($button_bg_color); ?>;
}
<?php } if($button_text_color) { ?>
.no-class {}
.header-btn .saspot-btn {
  color: <?php echo esc_attr($button_text_color); ?>;
}
<?php } if($button_border_color) { ?>
.no-class {}
.header-btn .saspot-btn {
  border-color: <?php echo esc_attr($button_border_color); ?>;
}
<?php } if($button_bg_hover_color) { ?>
.no-class {}
.header-btn .saspot-btn:hover {
  background-color: <?php echo esc_attr($button_bg_hover_color); ?>;
}
<?php } if($button_text_hover_color) { ?>
.no-class {}
.header-btn .saspot-btn:hover {
  color: <?php echo esc_attr($button_text_hover_color); ?>;
}
<?php } if($button_border_hover_color) { ?>
.no-class {}
.header-btn .saspot-btn:hover {
  border-color: <?php echo esc_attr($button_border_hover_color); ?>;
}
<?php } 
/* Sidebar Menu - Customizer */
$sidebar_bg_color  = cs_get_customize_option( 'sidebar_bg_color' );
$sidebar_link_color  = cs_get_customize_option( 'sidebar_link_color' );
$sidebar_link_hover_color  = cs_get_customize_option( 'sidebar_link_hover_color' );
$sidebar_menu_border_color  = cs_get_customize_option( 'sidebar_menu_border_color' );
$sidebar_submenu_link_color  = cs_get_customize_option( 'sidebar_submenu_link_color' );
$sidebar_submenu_link_hover_color  = cs_get_customize_option( 'sidebar_submenu_link_hover_color' );
$sidebar_submenu_bg_color = cs_get_customize_option('sidebar_submenu_bg_color');
$sidebar_submenu_border_color = cs_get_customize_option('sidebar_submenu_border_color');
if ($sidebar_bg_color) { ?>
.no-class {}
.saspot-fullscreen-navigation {
  background-color: <?php echo esc_attr($sidebar_bg_color); ?>;
}
<?php }if($sidebar_link_color) { ?>
.no-class {}
.saspot-fullscreen-navigation .saspot-navigation > ul > li > a  {
  color: <?php echo esc_attr($sidebar_link_color); ?>;
}
<?php } if($sidebar_link_hover_color) { ?>
.no-class {}
.saspot-fullscreen-navigation .saspot-navigation > ul > li > a:hover,
.saspot-fullscreen-navigation .saspot-navigation > ul > li > a:focus {
  color: <?php echo esc_attr($sidebar_link_hover_color); ?>;
}
<?php } if($sidebar_menu_border_color) { ?>
.no-class {}
.navigation-wrap .saspot-navigation > ul > li,
.saspot-fullscreen-navigation .saspot-navigation > ul > li > a,
.saspot-fullscreen-navigation .dropdown-nav > li > a {
  border-color: <?php echo esc_attr($sidebar_menu_border_color); ?>;
}
<?php } if($sidebar_submenu_link_color) { ?>
.no-class {}
..navigation-wrap .dropdown-nav > li > a,
.saspot-fullscreen-navigation .saspot-navigation ul li li a {
  color: <?php echo esc_attr($sidebar_submenu_link_color); ?>;
}
<?php } if($sidebar_submenu_link_hover_color) { ?>
.no-class {}
.navigation-wrap .dropdown-nav > li > a:hover,
.navigation-wrap .dropdown-nav > li.active > a,
.saspot-fullscreen-navigation .dropdown-nav > li.active > a,
.saspot-fullscreen-navigation .saspot-navigation ul li li a:hover,
.saspot-fullscreen-navigation .saspot-navigation ul li li a:focus {
  color: <?php echo esc_attr($sidebar_submenu_link_hover_color); ?>;
}
<?php } if($sidebar_submenu_bg_color){ ?>
.no-class {}
.saspot-fullscreen-navigation ul.dropdown-nav {
  background: <?php echo esc_attr($sidebar_submenu_bg_color); ?>;
}
<?php } if($sidebar_submenu_border_color){ ?>
.no-class {}
.navigation-wrap .dropdown-nav li {
  border-color: <?php echo esc_attr($sidebar_submenu_border_color); ?>;
}
<?php }

/* Mobile Menu - Customizer */
$mobile_menu_toggle_color = cs_get_customize_option('mobile_menu_toggle_color');
$mobile_menu_bg_color  = cs_get_customize_option( 'mobile_menu_bg_color' );
$mobile_menu_bg_hover_color  = cs_get_customize_option( 'mobile_menu_bg_hover_color' );
$mobile_menu_link_color  = cs_get_customize_option( 'mobile_menu_link_color' );
$mobile_menu_link_hover_color  = cs_get_customize_option( 'mobile_menu_link_hover_color' );
$mobile_menu_border_color  = cs_get_customize_option( 'mobile_menu_border_color' );
$mobile_menu_expand_color  = cs_get_customize_option( 'mobile_menu_expand_color' );
$mobile_menu_expand_hover_color  = cs_get_customize_option( 'mobile_menu_expand_hover_color' );
$mobile_menu_expand_bg_color  = cs_get_customize_option( 'mobile_menu_expand_bg_color' );
$mobile_menu_expand_bg_hover_color  = cs_get_customize_option( 'mobile_menu_expand_bg_hover_color' );
if($mobile_menu_toggle_color){ ?>
.no-class {}
.mean-container a.meanmenu-reveal span,
.mean-container a.meanmenu-reveal span:before,
.mean-container a.meanmenu-reveal span:after,
.mean-container a.meanmenu-reveal.meanclose span:before {
  background: <?php echo esc_attr($mobile_menu_toggle_color); ?>;
}
<?php }
if ($mobile_menu_bg_color) { ?>
.no-class {}
.mean-container .mean-nav {
  background-color: <?php echo esc_attr($mobile_menu_bg_color); ?>;
}
<?php } if($mobile_menu_bg_hover_color) { ?>
.no-class {}
.saspot-header .mean-container .dropdown-nav > li:hover > a,
.saspot-header .mean-container .dropdown-nav > li:focus > a,
.mean-container .mean-nav ul li:hover > a,
.mean-container .mean-nav ul li:focus > a {
  background-color: <?php echo esc_attr($mobile_menu_bg_hover_color); ?>;
}
<?php } if($mobile_menu_link_color) { ?>
.no-class {}
.mean-container .mean-nav ul li a {
  color: <?php echo esc_attr($mobile_menu_link_color); ?>;
}
<?php } if($mobile_menu_link_hover_color) { ?>
.no-class {}
.mean-container .mean-nav ul li a:hover,
.mean-container .mean-nav ul li a:focus,
.saspot-header .mean-container .dropdown-nav > li.active > a,
.mean-container ul li.current-menu-ancestor > a {
  color: <?php echo esc_attr($mobile_menu_link_hover_color); ?>;
}
<?php } if($mobile_menu_border_color) { ?>
.no-class {}
.mean-container .mean-nav ul li li a, .mean-container .mean-nav ul li a {
  border-color: <?php echo esc_attr($mobile_menu_border_color); ?>;
}
<?php } if($mobile_menu_expand_color) { ?>
.no-class {}
.mean-container .mean-nav ul li a.mean-expand {
  color: <?php echo esc_attr($mobile_menu_expand_color); ?>;
}
<?php } if($mobile_menu_expand_hover_color) { ?>
.no-class {}
.mean-container .mean-nav ul li a.mean-expand:hover,
.mean-container .mean-nav ul li a.mean-expand:focus,
.mean-container .mean-nav ul li:hover > a.mean-expand,
.mean-container .mean-nav ul li:focus > a.mean-expand,
.saspot-header .mean-container .dropdown-nav > li:hover > a.mean-expand,
.saspot-header .mean-container .dropdown-nav > li:focus > a.mean-expand {
  color: <?php echo esc_attr($mobile_menu_expand_hover_color); ?>;
}
<?php } if($mobile_menu_expand_bg_color) { ?>
.no-class {}
.mean-container .mean-nav ul li a.mean-expand {
  background-color: <?php echo esc_attr($mobile_menu_expand_bg_color); ?>;
}
<?php } if($mobile_menu_expand_bg_hover_color) { ?>
.no-class {}
.mean-container .mean-nav ul li a.mean-expand:hover,
.mean-container .mean-nav ul li a.mean-expand:focus,
.mean-container .mean-nav ul li:hover > a.mean-expand,
.mean-container .mean-nav ul li:focus > a.mean-expand,
.saspot-header .mean-container .dropdown-nav > li:hover > a.mean-expand,
.saspot-header .mean-container .dropdown-nav > li:focus > a.mean-expand {
  background-color: <?php echo esc_attr($mobile_menu_expand_bg_hover_color); ?>;
}
<?php }
/* Title Area - Theme Options - Background */
$titlebar_bg = cs_get_option('titlebar_bg');
$title_heading_color  = cs_get_customize_option( 'titlebar_title_color' );
$title_sub_heading_color  = cs_get_customize_option( 'titlebar_sub_title_color' );
if ($titlebar_bg) {

  $title_area = ( ! empty( $titlebar_bg['image'] ) ) ? 'background-image: url('. $titlebar_bg['image'] .');' : '';
  $title_area .= ( ! empty( $titlebar_bg['repeat'] ) ) ? 'background-repeat: '. $titlebar_bg['repeat'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['position'] ) ) ? 'background-position: '. $titlebar_bg['position'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['attachment'] ) ) ? 'background-attachment: '. $titlebar_bg['attachment'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['size'] ) ) ? 'background-size: '. $titlebar_bg['size'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['color'] ) ) ? 'background-color: '. $titlebar_bg['color'] .';' : ''; ?>
  .saspot-page-title {
    <?php echo esc_attr($title_area); ?>
  }
<?php } if($title_heading_color) { ?>
.no-class {}
.saspot-page-title .page-title {
  color: <?php echo esc_attr($title_heading_color); ?>;
}
<?php } if ($title_sub_heading_color) { ?>
.no-class {}
.saspot-page-title p {
  color: <?php echo esc_attr($title_sub_heading_color); ?>;
}
<?php }
/* Footer */
$footer_bg_color  = cs_get_customize_option( 'footer_bg_color' );
$footer_heading_color  = cs_get_customize_option( 'footer_heading_color' );
$footer_text_color  = cs_get_customize_option( 'footer_text_color' );
$footer_link_color  = cs_get_customize_option( 'footer_link_color' );
$footer_link_hover_color  = cs_get_customize_option( 'footer_link_hover_color' );
if ($footer_bg_color) { ?>
.no-class {}
.saspot-footer {
  background: <?php echo esc_attr($footer_bg_color); ?>;
}
<?php } if ($footer_heading_color) { ?>
.no-class {}
.footer-widget h4, .saspot-footer h1, .saspot-footer h2, .saspot-footer h3, .saspot-footer h4,
.footer-widget-title {
  color: <?php echo esc_attr($footer_heading_color); ?>;
}
<?php } if ($footer_text_color) { ?>
.no-class {}
.saspot-footer .footer-widget-area,
.saspot-footer .saspot-widget p,
.saspot-footer .saspot-widget p span,
.saspot-footer .saspot-widget span,
.saspot-footer .saspot-widget ul li,
.saspot-footer .footer-widget-area,
.saspot-footer .saspot-widget p,
.saspot-footer .saspot-recent-blog .widget-bdate,
.saspot-footer-wrap,
.saspot-footer .woocommerce ul.cart_list .woocommerce-Price-amount,
.saspot-footer .woocommerce ul.product_list_widget .woocommerce-Price-amount,
.saspot-footer table td,
.saspot-footer caption, .saspot-footer .saspot-widget.woocommerce.widget_products span.woocommerce-Price-amount.amount, .saspot-footer .footer-item p,
.saspot-footer .saspot-widget input[type="email"] {
  color: <?php echo esc_attr($footer_text_color); ?>;
}
<?php } if ($footer_link_color) { ?>
.no-class {}
.saspot-footer a,
.saspot-footer .footer-widget .saspot-widget ul li a,
.saspot-footer .saspot-widget a span,
.saspot-footer .widget_list_style ul a,
.saspot-footer .widget_categories ul a,
.saspot-footer .widget_archive ul a,
.saspot-footer .widget_archive ul li,
.saspot-footer .widget_recent_comments ul a,
.saspot-footer .widget_recent_entries ul a,
.saspot-footer .widget_meta ul a,
.saspot-footer .widget_pages ul a,
.saspot-footer .widget_rss ul a,
.saspot-footer .widget_nav_menu ul a,
.saspot-footer .saspot-recent-blog .widget-btitle,
.saspot-footer .saspot-widget.woocommerce.widget_shopping_cart a,
.saspot-footer table td a,
.saspot-footer ul li a, .saspot-footer .footer-item a,
.saspot-footer .footer-widget .saspot-widget ul li a {
  color: <?php echo esc_attr($footer_link_color); ?>;
}
<?php } if ($footer_link_hover_color) { ?>
.no-class {}
.saspot-footer a:hover,
.saspot-footer .footer-widget .saspot-widget ul li a:hover,
.saspot-footer .saspot-widget a:hover span,
.saspot-footer .widget_list_style ul a:hover,
.saspot-footer .widget_categories ul a:hover,
.saspot-footer .widget_archive ul a:hover,
.saspot-footer .widget_archive ul li,
.saspot-footer .widget_recent_comments ul a:hover,
.saspot-footer .widget_recent_entries ul a:hover,
.saspot-footer .widget_meta ul a:hover,
.saspot-footer .widget_pages ul a:hover,
.saspot-footer .widget_rss ul a:hover,
.saspot-footer .widget_nav_menu ul a:hover,
.saspot-footer .frxo-recent-blog .widget-btitle:hover,
.saspot-footer .frxo-widget.woocommerce.widget_shopping_cart a:hover,
.saspot-footer table td a:hover,
.frxo-footer ul li a:hover,
.saspot-footer .woocommerce ul.product_list_widget li a:hover,
.frxo-footer .footer-item a:hover,
.saspot-footer .footer-widget .saspot-widget ul li a:hover {
  color: <?php echo esc_attr($footer_link_hover_color); ?>;
}
<?php } 
// Copyright Colors
$copyright_text_color  = cs_get_customize_option( 'copyright_text_color' );
$copyright_link_color  = cs_get_customize_option( 'copyright_link_color' );
$copyright_link_hover_color  = cs_get_customize_option( 'copyright_link_hover_color' );
$copyright_bg_color  = cs_get_customize_option( 'copyright_bg_color' );
$copyright_border_color  = cs_get_customize_option( 'copyright_border_color' );
if ($copyright_text_color) { ?>
.no-class {}
.saspot-copyright,
.saspot-footer .saspot-copyright p,
.saspot-footer .saspot-copyright li {
  color: <?php echo esc_attr($copyright_text_color); ?>;
}
<?php } if ($copyright_link_color) { ?>
.no-class {}
.saspot-copyright a,
.saspot-footer .saspot-copyright a {
  color: <?php echo esc_attr($copyright_link_color); ?>;
}
<?php } if ($copyright_link_hover_color) { ?>
.no-class {}
.saspot-copyright a:hover,
.saspot-copyright a:focus,
.saspot-footer .saspot-copyright a:hover,
.saspot-footer .saspot-copyright a:focus {
  color: <?php echo esc_attr($copyright_link_hover_color); ?>;
}
<?php } if ($copyright_bg_color) { ?>
.no-class {}
.saspot-copyright {
  background-color: <?php echo esc_attr($copyright_bg_color); ?>;
}
<?php } if ($copyright_border_color) { ?>
.no-class {}
.saspot-copyright {
  border-color: <?php echo esc_attr($copyright_border_color); ?>;
}
<?php }

// Content Colors
$body_color  = cs_get_customize_option( 'body_color' ); 
if ($body_color) { ?>
.no-class {}
body, .section-title-wrap p, .looking-item p, .tool-item p, .enterprises-info p, .step-info p, .feature-item p, .market-wrap p, .card-body p, .agency-item p, .testimonial-info p, .resource-item p, .analytics-inner p, .marketing-item p, .promote-item p, .market-example-info p, .extension-wrap p, .app-info p, .process-info p, .class-item p, .webinar-info p, .mate-info p, .table td p, .blog-info p, .saspot-sidebar p, .place-info p, .address-info p, .office-item p, .chat-support-info p, .corporate-info p, .identify-info p, .single-post p, .single-apps p, .single-webinars p, .single-job p, .single-testimonial p, .single-team p, .nice-select .list, .contact-link-wrap .contact-label, .guide-info .nav-link, .accordion-title a:hover, .accordion-title a:focus, .tabs-style-two .nav-link, .nav-tabs.tabs-style-two .nav-item.show .nav-link, .nav-tabs.tabs-style-two .nav-link.active, .nav-tabs.tabs-style-two .nav-link:focus, .nav-tabs.tabs-style-two .nav-link:hover, .saspot-callout.callout-style-six .container, .saspot-market-types:before, .accordion-title a.collapsed:before, .navigation-wrap .saspot-navigation > ul > li > a, .page-links a:hover {
  color: <?php echo esc_attr($body_color); ?>;
}
<?php } 
$body_links_color  = cs_get_customize_option( 'body_links_color' );
if ($body_links_color) { ?>
.no-class {}
.saspot-primary a,
.saspot-content-side a,
a,
.saspot-mid-wrap div a,
.saspot-mid-wrap ul li a,
.saspot-mid-wrap span a, 
.saspot-mid-wrap p a,
.widget_list_style ul a,
.widget_categories ul a,
.widget_archive ul a,
.widget_recent_comments ul a,
.widget_recent_entries ul a,
.widget_meta ul a,
.widget_pages ul a,
.widget_rss ul a,
.widget_nav_menu ul a,
.widget_layered_nav ul a,
.widget_product_categories ul a,
.flex-control-paging li a.flex-active,
.saspot-blog-share .saspot-social.rounded a,
.nav-tabs > li > a,
.nav-tabs > li.active > a,
.panel-title a,
.saspot-navigation > ul > li > a,
.saspot-transparent-header .saspot-header-right .saspot-navigation > ul > li > a,
.saspot-fullscreen-navigation .dropdown-nav > li > a,
..navigation-wrap .dropdown-nav > li > a,
.owl-drag .owl-prev:before, .owl-drag .owl-next:before, ul.bxslider li .twittMeta a {
  color: <?php echo esc_attr($body_links_color); ?>;
}
<?php }
$body_link_hover_color  = cs_get_customize_option( 'body_link_hover_color' );
if ($body_link_hover_color) { ?>
.no-class {}
.saspot-primary a:hover,
.saspot-content-side a:hover,
a:hover,
a:focus,
.saspot-link a:hover:after,
.close-btn a:hover,
.saspot-mid-wrap div a:hover,
.saspot-mid-wrap ul li a:hover,
.saspot-mid-wrap span a:hover, 
.saspot-mid-wrap p a:hover,
.saspot-mid-wrap div a:focus,
.saspot-mid-wrap ul li a:focus,
.saspot-mid-wrap span a:focus, 
.saspot-mid-wrap p a:focus,
.saspot-pagination ul li a:hover,
.saspot-pagination ul li a:focus,
a:hover, a:focus, .saspot-topbar ul li a:hover, .saspot-topbar ul li a:focus, .saspot-navigation > ul > li:hover > a, .saspot-navigation > ul > li.active > a, .saspot-navigation > ul > .current-page-ancestor > a, .dropdown-nav li a:hover .menu-subtitle, ul:not(.elementor-nav-menu) .dropdown-nav li.all-resources a:hover, .banner-subtitle a:hover, .client-subtitle, .footer-widget ul li a:hover, .footer-widget ul li a:focus, .saspot-copyright p a:hover, .step-subtitle, .section-title span, .checkbox-icon-wrap input[type="checkbox"]:checked + .checkbox-icon:before, .job-label, .saspot-btn.saspot-white-btn, .video-btn, .class-date, .indepth-subtitle, .guide-label, .guide-main-label, .arrow-wrap:before, .wpcf7-list-item-label a, .market-role-item i, .common-feature-inner .saspot-icon, .analytics-inner .icon-linea, .accordion-title a:before, .pricing-plan-title span, .nav-tabs.tabs-style-three .nav-item.show .nav-link, .nav-tabs.tabs-style-three .nav-link.active, .nav-tabs.tabs-style-three .nav-link:focus, .nav-tabs.tabs-style-three .nav-link:hover, .chat-support-item .icon-linea, .no-cost-wrap .saspot-icon .icon-linea, .saspot-sidebar .saspot-widget ul li a:hover, .bullet-list li a:hover, .masonry-filters.filters-style-two ul li a.active, .use-step-arrow:before, .navigation-wrap .dropdown-nav li a:hover, .saspot-cookies p a:hover, .dropdown-nav li .inline-link a:hover {
  color: <?php echo esc_attr($body_link_hover_color); ?>;
}

<?php }
$sidebar_content_color  = cs_get_customize_option( 'sidebar_content_color' );
if ($sidebar_content_color) { ?>
.no-class {}
.saspot-secondary p, .saspot-secondary .saspot-widget,
.saspot-secondary .widget_rss .rssSummary,
.saspot-secondary .news-time, .saspot-secondary .recentcomments,
.saspot-secondary input[type="text"], .saspot-secondary .nice-select, .saspot-secondary caption,
.saspot-secondary table td, .saspot-secondary .saspot-widget input[type="search"] {
  color: <?php echo esc_attr($sidebar_content_color); ?>;
}
<?php }
$sidebar_links_color  = cs_get_customize_option( 'sidebar_links_color' );
if ($sidebar_links_color) { ?>
.no-class {}
.saspot-secondary a,
.saspot-mid-wrap .saspot-secondary a,
.saspot-secondary .saspot-widget ul li a {
  color: <?php echo esc_attr($sidebar_links_color); ?>;
}
<?php }
$sidebar_links_hover_color  = cs_get_customize_option( 'sidebar_links_hover_color' );
if ($sidebar_links_hover_color) { ?>
.no-class {}
.saspot-secondary a:hover,
.saspot-mid-wrap .saspot-secondary a:hover,
.saspot-mid-wrap .saspot-secondary a:focus,
.saspot-secondary .saspot-widget ul li a:hover {
  color: <?php echo esc_attr($sidebar_links_hover_color); ?>;
}
<?php }
$content_heading_color  = cs_get_customize_option( 'content_heading_color' );
if ($content_heading_color) { ?>
.no-class {}
.saspot-primary h1, .saspot-primary h2, .saspot-primary h3, .saspot-primary h4, .saspot-primary h5, .saspot-primary h6,
h1, h2, h3, h4, h5, h6 {
  color: <?php echo esc_attr($content_heading_color); ?>;
}
<?php }
$sidebar_heading_color  = cs_get_customize_option( 'sidebar_heading_color' );
if ($sidebar_heading_color) { ?>
.no-class {}
.saspot-secondary h1, .saspot-secondary h2, .saspot-secondary h3, .saspot-secondary h4, .saspot-secondary h5, .saspot-secondary h6 {
  color: <?php echo esc_attr($sidebar_heading_color); ?>;
}
<?php }
// Maintenance Mode
$maintenance_mode_bg  = cs_get_option( 'maintenance_mode_bg' );
if ($maintenance_mode_bg) {
  $maintenance_css = ( ! empty( $maintenance_mode_bg['image'] ) ) ? 'background-image: url('. $maintenance_mode_bg['image'] .');' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['repeat'] ) ) ? 'background-repeat: '. $maintenance_mode_bg['repeat'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['position'] ) ) ? 'background-position: '. $maintenance_mode_bg['position'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['attachment'] ) ) ? 'background-attachment: '. $maintenance_mode_bg['attachment'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['size'] ) ) ? 'background-size: '. $maintenance_mode_bg['size'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['color'] ) ) ? 'background-color: '. $maintenance_mode_bg['color'] .';' : ''; ?>
  .vt-maintenance-mode {
    <?php echo esc_attr($maintenance_css); ?>
  }
<?php } 
// Mobile Menu Breakpoint
$mobile_breakpoint = cs_get_option('mobile_breakpoint');
$breakpoint = $mobile_breakpoint ? $mobile_breakpoint : '1199'; ?>
@media (max-width: <?php echo esc_attr($breakpoint); ?>px) {
  .no-class {}
  .saspot-header .saspot-navigation {
    display: none !important;
  }
  .saspot-header {
    padding: 20px 25px;
  }
  .saspot-navigation > ul > li {
    display: block;
    margin: 0;
    border-bottom: 1px solid #dddddd;
  }
  .saspot-navigation > ul > li.open, .saspot-navigation > ul > li.open.active {
    border-bottom-color: transparent;
  }
  .saspot-navigation > ul > li.active {
    border-color: #4776e6;
  }
  .saspot-navigation > ul > li > a {
    padding: 7px 60px 7px 0;
    line-height: 21px;
  }
  .saspot-navigation > ul > li.has-dropdown:after, .saspot-navigation > ul > li.has-dropdown .menu-text:after {
    display: none;
  }
  .saspot-navigation > ul > li.active .menu-text:before {
    display: none;
  }
  .mega-menu .dropdown-nav {
    min-width: 10px;
  }
  .mean-container .mega-menu .dropdown-nav li.active .menu-subtitle,
  .mean-container .mega-menu .dropdown-nav li.active a {
    background: none;
    border: none;
    color: #4776e6;
  }
  .mean-container .mega-menu .dropdown-nav li.active a {
    border-top: 1px solid #d5dbe9;
  }
  .dropdown-nav li.active, .dropdown-nav li.active a, .mega-menu .dropdown-nav li.all-resources {
    padding: 0;
  }
  .dropdown-nav li.all-resources {
    padding: 0;
  }
  .dropdown-nav li.menu-spacer a {
    padding: 25px 20px;
  }
  .dropdown-nav li.inline-text {
    padding: 0;
  }
  .dropdown-nav li .inline-link {
    padding-top: 10px;
  }
  .dropdown-nav li a {
    padding: 15px 20px;
  }
  .dropdown-nav.normal-style {
    background: transparent;
    border: none;
  }
  .dropdown-nav.normal-style li a {
    padding: 11px 10px;
  }
}
<?php
$saaspot_vt_get_typography  = saaspot_vt_get_typography();
echo $saaspot_vt_get_typography;