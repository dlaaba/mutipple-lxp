<?php
/*
 * The template for displaying 404 pages (not found).
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

// Theme Options
$saaspot_error_heading = cs_get_option('error_heading');
$saaspot_error_page_content = cs_get_option('error_page_content');
$saaspot_error_page_bg = cs_get_option('error_page_bg');
$saaspot_error_btn_text = cs_get_option('error_btn_text');
$saaspot_error_heading = ( $saaspot_error_heading ) ? $saaspot_error_heading : esc_html__( 'Sorry!!! Page Not Found!', 'saaspot' );
$saaspot_error_page_content = ( $saaspot_error_page_content ) ? $saaspot_error_page_content : esc_html__( 'We can\'t to find the page you\'re looking for, please go back to the homepage', 'saaspot' );
$saaspot_error_page_bg = ( $saaspot_error_page_bg ) ? wp_get_attachment_url($saaspot_error_page_bg) : SAASPOT_IMAGES . '/404.png';
$saaspot_error_btn_text = ( $saaspot_error_btn_text ) ? $saaspot_error_btn_text : esc_html__( 'Go back to home', 'saaspot' );

get_header();
?>
<div class="saspot-404-error saspot-overlay" style="background: url(<?php echo esc_url($saaspot_error_page_bg); ?>);">
  <div class="saspot-table-wrap">
    <div class="saspot-align-wrap top">
      <div class="container">
        <div class="saspot-container">
          <h1 class="error-title"><?php echo esc_html($saaspot_error_heading); ?></h1>
          <p><?php echo esc_html($saaspot_error_page_content); ?></p>
          <div class="saspot-btns-group"><a href="<?php echo esc_url(home_url( '/' )); ?>" class="saspot-btn saspot-light-blue-btn"><?php echo esc_html($saaspot_error_btn_text); ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
get_footer();
