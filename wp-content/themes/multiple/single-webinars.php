<?php
/*
 * The template for displaying all single webinars.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();

// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else { $saaspot_content_padding = ''; }
// Padding - Theme Options
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
}

$saaspot_webinars_need_related = cs_get_option('webinars_need_related');
$saaspot_webinars_related_title = cs_get_option('webinars_related_title');
$saaspot_webinars_related_content = cs_get_option('webinars_related_content');
$saaspot_webinars_related_title = $saaspot_webinars_related_title ? $saaspot_webinars_related_title : esc_html__('Related Webinars', 'saaspot');
$saaspot_webinars_related_content = $saaspot_webinars_related_content ? $saaspot_webinars_related_content : '';
?>
<div class="saspot-mid-wrap webinars-single-page <?php echo esc_attr($saaspot_content_padding); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
	<div class="container ">
		<div class="webinars-details">
			<div class="row">
	      <div class="col-lg-12">
					<?php
					if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile;
					endif;
					?>
				</div>
			</div>			
		</div>
	</div>
	<?php
	if ($saaspot_webinars_need_related) {
	//get the taxonomy terms of custom post type
	$customTaxonomyTerms = wp_get_object_terms( $post->ID, 'webinars_category', array('fields' => 'ids') );
	//query arguments
	$args = array(
	  'post_type' => 'webinars',
	  'post_status' => 'publish',
	  'posts_per_page' => 2,
	  'orderby' => 'rand',
	  'tax_query' => array(
	    array(
	      'taxonomy' => 'webinars_category',
	      'field' => 'id',
	      'terms' => $customTaxonomyTerms
	    )
	  ),
	  'post__not_in' => array ($post->ID),
	);
	//the query
	$relatedPosts = new WP_Query( $args );

	//loop through query
	if($relatedPosts->have_posts()){ ?>
		<div class="saspot-webinars examples-style-two saspot-overlay">
		  <div class="container">
		    <div class="section-title-wrap">
		      <h2 class="section-title"><?php echo esc_html($saaspot_webinars_related_title); ?></h2>
		      <p><?php echo esc_html($saaspot_webinars_related_content); ?></p>
		    </div>
		    <div class="webinars-items-wrap">
		      <div class="row">
		        <?php while ($relatedPosts->have_posts()) {
						$relatedPosts->the_post();

						$webinars_meta  = get_post_meta( get_the_ID(), 'webinars_metabox', true );
						if ($webinars_meta) {
						  $webinars_video = $webinars_meta['webinars_video'];
						} else {
						  $webinars_video = '';
						}

						$video_text = cs_get_option('video_text');
						$video_text = $video_text ? $video_text : esc_html__('WATCH NOW', 'saaspot');
						$saaspot_webinars_aqr = cs_get_option('webinars_aqr');

						// Featured Image
						$saaspot_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
						$saaspot_large_image = $saaspot_large_image[0];
						if ($saaspot_webinars_aqr) {
						  $saaspot_featured_img = $saaspot_large_image;
						} else {
					    if(class_exists('Aq_Resize')) {
					      $saaspot_webinars_img = aq_resize( $saaspot_large_image, '560', '360', true );
					    } else {$saaspot_webinars_img = $saaspot_large_image;}
					    $saaspot_featured_img = ( $saaspot_webinars_img ) ? $saaspot_webinars_img : SAASPOT_IMAGES . '/holders/560x360.png';
						}
			      ?>
		        <div class="col-md-6">
						  <div class="market-example-item">
						    <div class="video-wrap-inner">
						      <div class="saspot-image">
						        <?php if ($webinars_video) { ?>
						        <a href="#0" id="myUrl" data-toggle="modal" data-src="<?php echo esc_url($webinars_video); ?>" data-target="#SaaSpotVideoModal" class="saspot-video-btn">
						          <span class="video-btn"><i class="fa fa-play" aria-hidden="true"></i></span>
						        </a>
						        <?php } ?>
						        <img src="<?php echo esc_url($saaspot_featured_img); ?>" alt="<?php echo esc_attr(get_the_title()); ?>">
						      </div>
						    </div>
						    <div class="market-example-info">
						      <div class="saspot-label">
						        <?php
						          $category_list = wp_get_post_terms($post->ID, 'webinars_category');
						          foreach ($category_list as $term) {
						            echo '<span>'. esc_html($term->name) .'</span> ';
						          }
						        ?>
						      </div>
						      <h4 class="market-example-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(the_title()); ?></a></h4>
						      <p><?php the_excerpt(); ?></p>
						    </div>
						  </div>
						</div>
		        <?php } ?>
		      </div>
		    </div>
		  </div>
		</div>
		
	<?php } else{ } }
	//restore original post data
	wp_reset_postdata(); ?>
</div>
<?php
get_footer();
