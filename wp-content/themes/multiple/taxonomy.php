<?php
/*
 * The template for apps category pages.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();
$noneed_apps_post = cs_get_option('noneed_apps_post');
$noneed_webinars_post = cs_get_option('noneed_webinars_post');
$noneed_job_post = cs_get_option('noneed_job_post');

if (function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('apps') && !$noneed_apps_post)) {

  $apps_limit = cs_get_option('apps_limit');
  $saaspot_apps_orderby = cs_get_option('apps_orderby');
  $saaspot_apps_order = cs_get_option('apps_order');
  $saaspot_apps_pagination = cs_get_option('apps_pagination');
  $saaspot_categories_text = cs_get_option('categories_text');
  $apps_limit = $apps_limit ? $apps_limit : '9';

  $all_text = cs_get_option('all_text');
  $all_text_actual = $all_text ? $all_text : esc_html__('All', 'saaspot');
  $saaspot_categories_text = $saaspot_categories_text ? $saaspot_categories_text : esc_html__('Categories', 'saaspot');

  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }

  $apps_category = get_queried_object();

  $args = array(
    // other query params here,
    'paged' => $my_page,
    'post_type' => 'apps',
    'posts_per_page' => (int)$apps_limit,
    'apps_category' => $apps_category->name,
    'orderby' => $saaspot_apps_orderby,
    'order' => $saaspot_apps_order
  );

  $saaspot_port = new WP_Query( $args ); ?>
  <div class="saspot-apps">
    <div class="container">
      <div class="row">

  	    <!-- Start -->
  	    <div class="col-xl-12">
          <div class="apps-wrap">
            <div class="saspot-masonry" id="section-apps">
      	    	<?php
      	      if ($saaspot_port->have_posts()) : while ($saaspot_port->have_posts()) : $saaspot_port->the_post();
      	        get_template_part( 'layouts/post/content', 'apps' );
      	        endwhile;
      	      else :
      	        get_template_part( 'layouts/post/content', 'none' );
      	      endif; ?>
            </div>
          </div>
  	    </div>
  			<!-- End -->
        <?php if($saaspot_port->max_num_pages > 1) { ?>
        <div class="pagination-wrap">
  	    	<?php if ($saaspot_apps_pagination) { saaspot_paging_nav($saaspot_port->max_num_pages,"",$paged); } ?>
  	  	</div>
        <?php } wp_reset_postdata(); ?>

    	</div>
    </div>
  </div>

<?php } elseif (function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('webinars') && !$noneed_webinars_post)) {

  $webinars_style = cs_get_option('webinars_style');
  $webinars_limit = cs_get_option('webinars_limit');
  $saaspot_webinars_orderby = cs_get_option('webinars_orderby');
  $saaspot_webinars_order = cs_get_option('webinars_order');
  $saaspot_webinars_filter = cs_get_option('webinars_filter');
  $saaspot_webinars_pagination = cs_get_option('webinars_pagination');
  $webinars_limit = $webinars_limit ? $webinars_limit : '9';

  $webi_all_text = cs_get_option('webi_all_text');
  $webi_all_text_actual = $webi_all_text ? $webi_all_text : esc_html__('ALL WEBINARS', 'saaspot');

  if ($webinars_style === 'two') {
    $style_class = ' examples-style-two';
    $row_class = 'row';
  } else {
    $style_class = '';
    $row_class = 'webinars-wrap';
  }

  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }
  $webinars_category = get_queried_object();

  $args = array(
    // other query params here,
    'paged' => $my_page,
    'post_type' => 'webinars',
    'posts_per_page' => (int)$webinars_limit,
    'webinars_category' => $webinars_category->name,
    'orderby' => $saaspot_webinars_orderby,
    'order' => $saaspot_webinars_order
  );

  $saaspot_port = new WP_Query( $args ); ?>
  <div class="saspot-webinars<?php echo esc_attr($style_class); ?>">
    <div class="container">
      <div class="<?php echo esc_attr($row_class); ?>">

        <?php if ($saaspot_webinars_filter && $webinars_style !== 'two') { ?>
        <div class="masonry-filters filters-style-two">
          <ul>
            <li><a href="javascript:void(0);" data-filter="*" class="active"><?php echo esc_html($webi_all_text_actual); ?></a></li>
            <?php
              if ($saaspot_webinars_show_category) {
                $cat_name = explode(',', $saaspot_webinars_show_category);
                $terms = $cat_name;
                $count = count($terms);
                if ($count > 0) {
                  foreach ($terms as $term) {
                    echo '<li class="webi-'. preg_replace('/\s+/', "", strtolower($term)) .'"><a href="javascript:void(0);" data-filter=".webi-'. preg_replace('/\s+/', "", strtolower($term)) .'" title="' . str_replace('-', " ", strtolower($term)) . '">' . str_replace('-', " ", strtolower($term)) . '</a></li>';
                   }
                }
              } else {
                if ( function_exists( 'saaspot_webinars_category_list' ) ) {
                  echo saaspot_webinars_category_list();
                }
              }
            ?>
          </ul>
        </div>
        <?php } ?>
        <!-- Start -->
        <?php if ($webinars_style !== 'two') { ?>
        <div class="webinars-inner-wrap">
          <div class="saspot-masonry" data-items="1">
            <?php }
            if ($saaspot_port->have_posts()) : while ($saaspot_port->have_posts()) : $saaspot_port->the_post();
              get_template_part( 'layouts/post/content', 'webinars' );
              endwhile;
            else :
              get_template_part( 'layouts/post/content', 'none' );
            endif;
        if ($webinars_style !== 'two') { ?>
          </div>
        </div>
        <?php } ?>
        <!-- End -->
        <?php if($saaspot_port->max_num_pages > 1) { ?>
        <div class="pagination-wrap">
          <?php if ($saaspot_webinars_pagination) { saaspot_paging_nav($saaspot_port->max_num_pages,"",$paged); } ?>
        </div>
        <?php } wp_reset_postdata(); ?>

      </div>
    </div>
  </div>

<?php } elseif (function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('job') && !$noneed_job_post)) {

  $job_limit = cs_get_option('job_limit');
  $job_orderby = cs_get_option('job_orderby');
  $job_order = cs_get_option('job_order');
  $job_pagination = cs_get_option('job_pagination');
  $applay_job = cs_get_option('applay_job');

  $job_limit = $job_limit ? $job_limit : '8';
  // Query Starts Here
  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }
  $job_category = get_queried_object();

  $args = array(
    'paged' => $my_page,
    'post_type' => 'job',
    'posts_per_page' => (int)$job_limit,
    'job_role' => $job_category->name,
    'orderby' => $job_orderby,
    'order' => $job_order,
  );

  $saaspot_job_qury = new WP_Query( $args );
  if ($saaspot_job_qury->have_posts()) :
  ?>
  <div class="saspot-jobs">
    <div class="container">
      <div class="jobs-wrap">
      <?php
      while ($saaspot_job_qury->have_posts()) : $saaspot_job_qury->the_post();
      // Featured Image
  		$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
  		$large_image = $large_image[0];
  		$abt_title = get_the_title();

      // Link
      $applay_job = $applay_job ? $applay_job : esc_html__('APPLY','saaspot');
      ?>
      <div class="job-item">
        <div class="row align-items-center">
          <div class="col-md-9">
            <p>
              <?php
                $category_list = wp_get_post_terms($post->ID, 'job_role');
                foreach ($category_list as $term) {
                  $job_term_link = get_term_link( $term );
                  echo '<span><a href="'. esc_url($job_term_link) .'">'. esc_html($term->name) .'</a></span> ';
                }
              ?>
            </p>
            <h3 class="job-title"><?php echo esc_html($abt_title); ?></h3>
          </div>
          <div class="col-md-3 textright">
            <div class="job-btn"><a href="<?php echo esc_url( get_permalink() ); ?>" class="saspot-btn saspot-light-blue-bdr-btn"><?php echo esc_html($applay_job); ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
          </div>
        </div>
      </div>

      <?php endwhile; ?>
      </div>
      <?php if ($job_pagination) { ?>
      <div class="pagination-wrap">
        <?php saaspot_paging_nav($saaspot_job_qury->max_num_pages,"",$paged); wp_reset_postdata(); ?>
      </div>
      <?php } ?>
    </div>
  </div> <!-- Job End -->

  <?php
    endif;
}
get_footer();
