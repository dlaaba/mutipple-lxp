<?php
/*
 * The template for displaying archive pages.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();
$noneed_testimonial_post = cs_get_option('noneed_testimonial_post');
$noneed_apps_post = cs_get_option('noneed_apps_post');
$noneed_webinars_post = cs_get_option('noneed_webinars_post');
$noneed_team_post = cs_get_option('noneed_team_post');
$noneed_job_post = cs_get_option('noneed_job_post');

if(function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('testimonial') && !$noneed_testimonial_post)){

	$testimonial_style = cs_get_option('testimonial_style');
	$testimonial_limit = cs_get_option('testimonial_limit');
	$testimonial_orderby = cs_get_option('testimonial_orderby');
	$testimonial_order = cs_get_option('testimonial_order');
  $testimonial_limit = $testimonial_limit ? $testimonial_limit : '-1';

  // Query Starts Here
  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }

  $args = array(
    'paged' => $my_page,
    'post_type' => 'testimonial',
    'posts_per_page' => (int)$testimonial_limit,
    'orderby' => $testimonial_orderby,
    'order' => $testimonial_order,
  );

  // Testimonial Style
  if ($testimonial_style === 'testimonial_two') {
    $testimonial_style_class = ' testimonials-style-two saspot-overlay';
  } else {
    $testimonial_style_class = '';
  }
  // RTL
  if ( is_rtl() ) {
    $switch_rtl = ' data-rtl="true"';
  } else {
    $switch_rtl = ' data-rtl="false"';
  }

  $saaspot_testi = new WP_Query( $args );
  if ($saaspot_testi->have_posts()) :
  ?>
  <div class="saspot-testimonials<?php echo esc_attr($testimonial_style_class); ?>">
    <div class="container">
    <?php  if ($testimonial_style === 'testimonial_two') { ?>
    <div class="testimonials-wrap">
    <div class="saspot-icon"><img src="<?php echo esc_url(SAASPOT_IMAGES.'/icons/icon33@2x.png'); ?>" width="40" alt="Quote"></div>
      <div class="owl-carousel" data-items="1" data-margin="0" data-loop="true" data-nav="true" data-dots="true" data-autoplay="true">
    <?php } else { ?>
      <div class="owl-carousel" data-items="2" data-items-tablet="1" data-margin="72" data-loop="true" data-nav="true" data-dots="true" data-autoplay="true">
    <?php }
        while ($saaspot_testi->have_posts()) : $saaspot_testi->the_post();

        // Get Meta Box Options - saaspot_framework_active()
        $testimonial_options = get_post_meta( get_the_ID(), 'testimonial_options', true );
        $testi_job = $testimonial_options['testi_position'];

        // Featured Image
        $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
        $large_image = $large_image[0];
        $saaspot_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);

        if ($testimonial_style === 'testimonial_two') { // Style Two
        ?>
          <div class="item">
            <p><?php the_excerpt(); ?></p>
            <h6 class="author-name"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a> <?php if ($testi_job) { ?> / <?php echo esc_html($testi_job); ?><?php } ?></h6>
          </div>
        <?php } else { ?>
          <div class="item">
            <div class="testimonial-item">
              <div class="testimonial-author-wrap saspot-item">
                <div class="saspot-table-wrap">
                  <div class="saspot-align-wrap">
                    <?php if($large_image) { ?>
                    <div class="saspot-image"><img src="<?php echo esc_url($large_image); ?>" alt="<?php echo esc_attr($saaspot_alt); ?>"></div>
                    <?php } ?>
                    <h4 class="author-name"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a></h4>
                    <?php if ($testi_job) { ?><p><?php echo esc_html($testi_job); ?></p><?php } ?>
                  </div>
                </div>
              </div>
              <div class="testimonial-info saspot-item"><p><?php the_excerpt(); ?></p></div>
            </div>
          </div>
        <?php }

        endwhile;
        wp_reset_postdata();
        ?>
    <?php if ($testimonial_style === 'testimonial_two') { ?>
      </div>
    </div>
    <?php } else { ?>
      </div>
    <?php } ?>
    </div>
  </div>

  <?php
    endif;

} elseif (function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('apps') && !$noneed_apps_post)) {

  $apps_limit = cs_get_option('apps_limit');
  $saaspot_apps_show_category = cs_get_option('apps_show_category');
  $saaspot_apps_orderby = cs_get_option('apps_orderby');
  $saaspot_apps_order = cs_get_option('apps_order');
  $saaspot_apps_filter = cs_get_option('apps_filter');
  $saaspot_apps_pagination = cs_get_option('apps_pagination');
  $saaspot_categories_text = cs_get_option('categories_text');
  $apps_limit = $apps_limit ? $apps_limit : '9';

  $all_text = cs_get_option('all_text');
  $all_text_actual = $all_text ? $all_text : esc_html__('All', 'saaspot');
  $saaspot_categories_text = $saaspot_categories_text ? $saaspot_categories_text : esc_html__('Categories', 'saaspot');

  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }

  $args = array(
    // other query params here,
    'paged' => $my_page,
    'post_type' => 'apps',
    'posts_per_page' => (int)$apps_limit,
    'apps_category' => esc_attr($saaspot_apps_show_category),
    'orderby' => $saaspot_apps_orderby,
    'order' => $saaspot_apps_order
  );

  $saaspot_port = new WP_Query( $args ); ?>
  <div class="saspot-apps">
    <div class="container">
      <div class="row">

  	  	<?php if ($saaspot_apps_filter) { ?>
        <div class="col-xl-2 col-md-3">
          <div class="apps-categories">
            <h3 class="categories-title"><?php echo esc_html($saaspot_categories_text); ?></h3>
            <div class="masonry-filters">
              <ul>
                <li><a href="#section-apps" data-filter="*" class="active"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo esc_html($all_text_actual); ?></a></li>
                <?php
                  if ($saaspot_apps_show_category) {
                    $cat_name = explode(',', $saaspot_apps_show_category);
                    $terms = $cat_name;
                    $count = count($terms);
                    if ($count > 0) {
                      foreach ($terms as $term) {
                        echo '<li class="cat-'. preg_replace('/\s+/', "", strtolower($term)) .'"><a href="#section-apps" data-filter=".cat-'. preg_replace('/\s+/', "", strtolower($term)) .'" title="' . str_replace('-', " ", strtolower($term)) . '"><i class="fa fa-angle-right" aria-hidden="true"></i> ' . str_replace('-', " ", strtolower($term)) . '</a></li>';
                       }
                    }
                  } else {
                    if ( function_exists( 'saaspot_apps_category_list' ) ) {
                      echo saaspot_apps_category_list();
                    }
                  }
                ?>
              </ul>
            </div>
          </div>
        </div>
  	    <?php } ?>
  	    <!-- Start -->
  	    <div class="col-xl-10 col-md-9">
          <div class="apps-wrap">
            <div class="saspot-masonry" id="section-apps">
      	    	<?php
      	      if ($saaspot_port->have_posts()) : while ($saaspot_port->have_posts()) : $saaspot_port->the_post();
      	        get_template_part( 'layouts/post/content', 'apps' );
      	        endwhile;
      	      else :
      	        get_template_part( 'layouts/post/content', 'none' );
      	      endif; ?>
            </div>
          </div>
  	    </div>
  			<!-- End -->
        <?php if($saaspot_port->max_num_pages > 1) { ?>
        <div class="pagination-wrap">
  	    	<?php if ($saaspot_apps_pagination) { saaspot_paging_nav($saaspot_port->max_num_pages,"",$paged); } ?>
  	  	</div>
        <?php } wp_reset_postdata(); ?>

    	</div>
    </div>
  </div>

<?php } elseif (function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('webinars') && !$noneed_webinars_post)) {

  $webinars_style = cs_get_option('webinars_style');
  $webinars_limit = cs_get_option('webinars_limit');
  $saaspot_webinars_show_category = cs_get_option('webinars_show_category');
  $saaspot_webinars_orderby = cs_get_option('webinars_orderby');
  $saaspot_webinars_order = cs_get_option('webinars_order');
  $saaspot_webinars_filter = cs_get_option('webinars_filter');
  $saaspot_webinars_pagination = cs_get_option('webinars_pagination');
  $webinars_limit = $webinars_limit ? $webinars_limit : '9';

  $webi_all_text = cs_get_option('webi_all_text');
  $webi_all_text_actual = $webi_all_text ? $webi_all_text : esc_html__('ALL WEBINARS', 'saaspot');

  if ($webinars_style === 'two') {
    $style_class = ' examples-style-two';
    $row_class = 'row';
  } else {
    $style_class = '';
    $row_class = 'webinars-wrap';
  }

  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }

  $args = array(
    // other query params here,
    'paged' => $my_page,
    'post_type' => 'webinars',
    'posts_per_page' => (int)$webinars_limit,
    'webinars_category' => esc_attr($saaspot_webinars_show_category),
    'orderby' => $saaspot_webinars_orderby,
    'order' => $saaspot_webinars_order
  );

  $saaspot_port = new WP_Query( $args ); ?>
  <div class="saspot-webinars<?php echo esc_attr($style_class); ?>">
    <div class="container">
      <div class="<?php echo esc_attr($row_class); ?>">

        <?php if ($saaspot_webinars_filter && $webinars_style !== 'two') { ?>
        <div class="masonry-filters filters-style-two">
          <ul>
            <li><a href="javascript:void(0);" data-filter="*" class="active"><?php echo esc_html($webi_all_text_actual); ?></a></li>
            <?php
              if ($saaspot_webinars_show_category) {
                $cat_name = explode(',', $saaspot_webinars_show_category);
                $terms = $cat_name;
                $count = count($terms);
                if ($count > 0) {
                  foreach ($terms as $term) {
                    echo '<li class="webi-'. preg_replace('/\s+/', "", strtolower($term)) .'"><a href="javascript:void(0);" data-filter=".webi-'. preg_replace('/\s+/', "", strtolower($term)) .'" title="' . str_replace('-', " ", strtolower($term)) . '">' . str_replace('-', " ", strtolower($term)) . '</a></li>';
                   }
                }
              } else {
                if ( function_exists( 'saaspot_webinars_category_list' ) ) {
                  echo saaspot_webinars_category_list();
                }
              }
            ?>
          </ul>
        </div>
        <?php } ?>
        <!-- Start -->
        <?php if ($webinars_style !== 'two') { ?>
        <div class="webinars-inner-wrap">
          <div class="saspot-masonry" data-items="1">
            <?php }
            if ($saaspot_port->have_posts()) : while ($saaspot_port->have_posts()) : $saaspot_port->the_post();
              get_template_part( 'layouts/post/content', 'webinars' );
              endwhile;
            else :
              get_template_part( 'layouts/post/content', 'none' );
            endif;
        if ($webinars_style !== 'two') { ?>
          </div>
        </div>
        <?php } ?>
        <!-- End -->
        <?php if($saaspot_port->max_num_pages > 1) { ?>
        <div class="pagination-wrap">
          <?php if ($saaspot_webinars_pagination) { saaspot_paging_nav($saaspot_port->max_num_pages,"",$paged); } ?>
        </div>
        <?php } wp_reset_postdata(); ?>

      </div>
    </div>
  </div>

<?php } elseif (function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('team') && !$noneed_team_post)) {

  $team_limit = cs_get_option('team_limit');
  $team_orderby = cs_get_option('team_orderby');
  $team_order = cs_get_option('team_order');
  $saaspot_team_aqr = cs_get_option('team_aqr');

  $team_limit = $team_limit ? $team_limit : '8';
  // Query Starts Here
  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }

  $args = array(
    'paged' => $my_page,
    'post_type' => 'team',
    'posts_per_page' => (int)$team_limit,
    'orderby' => $team_orderby,
    'order' => $team_order,
  );

  $saaspot_team_qury = new WP_Query( $args );
  if ($saaspot_team_qury->have_posts()) :
  ?>

  <div class="saspot-mates">
    <div class="container">
      <div class="owl-carousel" data-items="4" data-items-tablet="2" data-items-mobile-landscape="1" data-margin="30" data-loop="true" data-nav="true" data-dots="true" data-autoplay="true">
      <?php
      while ($saaspot_team_qury->have_posts()) : $saaspot_team_qury->the_post();
      // Featured Image
  		$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
  		$large_image = $large_image[0];
  		$abt_title = get_the_title();
      if ($saaspot_team_aqr) {
        $team_featured_img = $large_image;
      } else {
    		if(class_exists('Aq_Resize')) {
    			$team_img = aq_resize( $large_image, '270', '370', true );
    		} else {$team_img = $large_image;}
    		$team_featured_img = ( $team_img ) ? $team_img : SAASPOT_IMAGES . '/holders/270x370.png';
      }
      // Link
      $team_options = get_post_meta( get_the_ID(), 'team_options', true );
      $team_pro = $team_options['team_job_position'];
      ?>
      <div class="item">
        <div class="mate-item">
          <?php if ($large_image) { ?>
          <div class="saspot-image"><a href="<?php echo esc_url( get_permalink() ); ?>"><img src="<?php echo esc_url($team_featured_img); ?>" alt="<?php echo esc_attr($abt_title); ?>"></a></div>
          <?php } ?>
          <div class="mate-info">
            <h5 class="mate-name"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html($abt_title); ?></a></h5>
            <p><?php echo esc_html($team_pro); ?></p>
          </div>
        </div>
      </div>
      <?php endwhile; ?>
      </div>
    </div>
  </div> <!-- Team End -->

  <?php
  endif;

} elseif (function_exists( 'saaspot_core_plugin_status' ) && (saaspot_is_post_type('job') && !$noneed_job_post)) {

  $job_limit = cs_get_option('job_limit');
  $job_orderby = cs_get_option('job_orderby');
  $job_order = cs_get_option('job_order');
  $job_show_category = cs_get_option('job_show_category');
  $job_pagination = cs_get_option('job_pagination');
  $applay_job = cs_get_option('applay_job');

  $job_limit = $job_limit ? $job_limit : '8';
  // Query Starts Here
  // Pagination
  global $paged;
  if( get_query_var( 'paged' ) )
    $my_page = get_query_var( 'paged' );
  else {
    if( get_query_var( 'page' ) )
      $my_page = get_query_var( 'page' );
    else
      $my_page = 1;
    set_query_var( 'paged', $my_page );
    $paged = $my_page;
  }

  $args = array(
    'paged' => $my_page,
    'post_type' => 'job',
    'posts_per_page' => (int)$job_limit,
    'job_role' => esc_attr($job_show_category),
    'orderby' => $job_orderby,
    'order' => $job_order,
  );

  $saaspot_job_qury = new WP_Query( $args );
  if ($saaspot_job_qury->have_posts()) :
  ?>
  <div class="saspot-jobs">
    <div class="container">
      <div class="jobs-wrap">
      <?php
      while ($saaspot_job_qury->have_posts()) : $saaspot_job_qury->the_post();
      // Featured Image
  		$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
  		$large_image = $large_image[0];
  		$abt_title = get_the_title();

      // Link
      $applay_job = $applay_job ? $applay_job : esc_html__('APPLY','saaspot');
      ?>
      <div class="job-item">
        <div class="row align-items-center">
          <div class="col-md-9">
            <p>
              <?php
                $category_list = wp_get_post_terms($post->ID, 'job_role');
                foreach ($category_list as $term) {
                  $job_term_link = get_term_link( $term );
                  echo '<span><a href="'. esc_url($job_term_link) .'">'. esc_html($term->name) .'</a></span> ';
                }
              ?>
            </p>
            <h3 class="job-title"><?php echo esc_html($abt_title); ?></h3>
          </div>
          <div class="col-md-3 textright">
            <div class="job-btn"><a href="<?php echo esc_url( get_permalink() ); ?>" class="saspot-btn saspot-light-blue-bdr-btn"><?php echo esc_html($applay_job); ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
          </div>
        </div>
      </div>

      <?php endwhile; ?>
      </div>
      <?php if ($job_pagination) { ?>
      <div class="pagination-wrap">
        <?php saaspot_paging_nav($saaspot_job_qury->max_num_pages,"",$paged); wp_reset_postdata(); ?>
      </div>
      <?php } ?>
    </div>
  </div> <!-- Job End -->

  <?php
    endif;
} else {
// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );

if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else { $saaspot_content_padding = ''; }
// Padding - Metabox
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
} else {
	$saaspot_custom_padding = '';
}

// Theme Options
$saaspot_sidebar_position = cs_get_option('blog_sidebar_position');
$saaspot_blog_widget = cs_get_option('blog_widget');
$saaspot_blog_style = cs_get_option('blog_listing_style');
// Style
if ($saaspot_blog_style === 'style-two') {
	$saaspot_blog_style = ' blog-style-two';
} else {
	$saaspot_blog_style = '';
}

if ($saaspot_blog_widget) {
  $widget_select = $saaspot_blog_widget;
} else {
  if (is_active_sidebar('sidebar-1')) {
    $widget_select = 'sidebar-1';
  } else {
    $widget_select = '';
  }
}

// Sidebar Position
if ($widget_select && is_active_sidebar( $widget_select )) {
  if ($saaspot_sidebar_position === 'sidebar-hide') {
    $layout_class = 'col-md-12';
    $saaspot_sidebar_class = 'hide-sidebar';
  } elseif ($saaspot_sidebar_position === 'sidebar-left') {
    $layout_class = 'saspot-primary';
    $saaspot_sidebar_class = 'left-sidebar';
  } else {
    $layout_class = 'saspot-primary';
    $saaspot_sidebar_class = 'right-sidebar';
  }
} else {
  $saaspot_sidebar_position = 'sidebar-hide';
  $layout_class = 'col-md-12';
  $saaspot_sidebar_class = 'hide-sidebar';
}

?>

<div class="saspot-blog <?php echo esc_attr($saaspot_blog_style); ?> <?php echo esc_attr($saaspot_content_padding .' '. $saaspot_sidebar_class); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
  <div class="container">
    <div class="row">
      <?php if ($saaspot_sidebar_position === 'sidebar-left' && $saaspot_sidebar_position !== 'sidebar-hide') { get_sidebar(); } ?>
      <div class="<?php echo esc_attr($layout_class); ?>">
        <?php
        if ( have_posts() ) :
          /* Start the Loop */
          while ( have_posts() ) : the_post();
            get_template_part( 'layouts/post/content' );
          endwhile;
        else :
          get_template_part( 'layouts/post/content', 'none' );
        endif; ?>
        <?php
          saaspot_default_paging_nav();
          wp_reset_postdata();  // avoid errors further down the page
        ?>
      </div><!-- row -->
      <?php if ($saaspot_sidebar_position !== 'sidebar-hide') { get_sidebar(); } ?>
    </div>
  </div>
</div>
<?php }
get_footer();
