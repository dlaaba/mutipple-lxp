<?php
/*
 * The sidebar containing the main widget area.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

$saaspot_blog_widget = cs_get_option('blog_widget');
$saaspot_single_blog_widget = cs_get_option('single_blog_widget');

if (is_page()) {
	// Page Layout Options
	$saaspot_page_layout = get_post_meta( get_the_ID(), 'page_type_metabox', true );	
}

?>
<div class="saspot-secondary saspot-sidebar">
	<?php do_action( 'saaspot_before_sidebar_action' ); // SaaSpot Action ?>
	<?php if (is_page() && $saaspot_page_layout['page_sidebar_widget']) {
		if (is_active_sidebar($saaspot_page_layout['page_sidebar_widget'])) {
			dynamic_sidebar($saaspot_page_layout['page_sidebar_widget']);
		}
	} elseif (!is_page() && $saaspot_blog_widget && !is_single()) {
		if (is_active_sidebar($saaspot_blog_widget)) {
			dynamic_sidebar($saaspot_blog_widget);
		}
	} elseif (is_single() && $saaspot_single_blog_widget) {
		if (is_active_sidebar($saaspot_single_blog_widget)) {
			dynamic_sidebar($saaspot_single_blog_widget);
		}
	} else {
		if (is_active_sidebar('sidebar-1')) {
			dynamic_sidebar( 'sidebar-1' );
		}
	} ?>
	<?php do_action( 'saaspot_after_sidebar_action' ); // SaaSpot Action ?>
</div><!-- #secondary -->
<?php
