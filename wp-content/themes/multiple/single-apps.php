<?php
/*
 * The template for displaying all single apps.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();

// Metabox
$saaspot_id    = ( isset( $post ) ) ? $post->ID : 0;
$saaspot_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $saaspot_id;
$saaspot_id    = ( saaspot_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $saaspot_id;
$saaspot_meta  = get_post_meta( $saaspot_id, 'page_type_metabox', true );
$apps_meta  = get_post_meta( $saaspot_id, 'apps_single_metabox', true );
$saaspot_meta_port  = get_post_meta( $saaspot_id, 'apps_client', true );
$saaspot_meta_port_type  = get_post_meta( $saaspot_id, 'apps_type', true );

if ($saaspot_meta) {
	$saaspot_content_padding = $saaspot_meta['content_spacings'];
} else { $saaspot_content_padding = ''; }
// Padding - Metabox
if ($saaspot_content_padding && $saaspot_content_padding !== 'padding-none') {
	$saaspot_content_top_spacings = $saaspot_meta['content_top_spacings'];
	$saaspot_content_bottom_spacings = $saaspot_meta['content_bottom_spacings'];
	if ($saaspot_content_padding === 'padding-custom') {
		$saaspot_content_top_spacings = $saaspot_content_top_spacings ? 'padding-top:'. saaspot_check_px($saaspot_content_top_spacings) .';' : '';
		$saaspot_content_bottom_spacings = $saaspot_content_bottom_spacings ? 'padding-bottom:'. saaspot_check_px($saaspot_content_bottom_spacings) .';' : '';
		$saaspot_custom_padding = $saaspot_content_top_spacings . $saaspot_content_bottom_spacings;
	} else {
		$saaspot_custom_padding = '';
	}
} else {
	$saaspot_custom_padding = '';
}
// Translation
$noneed_apps_post = cs_get_option('noneed_apps_post');

if (!$noneed_apps_post) {
	$saaspot_apps_need_related = cs_get_option('apps_need_related');
	$saaspot_apps_related_title = cs_get_option('apps_related_title');
	$saaspot_apps_related_title = $saaspot_apps_related_title ? $saaspot_apps_related_title : esc_html__('Related Links', 'saaspot');

	// Carousel Data's
	?>
	<div class="saspot-mid-wrap <?php echo esc_attr($saaspot_content_padding); ?>" style="<?php echo esc_attr($saaspot_custom_padding); ?>">
	  <div class="container">
	    <div class="apps-detail">
	      <div class="row">
		      <div class="col-lg-12">
						<?php
						if (have_posts()) : while (have_posts()) : the_post();
							the_content();
						endwhile;
						endif;
						?>
					</div>
				</div>
				<?php
				if ($saaspot_apps_need_related) {
				//get the taxonomy terms of custom post type
				$customTaxonomyTerms = wp_get_object_terms( $post->ID, 'apps_category', array('fields' => 'ids') );
				//query arguments
				$args = array(
				  'post_type' => 'apps',
				  'post_status' => 'publish',
				  'posts_per_page' => 3,
				  'orderby' => 'rand',
				  'tax_query' => array(
				    array(
				      'taxonomy' => 'apps_category',
				      'field' => 'id',
				      'terms' => $customTaxonomyTerms
				    )
				  ),
				  'post__not_in' => array ($post->ID),
				);
				//the query
				$relatedPosts = new WP_Query( $args );

				//loop through query
				if($relatedPosts->have_posts()){ ?>
					<div class="saspot-apps apps-style-two">
					  <div class="container">
					    <div class="section-title-wrap">
					      <h2 class="section-title"><?php echo esc_html($saaspot_apps_related_title); ?></h2>
					    </div>
					    <div class="apps-items-wrap">
					      <div class="row">
					        <?php while ($relatedPosts->have_posts()) {
									$relatedPosts->the_post();
									$related_large_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
						      $related_large_image = $related_large_image[0];
									if(class_exists('Aq_Resize')) {
						      	$port_img = aq_resize( $related_large_image, '300', '230', true );
									} else {$port_img = $related_large_image;}
									$featured_img = ( $port_img ) ? $port_img : $related_large_image;
						      ?>
					        <div class="col-lg-4 col-md-6">
					          <div class="app-item">
					            <div class="saspot-image">
					              <a href="<?php echo esc_url( get_permalink() ); ?>"><img src="<?php echo esc_url($featured_img); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></a>
					              <div class="saspot-label">
					              	<?php
									          $category_list = wp_get_post_terms($post->ID, 'apps_category');
									          foreach ($category_list as $term) {
									            echo '<span>'. esc_html($term->name) .'</span> ';
									          }
									        ?>
					              </div>
					            </div>
					            <div class="app-info">
					              <h4 class="app-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a></h4>
					              <p><?php the_excerpt(); ?></p>
					            </div>
					          </div>
					        </div>
					        <?php } ?>
					      </div>
					    </div>
					  </div>
					</div>
					
				<?php } else{ } }
				//restore original post data
				wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
<?php }
get_footer();
